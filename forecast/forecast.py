"""

try to forecast the next direction of a price 
using a reinforcement learning algorithm

note that by construction we believe in markets
with perfect information: all the information is
contained in the price itself and its history

also, since the full history up to 'now' is known
at the moment of the beginning of the learning process,
we can feed the full price to the machine and see if and how
there is actually an improvement in the forecasting ability:
every decision can (but is not required to) be based 
on all the known past price values

"""

import random
from typing import NamedTuple

import numpy as np
import matplotlib.pyplot as plt

from learner import Learner
from mc_gbm import mc_GBM

# ============================================================================

# the "rules" for this "game"

class Options(NamedTuple):
    up: bool = True
    down: bool = False

# possible strategies
# from bool to bool

def random_choice(my, opp) -> bool:
    return random.choice(Options())

def follow_up(my, opp) -> bool:
    """
    follow the price trend (if going down -> say it will do again)
    """
    return opp[-1]

def bet_against(my, opp) -> bool:
    """bet on a trend inversion"""
    return not opp[-1]

# ============================================================================

class PriceForecast(Learner):

    @staticmethod
    def strategy_wins(my_move, opponent_move) -> bool:
        return my_move == opponent_move

# ============================================================================



def main():

    strats = (
        follow_up,
        bet_against,
        random_choice
    )

    rand = np.random.RandomState(321)
    reps = 1
    times = int(input("How many price steps? "))
    dt = 1/12
    sigma = 0.3
    mu = 0.0
    s0 = 4.0

    price = mc_GBM(rand, reps, times, mu, sigma, dt, s0)
    increments = np.where(price[1:]>price[:-1], True, False)

    pl = PriceForecast(strategies=strats)
    pl._opponent_hist = list(increments[:2])
    pl._my_hist = [random_choice(0, 0), random_choice(0, 0)]

    pl.learn(increments[2:])
    # pl.show_stats()

    forecasts = list(pl._my_hist)

    # every time we were correct
    tot = 0
    equal_x = []
    different_x = []
    for i, (r,f) in enumerate(zip(increments, forecasts)):
        if r==f:
            tot += 1
            equal_x.append(i)
        else:
            different_x.append(i)

    where_equal = [forecasts[i] for i in equal_x]
    where_different_real = [increments[i] for i in different_x]

    plt.figure()
    plt.subplot(2, 1, 1)
    plt.plot(price, linewidth=.5)
    plt.subplot(2, 1, 2)
    plt.scatter(equal_x, where_equal, s=16, color="r", label="good guess")
    plt.scatter(different_x, where_different_real, s=16, color="k", marker="x", label="should have picked this")
    plt.legend()
    plt.show()

    return 0


if __name__ == "__main__":
    import os
    _ = os.system("cls")
    main()