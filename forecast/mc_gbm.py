import numpy as np


def mc_GBM(rand:np.random.RandomState, reps:int, times:int, mu:float, sigma:float, dt:float, s0:float):
    '''returns N GMB trajs, as a matrix'''
    x = np.empty(shape=(times+1, reps)) 
    shocks = rand.normal(size=(times, reps), scale=np.sqrt(dt)*sigma)
    x[1:] = (mu-.5*sigma*sigma)*dt + shocks
    np.add.accumulate(x[1:], out=x[1:], axis=0)
    np.exp(x[1:], out=x[1:])
    x[0] = s0
    np.multiply(x[1:], s0, out=x[1:])
    return x
