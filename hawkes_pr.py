'''
    Hawkes processes
    https://colab.research.google.com/github/omitakahiro/Hawkes/blob/master/notebooks/tutorial.ipynb#scrollTo=NYaJ4ZaWKByE
'''

import numpy as np
import matplotlib.pyplot as plt
import Hawkes as hk

def main():
    # global rng set
    np.random.seed(42)
    
    # simulator obj
    M = hk.simulator()
    
    # kernel
    M.set_kernel('exp') # 'pow'
    
    # baseline intensity
    M.set_baseline('const')
    # baseline function
##    mu_t = lambda x: 0.08 + 0.02*np.sin(2*np.pi*x/100)
##    M.set_baseline('custom', l_custom = mu_t)
    
    # parameters
    params = {'mu': .1, 'alpha': .67, 'beta': .42}
    
    # the upward jump in the conditional intensity is a*b with kernel = 'exp'
    M.set_parameter(params)
    
    # simulation interval
    st = 0
    en = 200
    itv = [st, en]
    T = M.simulate(itv)
    
    # extract conditional intensity
    t, lda_ker = M.kernel.sequential().tl(T, itv)
    lda_bas = M.baseline.l(t)
    lda_t = lda_ker + lda_bas
    
    # num of events to plot
    x = np.hstack([st, np.repeat(T, 2), en]) # the inter-arrival times
    y = np.repeat(np.arange(len(T)+1), 2) # the counting process
    p = x[1::2] # events "location"
    
    # plot
    fig, ax1 = plt.subplots()
    plt.xlabel("Time")
    ax1.set_ylabel("# events", color="b")
    ax1.plot(x, y, color="b", linewidth=1.2, label="Number of Events")
    ax2 = ax1.twinx()
    ax2.set_ylabel("$\lambda(t)$", color="r")
    ax2.plot(t, lda_t, color="r", linewidth=.4, label="Conditional Intensity")
    ax2.spines['right']
    plt.title("Hawkes process")
    plt.scatter(p, min(lda_t)*np.full(shape=len(p), fill_value=.9), s=4, color="k", label="Event's Occurence")
##    plt.show()
    plt.figure()
    plt.hist(lda_t, bins=max(len(lda_t)//50, 200), density=True)
    plt.title("Histogram of conditional intensity")
    plt.show()
    
    return 0    

if __name__=="__main__":
    main()
