
import numpy as np
import matplotlib.pyplot as plt
from time import time, sleep
from sys import stdout as cout
from keras.models import Sequential
from keras.layers import Dense
from os import system, name, environ
from sklearn.preprocessing import StandardScaler

##def nn_model(in_dim):
##    '''starting from input dimension, returns a customized,
##    fully connected FFNN with continous output'''
##    # get hyperparams
##    layer_n = int(input("\nHow many hidden layers? "))
##    layers = np.ndarray(layer_n, dtype=int)
##    # istantiate model
##    model = Sequential()
##    # add all the required hidden layers 
##    for i in range(layer_n):
##        x = int(input("How many neurons in layer %d? "%(i+1)))
##        layers[i] = x
##        if i==0:
##            model.add(Dense(x, input_dim=in_dim, activation='relu'))
##        else:
##            model.add(Dense(x, activation='relu'))
##    # add out layer
##    model.add(Dense(1))
##    # print some info    
##    print("\nYou have created a NN architecture as follows:\n")
##    print("INPUTS -> ", end='')
##    for i in layers:
##        sprint(" %d -> "%i, end='')
##    print(" OUTPUT\n")
##    return model


def nn_model(in_dim):
    # istantiate model
    model = Sequential()
    model.add(Dense(6, input_dim=in_dim, activation='relu'))
    model.add(Dense(12, activation='relu'))
    model.add(Dense(3, activation='relu'))
    model.add(Dense(1))
    return model

####################################################################

# datafile, split, epochs, verbose, batch_size

if __name__=="__main__":

    environ["CUDA_VISIBLE_DEVICES"]="-1"

    print("This code accepts datasets of the form:\n x1, x2, ..., xN, y\n")

    datafile = "rv_hes_1.txt"#str(input("datafile name (remember extension)? "))
    print("\nloading %s"%datafile)
    tim = time()
    dataf = (np.loadtxt(datafile, delimiter=','))

    first = 100
    plt.figure()
    plt.title("The first %d data points"%first)
    plt.plot(dataf[0:first, ])
    plt.show()
    
    print("data shape: ", np.shape(dataf))
    print("elapsed: %.2f\n"%(time()-tim))
    #print("you have %d observations, " %len(dataf))
    split = 8000#int(input("how many of them as TRAIN set? "))
    print("train size: %d"%split)
    test_size = len(dataf)-split
    print("test size: %d"%test_size)
    e = 3000#int(input("\nhow many epochs? ")) # epochs
    v = 2#int(input("verbose? (0,1,2) ")) # verbose
    bs = 8000#int(input("batch size? ")) # batch size

    ####################################################################

    ### 
    if len(dataf)<(split+test_size-1):
        print("\ndataset rows: %d\ntrain+test=%d\nselect smaller subsets"%(len(dataf), (split+test_size)))
        quit()

    ### test data
    data = dataf[0:split, :]

    muy = np.mean(data[:, -1])
    sdy = np.sqrt(np.var(data[:, -1]))

    # create scaler
    scaler = StandardScaler()
    
    # fit scaler on data
    scaler.fit(data)
    
    # apply transform
    standardized = scaler.transform(data)
    
    # inverse transform
    ##inverse = scaler.inverse_transform(standardized)
    #print("\ndata scaled\n")


    X = standardized[:, 0:-1]
    y = standardized[:, -1]

    X = np.asarray(X)
    y = np.asarray(y)
    #print("X", np.shape(X))
    #print("y", np.shape(y))
    in_dim = len(X[0])
    print("\nthere are %d inputs"%in_dim)

    ########################################################################

    print("|FF NN|")
    tim = time()
    
    model = nn_model(in_dim)

    model.compile(loss='mse', optimizer='adam', metrics=["mse", "mae"])

    print("\nfitting...\n")
    np.random.seed(2)

    history = model.fit(X, y, epochs=e, batch_size=bs ,verbose=v)#bs=len(y)

    pred = model.predict(X)

    print("\nelapsed: %.2f\n"%(time()-tim))
    ########################################################################

    # summary
    print(model.summary())

    # recover original values
    pred = (pred * sdy) + muy
    y = data[:, -1]

    # plot errors
    plt.figure()
    plt.plot(np.asarray(history.history['mse']), label="mse")
    plt.plot(np.asarray(history.history['mae']), label="mae")
    plt.legend(loc="upper right")
    plt.xlabel("epochs")
    plt.xticks([])
    plt.title("FF NN, errors")
    plt.show()

    tot = len(y)

    plt.subplot(2, 2, 1)
    plt.plot(range(tot), y[:tot], linewidth=0.4, color="k", label="y")
    plt.scatter(range(tot), pred[:tot], s=1, color="r", label="y hat")
    ##plt.plot(range(tot), pred[:tot], linewidth=0.4, color="r")
    plt.legend()
    plt.xticks([])
    plt.title("FF NN\nIn-sample predictions\n%d values"%tot)
##    plt.show()

    tot = len(y)

    plt.subplot(2, 2, 2)
    plt.scatter(pred[:tot], y[:tot], s=0.4, color="r")
    d, u = min(y[:tot]), max(y[:tot])
    plt.ylim(bottom=d, top=u)
    d, u = min(pred[:tot]), max(pred[:tot])
    plt.xlim(left=d, right=u)
    plt.plot([d,u], [d,u],"k")
    plt.xlabel("y hat")
    plt.ylabel("y", rotation="horizontal")
    plt.title("FF NN\nIn-sample predictions\nall values")
##    plt.show()

    s = "n"#str(input("save model? (y) "))
    if s=="y":
        name = "FF12"#str(input("model name? "))
        model.save(name)
        print("model saved as %s"%name)
        sleep(2)

    ##########################################################################
    ##########################################################################
    ##########################################################################
    ##########################################################################

    print("\n\nOut of sample predictions\n")

    ### out of sample data
    data = dataf[split:split+test_size, :]

    # get targets
    X = data[:, 0:-1]
    y = data[:, -1]

##    print("X", np.shape(X))
##    print("y", np.shape(y))

    # scaled data is fed to predictive model
    scaler = StandardScaler()
    scaler.fit(X)
    X = scaler.transform(X)

    # predicting
    pred = model.predict(X)

    # converting predictions to original scale
    muy = np.mean(y)
    sdy = np.sqrt(np.var(y))
    pred = (pred * sdy) + muy

    ### plots
    tot = len(y)

    plt.subplot(2, 2, 3)
    plt.plot(range(tot), y[:tot], linewidth=0.4, color="k", label="y")
    plt.scatter(range(tot), pred[:tot], s=1, color="r", label="y hat")
    ##plt.plot(range(tot), pred[:tot], linewidth=0.4, color="r")
    plt.legend()
    plt.xticks([])
    plt.title("FF NN\nOut-of-sample predictions\n%d values"%tot)
##    plt.show()

    tot = len(y)

    plt.subplot(2, 2, 4)
    plt.scatter(pred[:tot], y[:tot], s=0.4, color="r")
    d, u = min(y[:tot]), max(y[:tot])
    plt.ylim(bottom=d, top=u)
    d, u = min(pred[:tot]), max(pred[:tot])
    plt.xlim(left=d, right=u)
    plt.plot([d,u], [d,u],"k")
    plt.xlabel("y hat")
    plt.ylabel("y", rotation="horizontal")
    plt.title("FF NN\nOut-of-sample predictions\nall values")
##    plt.show()

    plt.show()# show all plots




