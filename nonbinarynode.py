from itertools import chain, repeat


class Node:
    """

    (basic implementation of a non-binary tree)

    generic leaf type. the root is the one with no parents.
    the final leaves are the only ones with no childs
    
    ## example
    >>> root = Node(value="*")
    >>> a1 = Node(value="a", parent=root)
    >>> b1 = Node(value="b", parent=root)
    >>> c1 = Node(value="c", parent=root)
    >>> a2 = Node(value="aa1", parent=a1)
    >>> a3 = Node(value="aa2", parent=a1)
    >>> b2 = Node(value="bb1", parent=b1)
    """
    
    def __init__(self, value, parent = None):
        self.value = value
        self.parent = parent
        self.children = []
        if self.parent:
            self.parent.children.append(self)
        
    def __repr__(self) -> str:
        return str(self.value)
        

def print_tree(leaf:Node, __level: int = 0):
    """
    starting from a given Node (assumed to be the root)
    nicely prints the whole structure to the screen
    """
    assert isinstance(leaf, Node)
    print(' '*__level*4, leaf.value)
    if leaf.children:
        for c in leaf.children:
            print_tree(c, __level + 1)


def get_parents(leaf:Node) -> tuple:
    """
    given a leaf of a tree, returns a tuple with all the ordered 
    ancestry, starting from the root until the leave included
    """
    assert isinstance(leaf, Node)
    ancestry = list()
    curr_node = leaf
    while 1:
        ancestry.append(curr_node.value)
        cnp = curr_node.parent
        if not cnp:
            break
        curr_node = cnp
    return tuple(reversed(ancestry))


def print_ancestry(tree):
    """convenience function to print the ancestry dict"""
    for k, v in tree.items():
        print(f"%20s  ->  {v}"%k)
    print()


def full_ancestry(root:Node) -> dict:
    """
    given the root node, returns a dictionary that maps
    all members to their whole ancestry
    """
    assert isinstance(root, Node)
    assert (root.parent is None)
    ancestry = dict()
    to_visit = [root]
    while to_visit:
        n = to_visit.pop()
        ancestry[n.value] = get_parents(leaf=n)
        to_visit.extend(n.children)
    return ancestry


def add_padding(t:tuple, n:int) -> tuple:
    """
    returns a new tuple of lenght n, with extra
    spaces filled with None
    """
    assert isinstance(t, tuple)
    assert isinstance(n, int)
    assert (n > 1)
    m = len(t)
    if m == n:
        return t
    assert (m < n)
    return tuple(chain(t, repeat(None, n-m)))


def pad_ancestry(tree:dict, n:int=0) -> dict:
    """fully pads an ancestry dict"""
    max_depth = n
    for k, v in tree.items():
        lv = len(v)
        if lv > max_depth:
            max_depth = lv
    new_tree = dict.fromkeys(tree.keys())
    for k, v in tree.items():
        new_tree[k] = add_padding(v, max_depth)
    return new_tree
  
# ============================================================================
      
def main():
    from os import system
    _ = system("cls")

    # build the tree
    root = Node(value="ROOT")
    a1 = Node(value="a", parent=root)
    b1 = Node(value="b", parent=root)
    c1 = Node(value="c", parent=root)
    a2 = Node(value="aa1", parent=a1)
    a3 = Node(value="aa2", parent=a1)
    b2 = Node(value="bb1", parent=b1) 
    a4 = Node(value="aaa1", parent=a2)
    a5 = Node(value="aaa2", parent=a3)
    
    # convenience function for printing
    print_tree(root)
    print("\n", "_"*30, "\n")

    # get the full ancestry, this obj is independent of the
    # initial tree and is easily serialized
    family_tree = pad_ancestry(tree=full_ancestry(root=root), n=5)

    # convenience function for printing
    print_ancestry(family_tree)




if __name__ == "__main__":
    main()