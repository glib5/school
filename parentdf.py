import os

import pandas as pd


def rebuild_relations(df:pd.DataFrame, child_col:str, father_col:str, depth:int=1) -> pd.DataFrame:
    C = child_col
    F = father_col
    FX = f"{F}_x"
    CX = f"{C}_x"
    FY = f"{F}_y"
    CY = f"{C}_y"
    rels = df
    for i in range(depth):
        rels = (
            rels
            .merge(
                right=df,
                how="left",
                left_on=C,
                right_on=F
            )
            .drop(columns=[FY])
            .rename(columns={
                FX: f"{C}_{i}",
                CX: F,
                CY: C
            })
        )
    return rels.drop(columns=[F, C])


def numerical():
    fathers = list(map(float, range(1, 8)))
    childs = [i+1 for i in fathers]
    childs[-1] = None

    df = pd.DataFrame({"F": fathers, "C":childs})
    rels = rebuild_relations(df, "C", "F", 7)
    print(rels)


def main():
    c = ["falcon", "bird", "animal", "parrot", "dog"]
    f = ["bird", "animal", "alive", "bird", "animal"]
    data = pd.DataFrame({"C":c, "F":f})
    print(data)
    print()
    print(rebuild_relations(data, "F", "C", 4))
    print()




if __name__ == "__main__":
    _ = os.system("cls")
    # numerical()
    main()