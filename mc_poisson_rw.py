import numpy as np
import matplotlib.pyplot as plt
from time import perf_counter, sleep
from os import system, name


def poisson_rw(rand, reps, times, lda):
    '''generate a poisson rw, centered around 0'''
    J = np.empty(shape=(times+1, reps), dtype=int)
    J[0] = 0
    J[1:] = rand.poisson(lam=lda, size=(times, reps))
    #rand.choice([True, False], size=(perf_counters, reps))
    np.multiply(J[1:], rand.choice([-1, 1], size=(times, reps)), out=J[1:])
    np.add.accumulate(J[1:], out=J[1:], axis=0)
    return J


def MC(rand, reps, pr, times, lda):
    fm = np.zeros(shape=times+1)
    sm = np.zeros(shape=times+1)
    print("%12d"%reps)
    tim = perf_counter()
    for c in range(reps//pr):
        v = poisson_rw(rand, pr, times, lda)
        fm += np.sum(v, axis=1)
        sm += np.sum(v*v, axis=1)
        i = (c+1)*pr
        if i%(1<<20)==0:
            print("%12d\t%2.2f"%(i, perf_counter()-tim), end='\r', flush=True)
    fm /= reps
    sm = 3*np.sqrt( (sm/reps-fm*fm)/reps )
    return fm, sm
  

def main():

    if name=='nt':
        _=system('cls')

    rand = np.random.RandomState(seed=4628)

    lda = 5
    n = 21#int(input("Reps? "))
    reps = 1<<n
    pr = 1<<10
    times = 12
    s0 = 13

    es, err = MC(rand, reps, pr, times, lda)
    ax = np.arange(len(es))
    plt.errorbar(ax, es, err)
    S = es[0]
    plt.plot([0, ax[-1]], [S, S], "r")
    plt.ylim(S-.1, S+.1)
    plt.show()


    plt.plot(poisson_rw(rand, 4, 100, .5))
    plt.show()

if __name__=="__main__":
    main()  

        




    

