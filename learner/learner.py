from abc import ABC, abstractmethod
from collections import Counter
from itertools import accumulate
from random import choices
from typing import Callable, List


class Learner(ABC):

    """
    ABC class for reinforcement learning algorithm
    
    helps identyfying the best strategy among a set of pre-determined ones
    
    Args:
        strategies (List[Callable]): list of functions that choose a strategy.
        - these functions must always take as arguments my_history and opponent_history
        that are passed internally inside the self.learn() method. the case where they are
        empty lists should be acounted for. example:

        >>> def random_reply(my_history, opponent_history):
        ...     return choice(OPTIONS)

        >>> def another_strat(my_history, opponent_history):
        ...     if (not my_history) or (not opponent_history):
        ...         return random_reply(my_history, opponent_history)
        ...     return my_history[-1]

        verbose (bool, default=False)

    ABC notes:
        the staticmethod 'strategy_wins' has to be defined by the subclasses

    """

    def __init__(self, strategies: List[Callable], verbose: bool = False):
        self.strategies = Counter(strategies)
        self._v: bool = verbose
        
        self._my_hist: List = list()
        self._opponent_hist: List = list()

    @staticmethod
    @abstractmethod
    def strategy_wins(my_move: List, opponent_move: List) -> bool:
        """assert wheter a strategy was succesful or not"""
        raise Exception(f"method 'strategy_wins' not implemented")

    def _pop_and_cumweights(self):
        pop, weights = zip(*self.strategies.items())
        cum_weights = accumulate(weights)
        return (tuple(pop), tuple(cum_weights))

    def learn(self, opponent_moves: List):
        for opponent_move in opponent_moves:
            # choose a function
            population, weights = self._pop_and_cumweights()
            strat = choices(population=population, cum_weights=weights, k=1)[0]
            # get a move from that function
            my_move = strat(self._my_hist, self._opponent_hist)
            # update history
            self._my_hist.append(my_move)
            self._opponent_hist.append(opponent_move)
            # compare outcomes               
            if self.strategy_wins(my_move, opponent_move):
                # update weights
                self.strategies[strat] += 1
                if self._v:
                    print(f"Choosen strat: {strat.__name__} -> won")
            else:
                if self._v:
                    print(f"Choosen strat: {strat.__name__} -> lost")

    def show_stats(self):
        """prints basic stats on the current learning"""
        all_ = len(self._my_hist)
        tot = self.strategies.total()
        print(f"Total turns: {all_}")
        for k, v in self.strategies.items():
            print(f"{k.__name__} succeded {v-1} times. (weight={v/tot:4n})" )
            
    def show_history(self):
        """shows history of past plays"""
        head = f"%4s\tmy move\topponent outcome"%"turn"
        print(head)
        print("-"*len(head))
        for i, (my, opp) in enumerate(zip(self._my_hist, self._opponent_hist), start=1):
            ans = "won" if self.strategy_wins(my, opp) else "lost"
            print(f"%4d\t{my}\t{opp}\t{ans}"%i)
