from collections import Counter
from functools import lru_cache
from pprint import pprint
from random import choices, choice
from itertools import chain

from learner import Learner

# specific to the "game"

# possible moves
OPTIONS = ["R", "S", "P"]

# scoring of the moves
@lru_cache(maxsize=9) # 3x3 possible outcomes
def scorer(outcome:str) -> bool:
    if outcome in {"RS", "SP", "PR"}:
        return True
    return False

# possible strategies
# should account for t=0 i.e. history is empty list

ideal_response = {'P': 'S', 'R': 'P', 'S': 'R'}

# Strategy Utilities #############################

def select_proportional(events, baseline=()):
    rel_freq = Counter(chain(baseline, events))
    population, weights = zip(*rel_freq.items())
    return choices(population, weights)[0]

def select_maximum(events, baseline=()):
    rel_freq = Counter(chain(baseline, events))
    return rel_freq.most_common(1)[0][0]

# Strategies #####################################

def random_reply(p1hist, p2hist):
    return choice(OPTIONS)

def single_event_proportional(p1hist, p2hist):
    """ When opponent plays R two-thirds of the time,
        respond with P two-thirds of the time.'
    """
    prediction = select_proportional(p2hist, OPTIONS)
    return ideal_response[prediction]

def single_event_greedy(p1hist, p2hist):
    """ When opponent plays R more than P or S,
        always respond with P.'
    """
    prediction = select_maximum(p2hist, OPTIONS)
    return ideal_response[prediction]

def digraph_event_proportional(p1hist, p2hist):
    """ When opponent's most recent play is S
        and they usually play R two-thirds of the time
        after an S, respond with P two-thirds of the time.
    """
    recent_play = p2hist[-1:]
    digraphs = zip(p2hist, p2hist[1:])
    followers = [b for a, b in digraphs if a == recent_play]
    prediction = select_proportional(followers, OPTIONS)
    return ideal_response[prediction]

def digraph_event_greedy(p1hist, p2hist):
    """ When opponent's most recent play is S
        and they usually play R two-thirds of the time
        after an S, respond with P all of the time.
    """
    recent_play = p2hist[-1:]
    digraphs = zip(p2hist, p2hist[1:])
    followers = [b for a, b in digraphs if a == recent_play]
    prediction = select_maximum(followers, OPTIONS)
    return ideal_response[prediction]











# subclass the learner

class RSP(Learner):

    @staticmethod
    def strategy_wins(my_move, opponent_move) -> bool:
        outcome = "".join([my_move, opponent_move])
        return scorer(outcome)



def main():

    strats = [
              random_reply,
              single_event_proportional,
              single_event_greedy,
              digraph_event_proportional,
              digraph_event_greedy
              ]

    lrn = RSP(strategies=strats, verbose=0)
    trials = 1000
    for i in range(trials):
        # print(f"turn {i+1}/{trials}")
        human_move = choice(OPTIONS)
        lrn.learn(opponent_moves=[human_move])
    lrn.show_stats()
    print("\n\n")
    
    # if all the moves are known in advance, this is equivalent:
    lrn2 = RSP(strategies=strats, verbose=0)
    all_opponent_moves = choices(population=OPTIONS, k=trials)
    lrn2.learn(all_opponent_moves)
    print()
    lrn2.show_stats()

    return


if __name__ == "__main__":
    from os import system
    _ = system("cls")
    main()
