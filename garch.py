import numpy as np
import matplotlib.pyplot as plt
from time import time, sleep

def garch(rand, times, c, a, b):
    '''simulates garch(1 (a), 1(b)) and
    gives back returns and variance processes'''
    rets = np.ndarray(shape=times, dtype=np.double)
    rets = rand.normal(size=times)
    var = np.ndarray(shape=times, dtype=np.double)
    # first obs
    var[0] = c
    rets[0] *= c
    # other obs
    for t in range(1, times):
        var[t] = c + rets[t-1]*rets[t-1]*a + var[t-1]*b
        rets[t] *= var[t]
    return rets, var


def garch_pl(rets, var):
    plt.figure()
    plt.subplot(2, 1, 1)
    plt.title("returns")
    plt.plot(rets, linewidth=.8)
    plt.subplot(2, 1, 2)
    plt.title("variance")
    plt.plot(var, linewidth=.8)
    plt.suptitle("GARCH(1,1)")
    plt.figure()
    plt.hist(rets, bins=100, density=True)
    plt.title("returns ditribution")
    plt.show()

#------------------------------

def main():

    rand = np.random.RandomState()
    rand.seed(478944)

    times = 2000#int(input("Periods? "))#2000
    
    c = .015
    a = .5
    b = .8

##    print("GARCH(1,1)")
    # preallocate memory
    rets = np.ndarray(shape=times, dtype=np.double)    
    var = np.ndarray(shape=times, dtype=np.double)
    
    rets, var = garch(rand, times, c, a, b)

    garch_pl(rets, var)

if __name__=="__main__":
    main()
