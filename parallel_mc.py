
from concurrent.futures import ProcessPoolExecutor as ProcessPool
from multiprocessing import Pool
from copy import deepcopy
from math import sqrt
from typing import Any, Tuple

import numpy as np

# ============================================================================

class Pricer:
    """
    # Pricer
    class used to do MonteCarlo simulation for pricing derivatives

    ### params
    - a dict that contains all the params of the simulation
    - this dict is then unpacked into the custom functions

    ### tot_reps
    (int) log base 2 of the total number of simulations

    ### workers
    - (int) number of simultaneous processes
    - can be only one of (1, 2, 4, 8, 16, 32)

    ## attention:
    - the following functions must return 2D numpy ndarrays
    - where array[0] is the starting point for all the generated trajectories

    ### generator_func
    - function that generates the underlyng process
    - first argument is the random instance
    - second is how many trajectories are produced
    - third is the previuos 'params' parameter, that has to be unpacked as shown
    #### example:

    >>> def example(rand, reps, params):
    ...     times = params.get("times", 12)
    ...     return rand.normal(size=(times, reps))

    ### pricing_func
    this is the pricing rule. unpacks argument as in the above example, but has no rand instance
    
    instead it has a first argument that is the result of the generator_func
    #### example

    >>> def example(trajs, params):
    ...     strike = params.get("strike", 10)
    ...     return np.maximum(trajs, strike)

        """

    def __init__(self, params:dict, tot_reps:int, workers:int, generator_func, pricing_func):
        self.params = deepcopy(params)
        _tot_reps = 1<<tot_reps # 2**n
        assert workers in (1, 2, 4, 8, 16, 32) # max = 2**5
        self.generator_func = generator_func
        self.pricing_func = pricing_func
        # compute internal params
        mc_reps = _tot_reps // workers # reps that every MC process will do -> is automatically 2**p
        reps = 1<<10 # number of trajs for every "generator_func" call # 2**10
        if mc_reps < reps:
            raise ValueError(f"reps={mc_reps} < p_reps={reps} -> raise 'tot_reps'!")
        self.params.update({"tot_reps":_tot_reps,
                            "mc_reps":mc_reps, 
                            "reps":reps,
                            "workers":workers})
        
    def _pricing_rule(self, trajs:np.ndarray) -> np.ndarray:
        """custom pricing rule"""
        return self.pricing_func(trajs, self.params)

    def _generate_underlying(self, rand, reps) -> np.ndarray:
        """this generates the underlying trajectories"""
        j = self.generator_func(rand, reps, self.params) 
        # ensure correct matrix type and orientation
        assert isinstance(j, np.ndarray)
        assert all(j[0]==j[0][0])
        return j

    def _mc(self, rand) -> Tuple[np.ndarray, np.ndarray]:
        """MonteCarlo routine"""
        times = self.params.get("times")
        mc_reps = self.params.get("mc_reps") # tot_reps // workers
        reps = self.params.get("reps") # 1<<10
        # math
        es = np.zeros(shape=(times+1), dtype=np.double)
        err = np.zeros(shape=(times+1), dtype=np.double)
        for _ in range(mc_reps // reps):
            trajs = self._generate_underlying(rand, reps)
            trajs = self._pricing_rule(trajs)
            es += trajs.sum(axis=1)
            err += (trajs*trajs).sum(axis=1)
        return (es, err)

    def simulate(self) -> Tuple[np.ndarray, np.ndarray]: 
        """execute the mc function in parallel processes"""
        times = self.params.get("times")
        workers = self.params.get("workers")
        tot_reps = self.params.get("tot_reps")
        # multiprocessing
        
        with ProcessPool(max_workers=workers) as pool:
            # fix rng seed here
            rng = tuple((np.random.RandomState(seed) for seed in range(workers)))
            res = pool.map(self._mc, rng)

        # with Pool(processes=workers) as pool:
        #     # fix rng seed here
        #     rng = tuple((np.random.RandomState(seed) for seed in range(workers)))
        #     res = pool.imap_unordered(self._mc, rng)

        # aggregate stats
        ES = np.zeros(shape=(times+1), dtype=np.double)
        ERR = np.zeros(shape=(times+1), dtype=np.double)
        for es, err in res:
            ES += es
            ERR += err
        ES /= tot_reps
        ERR = 3*np.sqrt((ERR/tot_reps - ES*ES)/tot_reps)
        return ES, ERR

# ============================================================================

# define custom functions 

def price_call(trajs, params:dict[str, Any]) -> np.ndarray:
    """price a vanilla call option"""
    # extract params
    strike = params.get("strike", params.get("s0"))
    r = params.get("r")
    t = params.get("T")
    # math
    kt = strike*np.exp(-r*t)
    kT = np.full(shape=trajs.shape, fill_value=kt.reshape(len(kt), -1))
    return np.where(trajs - kT > .0, trajs - kT, .0)


def gbm(rand, reps, params:dict[str, Any]) -> np.ndarray:
    """this generates gbm trajectories"""
    # extract params
    times = params.get("times")
    mu = params.get("mu")
    sigma = params.get("sigma")
    dt = params.get("dt")
    s0 = params.get("s0")
    # math
    x = np.empty(shape=(times+1, reps))
    x[1:] = (mu-.5*sigma*sigma)*dt + rand.normal(size=(times, reps), scale=sqrt(dt)*sigma)
    np.add.accumulate(x[1:], out=x[1:], axis=0)
    np.exp(x[1:], out=x[1:])
    x[0] = s0
    x[1:] *= s0
    return x     

# ============================================================================

def main():

    params = {"times":  12,
              "mu":     0,
              "sigma":  0.3,
              "dt":     1/12,
              "s0":     4,
              "strike": 3.7,
              "T":      np.arange(12+1)*(1/12),
              "r":      0.017}

    vanilla_call = Pricer(params=params,
                          tot_reps=13,
                          workers=4,
                          generator_func=gbm, 
                          pricing_func=price_call)

    es, err = vanilla_call.simulate()

    for i, (a, b) in enumerate(zip(es, err)):
        print("%4d|\t %4.4f +- %4.4f"%(i, a, b))

    return 0

if __name__=="__main__":
    main()
    input()
