# thanks to https://www.youtube.com/watch?v=hOiRkFBxtpo !

import typing as t


def unzip(zipped:t.Iterable) -> t.Generator[tuple, t.Any, None]: 
    for i in zip(*zipped):
        yield i


if __name__ == "__main__":
    z = list(zip(range(3), range(10, 13)))
    print(z)
    u = unzip(z)
    print(list(u))
    w = next(unzip(z))
    print(w)
    
