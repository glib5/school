import numpy as np
import matplotlib.pyplot as plt

#-------------------------------------

def mc_bb(rand, reps, steps, dt, a, b):
    '''mc over a browinian bridge'''
    ax = np.arange(steps+1)*dt
    x = np.empty(shape=(steps+1, reps))
    x[0] = 0
    x[1:] = rand.normal(scale=np.sqrt(dt), size=(steps, reps))
    np.add.accumulate(x[1:], out=x[1:], axis=0)
    last = x[-1]
    for i in range(steps+1):
        x[i] -= ax[i]*last
    if a==b and a==0:
        return x
    elif a==b and a!=0:
        return x+a
    else:
        if a!=0:
            for i in range(steps+1):
                x[i] += (1-ax[i])*a
        if b!=0:
            for i in range(steps+1):
                x[i] += ax[i]*b
        return x

#-------------------------------------------

def brownian_excursion(rand, reps, steps, dt, b_reps=1<<10):
    '''generates some brownian excursion processes'''
    be = np.empty(shape=(steps+1, reps))
    i = 0
    while i!=reps:
        bb = mc_bb(rand, b_reps, steps, dt, 0, 0)
        for j in range(b_reps):
            z = bb[:, j]
            if min(z)>=0:
                be[:, i] = z
                i += 1
            elif max(z)<=0:
                be[:, i] = -z
                i += 1
            if i==reps:
                break
    return be

#-------------------------------------

def main():
    rand = np.random.RandomState(seed=11)
    # inputs
    reps = 1<<13
    steps = 24
    dt = 1/steps
    a = 0
    b = 0
    
    x = mc_bb(rand, reps, steps, dt, a, b)
    
    mu = np.mean(x, axis=1)
    err = 3*np.sqrt(np.var(x, axis=1)/reps)
        
    v = np.var(x, axis=1)

    ax = np.arange(steps+1)*dt
    plt.figure()
    plt.errorbar(x=ax, y=mu, yerr=err)
    plt.plot([0, ax[-1]], [a, b], "r")
    plt.title("Brownian Bridge -- MC")
    if a==b and a==0:
        plt.ylim(-.1, .1)
    else:
        plt.ylim(min(a,b)*.9, max(a,b)*1.1)
    
    plt.figure()
    plt.plot(ax, v)
    plt.title("Brownian Bridge -- variance in time")

    #----------------
    
    reps = 1<<3
    steps = 240
    dt = 1/steps
    a = 0
    b = 0
    
    x = mc_bb(rand, reps, steps, dt, a, b)
    plt.figure()
    plt.plot(x, linewidth=.4)
    plt.plot([0, steps], [0, 0], "k", linewidth=.5)
    plt.title("Brownian Bridge")
    
    x = brownian_excursion(rand, reps, steps, dt)
    plt.figure()
    plt.plot(x, linewidth=.4)
    plt.plot([0, steps], [0, 0], "k", linewidth=.5)
    plt.title("Brownian Excursion")
    
    plt.show()


if __name__=="__main__":
    main()
