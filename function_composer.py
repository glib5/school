
from functools import reduce
from typing import Any, Callable


ComposableFunction = Callable[[Any], Any]

def compose(*functions: ComposableFunction) -> ComposableFunction:
    return reduce(lambda f, g: lambda x: g(f(x)), functions)




def add_two(n):
    return n+2

def main():
    
    pipeline = compose(add_two, add_two)

    result = pipeline(3)

    print(result)



if __name__ == "__main__":
    main()


