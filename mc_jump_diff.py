from time import perf_counter
import numpy as np
import matplotlib.pyplot as plt
from numba import njit


@njit
def add_jumps(reps, times, dt, lda, muj, sigmaj, P, G):
    '''returns the jump component for a diffusion process'''    
    G += muj-sigmaj*sigmaj*.5
    s = 0
    for i in range(times*reps): 
        e = int(P[i])
        a = e-s
        if a==0:
            P[i] = 0
        elif a==1:
            P[i] = G[s]
        else:
            P[i] = sum(G[s:e]) 
        s = e
    return np.reshape(P, (times, reps))

    
@njit
def mc_jdiff(reps, times, mu, sigma, dt, s0, lda, muj, sigmaj, W, P, G):
    '''returns N GMB trajs,with a jump component'''
    x = np.empty(shape=(times+1, reps))
    x[1:] = (mu-.5*sigma*sigma)*dt + W + add_jumps(reps, times, dt, lda, muj, sigmaj, P, G)
    for i in range(1, times):
        x[i+1] += x[i]
    x[0] = s0
    x[1:] = s0*np.exp(x[1:])
    return x


def MC(rand, reps, p_reps, times, mu, sigma, dt, s0, lda, muj, sigmaj):
    print("%16d"%reps)
    ES = np.zeros(shape=times+1, dtype=np.double)
    ERR = np.zeros(shape=times+1, dtype=np.double)
    # computations
    tim = perf_counter()
    for c in range(reps//p_reps):
        P = rand.poisson(lam=lda*dt, size=times*p_reps).astype(np.float64).cumsum()
        G = rand.normal(size=int(P[-1]), scale=sigmaj)
        W = rand.normal(size=(times, p_reps), scale=np.sqrt(dt)*sigma)
        x = mc_jdiff(p_reps, times, mu, sigma, dt, s0, lda, muj, sigmaj, W, P, G)
        ES += np.sum(x, axis=1)
        ERR += np.sum(x*x, axis=1)
        i = (c+1)*p_reps
        if i%(1<<20)==0:
            print("%16d -- %2.2f\r"%(i, perf_counter()-tim), end='', flush=True)
    print("\n")
    # final stats
    ES /= reps
    ERR = 3*np.sqrt( (ERR/reps - ES*ES)/reps )
    return ES, ERR


def mc_plot(dt, ES, ERR):
    '''simple MC plotting routine'''
    s0 = ES[0]
    ax = [dt*i for i in range(len(ES))]
    plt.figure()
    plt.errorbar(x=ax, y=ES, yerr=ERR)
    plt.ylim(s0*.9, s0*1.1)
    plt.plot([0, ax[-1]], [s0, s0], "r")
    plt.title("Jump Diffusion -- martingale")
    plt.show()

#---------------------------------------------------------

def main():

    # params
    rand = np.random.RandomState(seed=2789)
    
    reps = 23#int(input("log_2 of reps? "))
    if reps<10:
        reps=10
    reps = 1<<reps
    p_reps = 10#int(input("log_2 of p_reps? "))
    p_reps = 1<<p_reps
   
    times = 12
    dt = 1/12
    sigma = .3
    mu = 0
    s0 = 40
    
    lda = 5
    muj = 0
    sigmaj = .2
    
    ES, ERR = MC(rand, reps, p_reps, times, mu, sigma, dt, s0, lda, muj, sigmaj)    
    mc_plot(dt, ES, ERR)


if __name__=="__main__":
    main()
    
