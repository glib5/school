### https://pyautogui.readthedocs.io/en/latest/

from time import sleep

import pyautogui as ag


def move_always_mouse():
    '''constantly moves the mouse cursor (keeps screen alive)'''
    print("Ctrl+C or drag cursor to top left corner of the screen to exit mouse routine")
    while 1:
        ag.click(1, 1)
        ag.click(2, 1)
        sleep(60)

if __name__ == "__main__":
    move_always_mouse()