import numpy as np
import matplotlib.pyplot as plt
from time import perf_counter, sleep

def MC_pi(rand, reps):
    '''computes pi with MC'''
    x = rand.uniform(size=(2, reps))
    x *= x
    x = x[0] + x[1]
    x = np.where(x<1, 4, 0)
    mu = np.mean(x)
    err = 3*np.sqrt(np.var(x)/reps)
    return mu, err
    

def main():

    print("Computing pi with MC\n")

    rand = np.random.RandomState()
    rand.seed(51980)

    mx = 26#int(input("max log_2 of MC reps? "))
    mx += 1
    N = [1<<i for i in range(mx)]

    E = np.ndarray(shape=mx, dtype=float)
    ERR = np.ndarray(shape=mx, dtype=float)

    tim = perf_counter()
    for i in range(mx):
        e, err = MC_pi(rand, N[i])
        E[i] = e
        ERR[i] = err
        print("%4d -- %2.2f\r"%(i, perf_counter()-tim), end='', flush=True)

    # get closest value
    dist = abs(E-np.pi)
    pos = 0
    best = dist[0]
    for i in range(1, mx):
        if dist[i]<best:
            pos = i
            best = dist[i]

    # plot 
    ax = np.asarray([i for i in range(mx)])
    plt.errorbar(x=ax, y=E, yerr=ERR, label="MC")
    plt.plot([0, ax[-1]], [np.pi, np.pi], "r", label="$\pi$")
    plt.axvline(x=pos, color="k", label="closest value", linewidth=.5)
    plt.xlabel("log_2 of reps")
    plt.title("MC for pi")
##    span = .5
##    plt.ylim(np.pi-span, np.pi+span)
    plt.legend()
    plt.show()



if __name__=="__main__":
    main()

    
    
