import numpy as np
import matplotlib.pyplot as plt
from numba import njit

def one_moving_average(arr, w):
    '''one-sided moving average of an array, using a window of w values from the past'''
    return np.convolve(arr, np.ones(w), 'valid')*(1/w)

@njit
def two_moving_average(arr, k):
    '''two-sided moving average of an array, using k values around the observed one'''
    if k%2==0:
        k+=1# k needs to be odd, to have symmetry around the observed value
    fin = np.empty(shape=len(arr)-k+1)
    a = int(k/2)
    for i, t in enumerate(range((k-1-a), (len(arr)-a))):
        fin[i] = np.mean(arr[t-a:t+a+1])
    return fin

def exp_ma(arr, lda=1, look_back=1):
    '''returns exponential moving average of the array arr
    @lda -> exponential param (0->returns the data, 1->simple one-sided m.a.)
    @look_back -> obs window'''
    # default checks, any exeption returns the data
    if lda<=0 or lda>1:
        return arr
    if look_back>(len(arr)-1) or look_back<1:
        return arr
    if lda==1 and look_back==1:
        return arr
    if lda==1:
        return np.convolve(arr, np.ones(look_back), 'valid')*(1/look_back)
    fin = np.empty(shape=len(arr)-look_back+1)
    # exponential coeffs
    ldas = np.power(lda, np.arange(look_back-1, -1, -1))
    # computations
    for t in range(look_back, len(arr)+1):
        f = t-look_back
        fin[f] = np.average(a=arr[f:t], weights=ldas)
    return fin


##def exp_updated_mean(arr, lda):
##    '''returns the exponentially weighted, updated mean of arr'''
##    if lda<=0 or lda>1: return arr
##    if lda==1: return np.cumsum(arr)/np.arange(1, len(arr)+1)
##    weights = np.power(lda, np.arange(len(values)-1, -1, -1, dtype=np.double))
##    e = np.empty(shape=len(values))
##    for i in range(1, len(values)+1): e[i-1] = np.average(a=values[:i], weights=weights[-i:])
##    return e


#--------------------------------------------------------


def example(x, values):
    count = 3 # tot number of images in single plot
    
    c = 0 # idx of plot
### two sided ma
    c += 1
    plt.subplot(count, 1, c)
    plt.plot(x, values, label="values")
    h = 2
    tma = two_moving_average(values, h)
    plt.plot(x[int(h/2):-int(h/2)], tma, label="tma, 2") # careful to the X axis
    h = 10
    tma = two_moving_average(values, h)
    plt.plot(x[int(h/2):-int(h/2)], tma, label="tma, 10") # careful to the X axis
    plt.legend()
    plt.title("Two-sided moving average")
    
### one sided ma
    c += 1
    plt.subplot(count, 1, c)
    plt.plot(x, values, label="values")
    k = 2
    oma = one_moving_average(values, k)
    plt.plot(x[(k-1):], oma, label="oma 2") # careful to the X axis
    k = 10
    oma = one_moving_average(values, k)
    plt.plot(x[(k-1):], oma, label="oma 10") # careful to the X axis
    plt.legend()
    plt.title("One-sided moving average")
    
### ema
    # geometry
    arr = values
    look_back = 5
    # various lambdas
    ldas = [.1, .5, 1.0]
    c += 1
    plt.subplot(count, 1, c)
    for l in ldas:
        ema = exp_ma(arr, l, look_back)
        xe = np.arange(len(arr)-len(ema), len(arr))
        plt.plot(xe, ema, label="ema, lda=%.1f"%l)
    # plot
    xa = np.arange(len(arr))
    plt.plot(xa, arr, label="data", color="k", linewidth=.6)
    plt.legend()
    plt.title("Exponential moving average")   
    plt.show()


    



def main():

    rand = np.random.RandomState()
    rand.seed(43251)

    tot = 100
    x = np.arange(tot)
    values = np.ndarray(shape=tot)
    values[0] = 0
    values[1:] = rand.normal(size=tot-1).cumsum()

    lb = 2
    y = exp_ma(values, lda=1, look_back=lb)
    plt.plot(x, values)
    plt.plot(x[lb-1:], y, linewidth=.5)
    plt.show()
    
##    example(x, values)


        
if __name__=="__main__":
    main()       



    

