
from os import system
from pathlib import Path    
from pickle import dump as pdump, load as pload
from pprint import pprint

from xlsheet import read_xlsheet


def main():
    _ = system("cls")

    xlfile = Path(r"C:\Users\beggi\Desktop\Python\codes\xl\to_read.xlsx")
    original = [d._asdict() for d in read_xlsheet(xlfile)]

    data = [tuple(original[0].keys())]
    data.extend((tuple(d.values()) for d in original))
    data = tuple(data)

    pklfile = xlfile.with_name("data.pkl")
    with pklfile.open("wb") as f:
        pdump(obj=data, file=f)

    with pklfile.open("rb") as f:
        data2 = pload(file=f)

    pprint(data2)

if __name__ == "__main__":
    main()
