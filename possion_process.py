import numpy as np
import matplotlib.pyplot as plt

def poipr(rand, reps, periods, lda):
    '''generates a poisson process' ARRIVAL TIMES'''
    x = rand.exponential(scale=1/lda, size=(periods, reps))
    np.add.accumulate(x, axis=0, out=x)
    return x


def compensated_poipr(P, lda):
    '''takes a Poisson process and returns the compensated martingale'''
    y = np.arange(1, len(P)+1)
    return np.full(shape=P.shape, fill_value=y.reshape(len(y), -1)) - lda*P


def main():
    rand = np.random.RandomState(seed=2)

    reps = 1<<2
    lda = 5
    periods = 30

    # arrival times
    x = poipr(rand, reps, periods, lda)

    # poisson process
    y = np.arange(1, periods+1)

    # compensated process
    m = compensated_poipr(x, lda)

    plt.figure()
    plt.subplot(2, 1, 1)
    plt.step(x, y)
    plt.title("Poisson process with $\lambda=$%d"%lda)
    plt.subplot(2, 1, 2)
    plt.step(x, m)
    plt.title("compensated Poisson process with $\lambda=$%d"%lda)
    plt.show()

#---------------------------------------------

    # MC
    reps = 1<<20
    X = poipr(rand, reps, periods, lda)
    U = np.mean(X, axis=1)
    M = compensated_poipr(X, lda)
    V = np.mean(M, axis=1)

    for u,v in zip(U, V):
        print("%2.6f%20.6f"%(u, v))


if __name__=="__main__":
    main()



    
