import numpy as np
from functools import cache
from numba import njit

@cache
def rec_traverse_G(r, c):
    '''how many ways a rXc grid can be traversed from (0,0) to (r-1,c-1)'''
    if r==1 or c==1:
        return 1
    if r==2:
        return c
    if c==2:
        return r
    else:
        return rec_traverse_G(r-1, c) + rec_traverse_G(r, c-1)

@njit
def traverse_G(r, c):
    '''how many ways a rXc grid can be traversed from (0,0) to (r-1,c-1)'''
    if r<c:
        (r,c)=(c,r)
    if c==1:
        return 1
    elif c==2:
        return r
    else:
        x = np.ones(shape=r)
        for i in range(c-1):
            x = np.cumsum(x)
        return x[-1]

def ex(tot, f=True):
    R = tot
    C = tot
    for r in range(1, R+1):
        for c in range(1, C+1):
            if f:
                print("(%4d, %-4d) -> %20s"%(r, c, traverse_G(r, c)), end="\r", flush=True)
            else:
                print("(%4d, %-4d) -> %20s"%(r, c, traverse_G(r, c)))


if __name__=="__main__":
    print(rec_traverse_G(7, 21))
    print(traverse_G(7, 21))
    print()
    ex(5, False)
    print()
    ex(516, True)
    input("\nend ")

    

