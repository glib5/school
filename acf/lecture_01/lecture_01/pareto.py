#!/usr/bin/env python3

from math import *
import numpy as np 
import matplotlib.pyplot as plt
from stats import stats

def pareto_rv(Obj, N, a = 1.0, nu=10.):
    array = Obj.uniform( low = 0.0, high = 1.0, size=N)
    return a * np.power( array, -1./nu)
# -----------------------------------------------------

def pareto(a, nu, x):
    Z =     nu*pow(a,nu)
    return Z/np.power( x, 1. + nu)
#------------------------------------------------

if __name__ == "__main__":

    Obj = np.random.RandomState()
    Obj.seed(92823)

    Ns = [6, 8, 10, 12, 14, 16, 18, 20, 22]

    ############################################################
    # pareto
    ############################################################
    
    Nu = [ 3.0, 2.5, 1.9]
    a  = 1.2

    for nu in Nu:
        M  = nu*a/(nu-1)
        S2 =  (nu*a*a)/(nu-2)  - M*M
        print()
        print("Pareto Distribution")
        print("@  a = %8.4f, nu = %8.4f" %(a, nu))
        if nu > 2: print("@ E[ X ] = %8.4f, E[ (X-m)^2] = %8.4f" %(M, S2))
        else:      print("@ E[ X ] = %8.4f, \infty" %(M))

        Eps = 1.e-04
        #
        # Plot up to the point where the density
        # function is greater that eps.
        # Z/(x^(1+nu) > eps
        # x < exp( (1./(1+nu) log( Z/eps ) ) )
        #
        Z =     nu*pow(a,nu)
        Xm  = exp( (1./(1+nu)*log( Z/Eps ) ) ) 

        # compute the density function
        Dx = (Xm-a)*1.0e-03
        x  = np.arange(a, Xm+Dx, Dx)
        y  = pareto(a, nu, x)

        print("%9s    %8s %10s -- %8s   %8s -- %s " %("N", "E[X]", "E[(X-m)^2]", "|M-E[X]|", "MC-err", "Op"))
        for n in Ns:
            N = ( 1 << n )

            array = pareto_rv( Obj, N, nu = nu, a = a)
            m, s2 = stats( array )
            err   = sqrt(s2/N)
            Op    = ( abs(m-M) < 2.*err )
            print("%9d    %8.4f %10.4f -- %8.4f   %8.4f -- %s " %( N, m, s2, abs(m-M), err, Op))

            if n >= 16:
                plt.hist(array, density=True, facecolor='g', bins=x)
                plt.plot(x, y, color='r')
                plt.show()
