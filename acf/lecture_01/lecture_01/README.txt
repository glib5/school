For these examples you need
 - numpy
 - matplotlib

They can probbaly be installed with the command:

$> pip3 install numpy
$> pip3 install matplotlib

To run:

$> python3 exp_pdf.py
$> python3 linear.py
$> python3 pareto.py
