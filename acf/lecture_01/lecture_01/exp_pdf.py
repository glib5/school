#!/usr/bin/env python3

from   math import *
import numpy as np 
import matplotlib.pyplot as plt
from   stats import stats

def exp_rv(Obj, N, l=1.):
    array = Obj.uniform( low = 0.0, high = 1.0, size=N)
    array = -l* np.log( array)

    return array
# -----------------------------------------------------

def exp_pdf( l, x):
    return (1./l)*np.exp(-x/l)
#-------------------

if __name__ == "__main__":

    Obj = np.random.RandomState()
    Obj.seed(92823)

    Ns = [6, 8, 10, 12, 14, 16, 18, 20, 22]

    ############################################################
    # pareto                                                   #
    ############################################################
    
    Lambda = [ 1.2, 1.3, 1.5 ]

    for l in Lambda:
        M  = l
        S2 = l**2
        print()
        print("Exponential Distribution")
        print("@ E[ X ] = %8.4f,  E[( X - M)^2] = %8.4f" %(M, S2))

        print("%9s    %8s %10s -- %8s   %8s -- %s " %("N", "E[X]", "E[(X-m)^2]", "|M-E[X]|", "MC-err", "Op"))

        Eps = 1.e-04
        #
        # Plot up to the point where the density
        # function is greater that eps.
        # (1/l) exp(-Xm/l) > eps
        # Xm < -l*log( l*eps )
        #
        Xm  = -l * log( l*1.e-04)

        Dx = Xm * 1.e-03
        x  = np.arange(0, Xm+Dx, Dx)
        y  = exp_pdf(l, x)

        for n in Ns:
            N = ( 1 << n )
            array = exp_rv( Obj, N, l=l)
            m, s2 = stats( array )
            err   = sqrt(s2/N)
            Op    = ( abs(m-M) < 2.*err )
            print("%9d    %8.4f %10.4f -- %8.4f   %8.4f -- %s " %( N, m, s2, abs(m-M), err, Op))

            if n >= 16:
                plt.hist(array, density=True, facecolor='g', bins=x)
                plt.plot(x, y, color='r')
                plt.show()
