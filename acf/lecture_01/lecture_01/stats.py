from math import *
import numpy as np 

def stats(x):

    '''
    Given the constant array 'x', stats will return the tuple
    ( E[x], E[ (x - E[x])^2 ] )
    '''
    N = x.size
    y = np.array(x, copy=True)
    m = np.sum( y )/N

    #for n in range( y .size ): y [n] -= m
    y -= m

    var = np.dot( y , y )/N
    return m, var
# ------------------------------------------------
