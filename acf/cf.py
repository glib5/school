# course recurring functions

import numpy as np
from matplotlib.pyplot import plot, show, ylim, hist, title, legend
from sys import stdout as cout
from time import time

#---------------------------------------------------------------

def stats(array):
    "returns basic statistis of a vector of values"
    mu = np.mean(array)
    var = np.var(array)
    return mu, var

def is_in(x, mu, sd, ampl):
    "is in the confidence interval? (ampl=1,2,3)"
    if mu-sd*ampl < x < mu+sd*ampl:
        return True
    else:
        return False

#---------------------------------------------------------------

def gen_linear(a, b, N):
    "generate N linearly distr. numbers between a and b"
    Y = []
    for i in range(N):
        u = np.sqrt(np.random.uniform()*(b**2 - a**2) + a**2)
        Y.append(u)
    return Y
def linear_dens(a, b, N):
    "generates N values for the linear density (to plot)"
    x = np.linspace(a, b, N)
    f = x*(2/(b**2 - a**2))
    return x, f
# following functions do the same with other common distribs
def gen_exponential(lambd, N):
    Y = []
    for i in range(N):
        e = -lambd*np.log(np.random.uniform())
        Y.append(e)
    return Y
def exp_dens(lambd, N):
    x = np.linspace(0, 40, N)
    f = (np.exp(-x/lambd))/lambd
    return x, f

def gen_pareto(a, v, N):
    Y = []
    for i in range(N):
        p = a/(np.random.uniform()**(1/v))
        Y.append(p)
    return Y
def pareto_dens(a, v, N):
    x = np.linspace(a, 20, N)
    f = (v*a**v)/x**(1+v)
    return x, f

def gen_norm(mu, sd, N):
    Y = []
    for i in range(N):
        n = np.random.normal(mu, sd)
        Y.append(n)
    return Y
def norm_dens(mu, sd, N):
    x = np.linspace(mu-sd*4, mu+sd*4, 100)
    f = (np.exp((-(x-mu)**2)/(2*sd**2)))/(np.sqrt(np.pi*2)*sd)
    return x, f

#-----------------------------------------------------------

def eu_put(s0, k, sigma, T, r):
    "Black-Sholes european put price"
    kT = np.exp(-r*T)*k
    k_hat = kT/s0
    d = (np.log(k_hat)+ sigma*sigma*0.5*T)/sigma
    N1 = norm.cdf(d/np.sqrt(T))
    N2 = norm.cdf((d-sigma*T)/np.sqrt(T))
    return kT*N1 - s0*N2

def eu_call(s0, k, sigma, T, r):
    "Black-Sholes european call price"
    kT = np.exp(-r*T)*k
    k_hat = kT/s0
    d = (np.log(k_hat)+ sigma*sigma*0.5*T)/sigma
    N1 = norm.cdf(d/np.sqrt(T))
    N2 = norm.cdf((d-sigma*T)/np.sqrt(T))
    return kT*(N1-1) + s0*(1-N2)

def eu_call(s0, k, sigma, T, r):
    '''call (parity) price'''
    call = s0 - k*np.exp(-r*(T)) + eu_put(s0, k, sigma, T, r)
    return call

def mc_eu_put(So, k, sigma, T, r, reps):
    "returns a vector of put payoffs"
    W = np.random.normal(loc=0, scale=np.sqrt(T), size=reps)
    ST = So*np.exp(-0.5*sigma*sigma*T + sigma*W)
    kT = k*np.exp(-r*T)
    payoff = kT - ST
    payoff = np.where(payoff>0.0, payoff, 0.0)
    return payoff

def mc_eu_call(So, k, sigma, T, r, reps):
    "returns a vector of call payoffs"
    W = np.random.normal(loc=0, scale=np.sqrt(T), size=reps)
    ST = So*np.exp(-0.5*sigma*sigma*T + sigma*W)
    kT = k*np.exp(-r*T)
    payoff = ST - kT
    payoff = np.where(payoff>0.0, payoff, 0.0)
    return payoff

#-------------------------------------------------

def gen_lognorm(lenght, s0, dt, sigma):
    "creates a log-normal trajectory (no drift)"
    values = [s0]
    for i in range(lenght):
        values.append(values[i]*np.exp((-sigma**2/2)*dt + sigma*np.sqrt(dt)*np.random.normal()))
    return values

def Jump(lamb, rand, mu=0, sd=1):
    '''gaussian jumps process'''
    numj = rand.poisson(lamb)
    # change jump shape here
    j = np.sum(rand.normal(loc=mu, scale=sd, size=numj))
    return j

def vcvm(dim):
    '''returns a random variance-covariance
    matrix (square, symmetric) of given size'''
    x=np.matrix(np.random.uniform(size=(dim, dim)))
    xt=x.getT()
    return x*xt

def md_gauss(decomp, reps):
    # get correct size
    dim = decomp.shape[0]
    # create values for mc
    X = np.random.normal(loc=0, scale=1, size=(dim, reps))
    Y = np.matmul(decomp, X)
    return Y.getT()

#----------------------------------------

def gen_heston(v0, x0, periods, rand, lam, v_bar, dt, eta, r, q, rho, s0):
    '''returns the price process according to the Heston model'''
    V = [v0]
    X = [x0]
    for i in range(periods):
        v = V[-1]
        y = rand.normal()
        nxtv = v + lam*(v_bar - v)*dt + eta*np.sqrt(v*dt)*y
        if nxtv<0.0:
            nxtv=0
        V.append(nxtv)

        x = X[-1] + (r-q-v/2)*dt + np.sqrt(v*dt)*(rho*y + np.sqrt(1-rho*rho)*rand.normal())
        X.append(x)

    S = s0*np.exp(X)
    return S

#---------------------------


    
