import numpy as np
from time import time, sleep

#4.1.1

#4.1.2

#4.1.3

### mc with cholesky
def vcvm(dim):
    '''returns a random variance-covariance
    matrix (square, symmetric)of given size'''
    x=np.matrix(np.random.uniform(low=0, high=1, size=(dim, dim)))
    xt=x.getT()
    return x*xt

def md_gauss(decomp, reps):
    # get correct size
    dim = decomp.shape[0]
    # create values for mc
    X = np.random.normal(loc=0, scale=1, size=(dim, reps))
    Y = np.matmul(decomp, X)
    return Y.getT()

if __name__=="__main__":
    t = time()
    dim = 3
    np.random.RandomState()
    np.random.seed(2)
    # generate a random vcv matrix
    X = vcvm(dim)
    # decomposition
    Chol = np.linalg.cholesky(X)
    # do the montecarlo
    # it uses the decomposition to simulate a matrix like 'X'
    reps = int(input("log_2 of reps? "))
    reps = 1<<reps
    arr = md_gauss(Chol, reps)
    mu = 0
    sd = 0
    for n in range(reps):
        x = np.matmul(arr[n].getT(), arr[n])
        mu += x
        sd += x*x
        print("%8d/%d\r"%(n, reps), end='', flush=True)
    mu *= 1/reps
    sd *= 1/reps
    err = np.sqrt(sd/reps)

    print("\nX\n%s"%str(X))
    print("\nmc values for X:\n%s" %str(mu))
    print("\nmc err\n%s"%str(err))
    print("\nabsolute error\n%s"%(str(X-mu)))
    print("\ntime: %8.4f" %(time()-t))
    
    print("\n\nclosing in 1 minute")
    sleep(60)
