import numpy as np
from matplotlib.pyplot import plot, title, show

#------------------------------------------------------

def gen_traj(lenght, s0, dt, sigma):
    "creates a log-normal trajectory (no drift)"
    values = [s0]
    for i in range(lenght):
        values.append(values[i]*np.exp(((-sigma**2)/2)*dt + sigma*np.sqrt(dt)*np.random.normal()))
    return values

################################# MAIN ######################################

# set seed
np.random.RandomState()
np.random.seed(29233)

# parameters
starting_price = 10
length = 12 # months
dt = 1/length
sigma = 0.3
ampl = 1 # amplitude of the confidence interval resulting (1, 2, 3)
reps = 500 # number of repetitons

trajs = []
for i in range(reps):
    trajs.append(gen_traj(length, starting_price, dt, sigma))

means=[]
bound_mu=[]
bound_md=[]
variances=[]
for pos in range(0, length+1):
    val = []
    for trj in trajs:
        val.append(trj[pos])
    m = np.mean(val)
    v = np.var(val)
    means.append(m)
    err = np.sqrt(v/reps)
    bound_mu.append(m + err*ampl)
    bound_md.append(m - err*ampl)
    variances.append(v)

plot(means)    
ax, ay = [0, length], [starting_price, starting_price]
plot(ax, ay, "k")
plot(bound_mu, "r")
plot(bound_md, "r")
title("mean")
show()

plot(variances)
ax = np.linspace(0, length, 50)
ay = starting_price**2 * ((np.exp(ax*sigma**2)) - 1)
plot(ax, ax, "k")
title("variance")
show()
