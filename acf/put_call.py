import numpy as np
from scipy.stats import norm
from time import time


#------------------
# set seed
np.random.RandomState()
np.random.seed(2)

#------------
start_time = time()

reps = 1<<14

# 2.4.1 -------------------------------
#params
So = 1
k = 1
sigma = 0.4
T = 1
r = 0.0
def eu_put(So, k, sigma, T, r):
    kT = np.exp(-r*T)*k
    k_hat = kT/So
    d = (np.log(k_hat)+ sigma*sigma*0.5*T)/sigma
    N1 = norm.cdf(d/np.sqrt(T))
    N2 = norm.cdf((d-sigma*T)/np.sqrt(T))
    return kT*N1 - So*N2
print("%-24s %8.4f" %("put theory ", eu_put(So, k, sigma, T, r)))


# 2.4.2 -------------------------------
def mc_eu_put(So, k, sigma, T, r, reps):
    W = np.random.normal(loc=0, scale=np.sqrt(T), size=reps)
    ST = So*np.exp(-0.5*sigma*sigma*T + sigma*W)
    kT = k*np.exp(-r*T)
    payoff = kT - ST
    payoff = np.where(payoff>0.0, payoff, 0.0)
    return payoff

j = mc_eu_put(So, k, sigma, T, r, reps)
price = np.mean(j)
v = np.mean(j**2)
mc_err = np.sqrt((v-price*price)/reps)
print("%-24s %8.4f" %("mc put price ", price))
print("%-24s %8.4f" %("mc err ", mc_err))
print()

# 2.4.3 -------------------------------
def eu_call(So, k, sigma, T, r):
    kT = np.exp(-r*T)*k
    k_hat = kT/So
    d = (np.log(k_hat)+ sigma*sigma*0.5*T)/sigma
    N1 = norm.cdf(d/np.sqrt(T))
    N2 = norm.cdf((d-sigma*T)/np.sqrt(T))
    return kT*(N1-1) + So*(1-N2)
print("%-24s %8.4f" %("call theory ", eu_call(So, k, sigma, T, r)))


# 2.4.4 -------------------------------
def mc_eu_call(So, k, sigma, T, r, reps):
    j = np.zeros(reps)
    for i in range(reps):
        ST = So*np.exp(-0.5*sigma*sigma*T + sigma*np.random.normal(loc=0, scale=np.sqrt(T)))
        kT = k*np.exp(-r*T)
        payoff = ST - kT
        j[i] = np.where(payoff>0.0, payoff, 0.0)
    return j

j = mc_eu_call(So, k, sigma, T, r, reps)
price = np.mean(j)
v = np.mean(j**2)
mc_err = np.sqrt((v-price*price)/reps)
print("%-24s %8.4f" %("mc call price ", price))
print("%-24s %8.4f" %("mc err ", mc_err))
print()

# 2.4.5 -------------------------------
# call = So - kT + put

kT = k*np.exp(-r*T)
j = np.zeros(reps)

for i in range(reps):
    j[i] = eu_put(So, k, sigma, T, r) + So - kT

price = np.mean(j)
print("%-24s %8.4f" %("mc call (parity) price ", price))
v = np.mean(j**2)
print("%-24s %8.4f" %("mc err ", np.sqrt((v-price*price)/reps)))
print()

# 2.4.6 -------------------------------
# cap and floor
def cf_payoff(ST, floor, cap, r, T, t=0):
    floor = floor*np.exp(-r*(T-t))
    cap = cap*np.exp(-r*(T-t))
    if ST < floor:
        return floor
    elif ST > cap:
        return cap
    else:
        return ST
#params
cap = 1.1
floor = 0.7

j = np.zeros(reps)

for i in range(reps):
    ST = So*np.exp(-0.5*sigma*sigma*T + sigma*np.random.normal(loc=0, scale=np.sqrt(T)))  
    j[i] = cf_payoff(ST, floor, cap, 0, T)
price = np.mean(j)
v = np.mean(j**2)
mc_err = np.sqrt((v-price*price)/reps)
print("%-24s %8.4f" %("cap-floor mc price ", price))
print("%-24s %8.4f" %("mc err ", mc_err))
print()

#--------------------
end_time = time()
print("seconds: ", end_time-start_time)

input("end")
