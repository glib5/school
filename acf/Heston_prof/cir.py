#!/usr/bin/env python3

import sys
from sys import stdout as cout
from math import *
import numpy as np
from config import get_input_parms
import matplotlib.pyplot as plt
from time import time
# -----------------------------------------------------

def mc_cir_n( rand, cir, L, dt, Nt, DT, N):
    '''
    @params rand: random number generator
    @params cir :  the CIR object
    @params L   :  number of steps for the cir trajectory simulation
    @params dt  :  length of the step for the cir trajectory simulation
    @params Nt  :  number of steps of the output trajectory
    @params DT  :  length of the step for the output trajectory
    @params N   :  number of trajectories
    '''

    zero = np.ndarray(shape = ( L+1 ), dtype=np.double )
    Z  = np.ndarray(shape = ( L+1, N ), dtype=np.double )
    Int  = np.ndarray(shape = ( L+1, N ), dtype=np.double )
    xi = rand.normal( loc = 0.0, scale = 1.0, size=(L, N))

    s   = cir.sigma
    th  = cir.theta
    k   = cir.kappa
    ro  = cir.ro

    Z[0]   = ro
    Int[0] = 0.0
    zero = 0.0

    for n in range(L):
        Ro = Z[n]
        Rn = Ro + k*(th - Ro)*dt + s*np.sqrt( Ro*dt)*xi[n]
        Io = Int[n]
        Rn = np.maximum(Rn,zero)
        Z[n+1] = Rn
        Int[n+1] = Io + .5*(Rn+Ro)*dt


    #print("dt: %8.4f, Dt: %8.4f" %(dt, DT))
    X  = np.ndarray(shape = ( Nt+1, N ), dtype=np.double ) # Y[Nt+1, 2]
    I  = np.ndarray(shape = ( Nt+1, N ), dtype=np.double ) # Y[Nt+1, 2]

    for n in range(Nt+1):
        tn = n * DT
        pos = int(tn/dt)
        X[n] = Z[pos]
        I[n] = Int[pos]

    return (X, I) 
# ----------------------------------------

class CIR:

    def __init__(self, **kwargs):
        self.kappa = kwargs["kappa"] 
        self.sigma = kwargs["sigma"] 
        self.theta = kwargs["theta"] 
        self.ro    = kwargs["ro"] 
        self.gamma = sqrt( self.kappa * self.kappa + 2 * self.sigma*self.sigma)
    # --------------

    def B(self, t):
        g = self.gamma
        k = self.kappa
        h = exp(g*t) - 1
        return 2 * h/( (g+k)*h + 2*g)
    # ------------------------

    def A(self, t):
        g = self.gamma
        k = self.kappa
        th= self.theta
        s = self.sigma
        h = exp(g*t) - 1
        return ( 2*k*th/(s*s) ) * log( ( 2 * g * exp(.5 * (k+g)*t) )/( (g+k)*h + 2*g ))
    # --------------------------------------

    def P_tT( self, t, r=None):
        if r == None: r = self.ro
        return exp( -self.B(t)*r + self.A(t) )
        
# ----------------------------------------

def usage():
    print("Usage: ./main.py -in input_file [ -- help ] [-out out_file] [ -nr exp] [-y year] [-nt intrvls] [-k kapp] [-s sigma] [-th theta] [ -dt dt]")
    print("        input_file: the input file holding interest rates")
    print("        exp       : log base 2 of the number of MC trajectories")
    print("                    the number of MC trajectories will be 2^exp.")
    print("                    If undefined will default to 10")
    print("        year      : the number of years of teh simulation")
    print("                    defaults to 10")
    print("        intrvl    : the number of intervals we are going to measure")
    print("                    defaults to 30")
    print("        kappa     : the kappa paramter of the CIR model")
    print("                    defaults to .5")
    print("        theta     : the theta parameter of the CIR model")
    print("                    defaults to .1")
    print("        sigma     : the vol paramter of the CIR model")
    print("                    defaults to .05")
    print("        dt        : the step for the CIR model")
    print("                    defaults to 1day")
#-------------


def run(argv):

    # initial values
    nr    = 10
    kappa = .5
    theta = .1
    sigma = .05
    ro    = .01
    Yrs    = 10.
    Nt    = 30
    dt    = 1./(365.)
    out   = "cir.txt"

    parms = get_input_parms(argv)

    try:
        Op = parms["help"]
        usage()
        return
    except KeyError:
        pass

    try: out = parms["out"]
    except KeyError: pass
    fp  = cout

    try: nr = int(parms["nr"] )
    except KeyError: pass
    N = (1 << nr)

    try: kappa = float(parms["k"] )
    except KeyError: pass

    try: theta = float(parms["th"] )
    except KeyError: pass

    try: sigma = float(parms["s"] )
    except KeyError: pass

    try: ro = float(parms["ro"] )
    except KeyError: pass

    try: Yrs = float(parms["y"] )
    except KeyError: pass

    try: Nt = int(parms["nt"] )
    except KeyError: pass
    Dt = Yrs/Nt

    try: dt = int(parms["dt"] )
    except KeyError: pass

    feller = 2*kappa*theta - sigma*sigma
    if feller > 0.0: OP_feller = "True"
    else: OP_feller = "False"
    print("@ %-12s: Feller = %8.4f %s" %("Info", feller, OP_feller) )

    # geometry
    nCir = int(Yrs/dt)
    print("dt: %8.6f CIR steps: %d T=%10.6f" %(dt, nCir, nCir*dt))

    dt = float(Yrs/nCir)
    print("dt: %8.6f CIR steps: %d T=%10.6f" %(dt, nCir, nCir*dt))
    print("Dt: %8.6f trj steps: %d T=%10.6f" %(Dt, Nt, Nt*Dt))

    Obj = np.random.RandomState()
    Obj.seed(92823)
    # -------------------------------

    # the HW model
    cir = CIR(kappa=kappa, sigma=sigma, theta=theta, ro = ro)

    tStart = time()
    # Begin MC
    P_tT  = 0.0
    P2_tT = 0.0
    n = N

    '''
    while n > 0:
        res = mc_cir( Obj, cir, nCir, dt, Nt, Dt)
        P_tT  += np.exp( -res[1] )/N
        P2_tT += np.exp( -2*res[1] )/N
        n -= 1
        cout.write("%4d\r" %(n))
        cout.flush()
    '''
    X, I = mc_cir_n( Obj, cir, nCir, dt, Nt, Dt, N)
    tEnd = time()
    cout.write("@ %-12s: %4d  %8.4f sec.\n" %("Iter", N, tEnd - tStart))

    p_tT  = np.exp( -I )
    P_tT  = np.add.reduce(p_tT, 1)/N

    p2_tT = np.exp( -2*I )
    P2_tT = np.add.reduce(p2_tT, 1)/N

    Err = 3*np.sqrt((P2_tT - P_tT*P_tT)/N)
    # End MC

    P_0T  = np.ndarray(shape = ( Nt+1 ), dtype=np.double )
    for n in range(Nt+1):
        P_0T[n] = cir.P_tT(n*Dt)
        fp.write( "%8.4f] %8.4f +/- %5.3f  %6.4f\n" %( n*Dt, P_tT[n], Err[n], P_0T[n]))

    
    # builds the array t = [0, 1, 2, ... , Nt]
    t  = np.arange(0, Nt+1, 1, dtype = np.double)

    # the array t is modified as follows: t = [ 0, Dt, 2*Dt, ...,  Nt*Dt]
    t *= Dt

    # plot MC vs discount curve ....
    plt.errorbar(t, P_tT, yerr=Err, fmt='x', color='g', label="MC")
    plt.title("CIR --  $r_o$=%4.2f  $\kappa$=%4.2f  $\sigma$=%5.2f  $\\theta$=%5.2f  Mc=$2^{%d}$ " %(ro, kappa, sigma, theta, nr))
    plt.ylim(top=1.2, bottom=.2)
    plt.xlabel("Years")
    plt.ylabel("P_0t")

    # plots the analytical value of the CIR curve
    plt.plot( t, P_0T, color='r', label="CIR curve")
    plt.legend(loc="best")

    plt.show()

    fp.close()
    
# -------------------------------


if __name__ == "__main__":
    run(sys.argv)

