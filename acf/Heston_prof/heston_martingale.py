#!/usr/bin/env python3

import sys
from sys import stdout as cout
from math import *
import numpy as np
from config import get_input_parms
import matplotlib.pyplot as plt
from cir import CIR,  mc_cir_n
from heston_evol import mc_heston
from time import time
# -----------------------------------------------------

def usage():
    print("Usage: ./main.py -in input_file [ -- help ] [-out out_file] [-nv ev] [ -ns es] [-y year] [-nt intrvls] [ -dt dt]")
    print("                                [-eta eta] [-k lambda] [-th nubar] [ -ro nu] [ -rho rho]")
    print("        input_file: the input file holding interest rates")
    print("        ev        : log base 2 of the number of vol trajectories")
    print("                    defaults to 10")
    print("        es        : log base 2 of the number of MC trajectories per vol trajectory")
    print("                    If undefined will default to 10")
    print("        year      : the number of years of the simulation")
    print("                    defaults to 1")
    print("        intrvl    : the number of intervals we are going to measure")
    print("                    defaults to 12")
    print("        dt        : the step for the CIR model")
    print("                    defaults to 1day")
    print(" ")
    print("        eta       : the vol of the vol process")
    print("                    defaults to .01")
    print("        lambda    : the kappa paramter in the cooresponding CIR model")
    print("                    defaults to .1153")
    print("        nubar     : the theta parameter in the corresponding CIR model")
    print("                    defaults to .024")
    print("        nu        : the initial value for vol of vol")
    print("                    defaults to .0635")
    print("        rho       : correlation between vol and underlying wiener processes")
    print("                    defaults to .2125")
#-------------

def run(argv):

    dd = 1./365.
    hh = dd/24.
    mm = hh/60.

    # initial values
    nv    = 10
    dt    = dd

    ns    = 10
    Yrs   = 1.
    Nt    = 12
    # --------------------------

    out   = "heston.txt"

    eta   = .01
    nubar = .024
    lmbda = .1153
    nu_o  = .0635
    rho   = 0.2125

    parms = get_input_parms(argv)

    try:
        Op = parms["help"]
        usage()
        return
    except KeyError:
        pass

    try: out = parms["out"]
    except KeyError: pass

    try: nv = int(parms["nv"] )
    except KeyError: pass

    try: ns = int(parms["ns"] )
    except KeyError: pass

    try: Yrs = float(parms["y"] )
    except KeyError: pass

    try: Nt = int(parms["nt"] )
    except KeyError: pass

    try: dt = int(parms["dt"] )
    except KeyError: pass
    # -----------------------------------------

    try: eta = float(parms["eta"] )
    except KeyError: pass

    try: lmbda = float(parms["k"] )
    except KeyError: pass

    try: nubar = float(parms["th"] )
    except KeyError: pass

    try: nu_o = float(parms["ro"] )
    except KeyError: pass

    try: rho = float(parms["rho"] )
    except KeyError: pass

    fp  = open(out,"w")
    NV = (1 << nv)
    NS = (1 << ns)
    Dt = Yrs/Nt

    feller = 2*lmbda*nubar - eta*eta
    if feller > 0.0: OP_feller = "True"
    else: OP_feller = "False"
    print("@ %-12s: Feller = %8.4f %s" %("Info", feller, OP_feller) )

    # geometry
    nCir = int(Yrs/dt)
    dt   = float(Yrs/nCir)

    Obj = np.random.RandomState()
    Obj.seed(29283)
    # -------------------------------


    # the HW model
    cir = CIR(kappa=lmbda, sigma=eta, theta=nubar, ro = nu_o)

    tStart = time()
    J = NV*NS
    vol, Ivol = mc_cir_n( Obj, cir, nCir, dt, Nt, Dt, NV)
    Ivol = Ivol.transpose()
    vol = vol.transpose()
    for n in range(NV):
        S = mc_heston( Obj, 1., vol[n], Ivol[n], cir, rho, Dt, NS )
        Savg = np.add.reduce(S,1)/NS

        if n == 0: 
            ES  = Savg/NV
            VAR = Savg*Savg/NV
        else:
            ES = ES + Savg/NV
            VAR = VAR + Savg*Savg/NV

        tEnd = time()
        cout.write("%6d  (%10d)   %8.4f sec.\r" %(n, NS*n, tEnd - tStart))
        cout.flush()

    tEnd = time()
    cout.write("%6d  (%10d)   elapsed %8.4f sec.\n" %(NV, NS*NV, tEnd - tStart))
    Err = 2*np.sqrt((VAR-ES*ES)/NV)
    # End MC

    
    # builds the array t = [0, 1, 2, ... , Nt]
    t  = np.arange(0, Nt+1, 1, dtype = np.double)

    # the array t is modified as follows: t = [ 0, Dt, 2*Dt, ...,  Nt*Dt]
    t *= Dt
    # ------------------------------

    print("\n%6s  %6s  %6s" %("t","E[S]","Err"))
    for x,y,z in zip(t,ES, Err):
        print("%6.3f  %6.4f  %6.4f" %(x,y,z))
    # ------------------------------

    # plot MC results
    plt.errorbar(t, ES, yerr=Err, fmt='x', color='g', label="MC $2^{%d+%d}$" %(nv, ns))
    plt.title("Martingale Test -- Heston Model\n$\\nu_o$=%4.2f  $\lambda$=%4.2f  $\eta$=%5.2f  $\overline{\\nu}$=%5.2f" %(nu_o, lmbda, eta, nubar))
    plt.ylim(top=1.1, bottom=.9)
    plt.xlabel("Years")
    plt.ylabel("E[S(t)]")

    # martingale check
    Cte = np.full(shape = Nt+1, fill_value=1., dtype=np.double)
    plt.plot( t, Cte, color='r', label="S_0")
    plt.legend(loc="best")

    plt.show()


    fp.close()
    
# -------------------------------

def test():
    NR = 3
    NC = 2
    S  = np.ndarray(shape = (NC, NR), dtype=np.double ) # S[NR, NC] in fortran matrix notation

    for n in range(NC): S[n] = n * 1.1
    print ( S )

    A = np.add.reduce(S, 0)
    B = np.add.reduce(S, 1)

    print(A)
    print(B)
# ---------------------

if __name__ == "__main__":
    #test()
    run(sys.argv)
