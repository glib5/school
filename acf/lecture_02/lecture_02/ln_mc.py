#!/usr/bin/env python3

from sys import stdout as cout
from math import *
from stats import stats
import numpy as np
import matplotlib.pyplot as plt

def mc(Obj, N, So, T, J, sigma):

    DT = T/N
    S = np.ndarray(shape = ( N+1, J), dtype=np.double)

    for j in range( S[0].size ): 
        S[0][j] = So
        X = Obj.normal( loc = -.5*sigma*sigma*DT, scale = sigma*sqrt(DT), size=N)
        for n in range(N):
            S[n+1][j] = S[n][j] * exp( X[n] )
        cout.write("Iter %d\r" %(j+1))
        cout.flush()

    cout.write("Iter %d\n" %(S[0].size))
    res = {"t" : np.ndarray(shape = (N+1,), dtype=np.double)
          ,"m" : np.ndarray(shape = (N+1,), dtype=np.double)
          ,"S2": np.ndarray(shape = (N+1,), dtype=np.double)
          }

    for n in range(N+1):
        X    = S[n]
        m,s2 = stats(X)
        res["t"][n]  = n*DT
        res['m'][n]  = m
        res["S2"][n] = s2

    return res
# -----------------------------------------------------

def run(J=10):

    Obj = np.random.RandomState()
    Obj.seed(92823)

    So     = 1.5
    T      = 3.0
    sigma  =  .60

    # The time step is 2-months
    N      = int( T * 6)
    St     = np.full(shape = (N+1,), fill_value=So, dtype=np.double)


    ex     = "2^%d" %J
    print()
    print("@ %-12s %8s"   %("J", ex))
    print("@ %-12s %8.4f" %("So", So))
    print("@ %-12s %8.4f" %("T", T))
    print("@ %-12s %8.4f" %("sigma", sigma))

    j   = ( 1 << J )
    res = mc(Obj, N, So, T, j, sigma)

    print("\n")
    print("%8s  %8s   %8s -- %8s" %( "t", "E[S(t)]", "McErr", "ThErr" ))
    for n in range( res["t"].size):
        t = res["t"][n]

        # the theoretica error in 't'.
        ThErr = So*sqrt( (exp( sigma*sigma*t) -1)/j )
        print("%8.4f  %8.4f   %8.4f -- %8.4f " %( t, res["m"][n], sqrt(res["S2"][n]/j), ThErr ))

    err = 3.*np.sqrt( res["S2"]/j)
    lbl = "* Mc: E[ S(t) ], J=%d" %j
    
    # the lower value of the y-axis
    Ym  = So*(1. - .3)

    # the highest value of the y-axis
    YM  = So*(1. + .3)

    # spacing between lines
    Dy  =  (YM-Ym)/20.

    Xl  = Dy
    Yl  = YM-Dy

    plt.ylim(Ym, YM)
    plt.errorbar(res["t"], res["m"], yerr=err, fmt='x', color='g')
    plt.text(Xl, Yl, lbl, color='g')
    Yl -= Dy

    plt.plot(res["t"], St, color='r')
    plt.text(Xl, Yl, "* So", color='r')

    plt.show()
# --------------------------

if __name__ == "__main__":

    J = [10, 12, 14, 16, 18]

    for j in J:
        run(J=j)
