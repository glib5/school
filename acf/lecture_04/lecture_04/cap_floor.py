#!/usr/bin/env python3
import sys
from sys import stdout as cout
from scipy.stats import norm
from math import *
import numpy as np
from time import time, sleep
from stats import stats
from config import get_input_parms

def mc(N, So, r, T, sigma, Bl, Bh):
    Obj = np.random.RandomState()
    Obj.seed(92823)
    lT   = exp(-r*T)*Bl
    hT   = exp(-r*T)*Bh

    X = Obj.normal( loc = -.5*sigma*sigma*T, scale = sigma*sqrt(T), size=N)

    # the payoff:
    S      = So*np.exp(X)
    S      = np.where( S > lT, S, lT)
    S      = np.where( S > hT, hT, S)

    return S
# -------------------------------------

def cap_floor( So, r, T, sigma, Bl, Bh):
    """theorical price of a cap-floor"""
    BlT   = exp(-r*T)*Bl
    BhT   = exp(-r*T)*Bh

    dl = ( log( BlT/So) + .5*sigma*sigma*T)/sigma
    dh = ( log( BhT/So) + .5*sigma*sigma*T)/sigma
    res = BlT*norm.cdf(dl/sqrt(T))
        + BhT*( 1. - norm.cdf(dh/sqrt(T)) )
        + So *( norm.cdf((dh-sigma*T)/sqrt(T))
        - norm.cdf((dl-sigma*T)/sqrt(T))  )
    return res
# -----------------------------------------------------

def usage():
    print("Usage: $> python3 euro_opt.py [options]")
    print("Options:")
    print("    %-24s: this output" %("--help"))
    print("    %-24s: output file" %("-out"))
    print("    %-24s: initial value of the underlying" %("-So"))
    print("    %-24s: option maturity" %("-T"))
    print("    %-24s: interest rate" %("-r"))
    print("    %-24s: volatility" %("-s"))
    print("    %-24s: low barrier" %("-Bl"))
    print("    %-24s: high barrier" %("-Bh"))
    print("    %-24s: log_2 of the number of iteration ( <= 30 ) " %("-n"))


def run(argv):

    output = None
    So     = 1.2
    T      = 2.0
    r      = 0.01
    sigma  =  .40
    Bl     = 0.9
    Bh     = 1.3
    n      = 20
    parms  = get_input_parms(argv)
    print("@ %-12s: %s" %("argv", str(argv)));
    print("@ %-12s: %s" %("parms", str(parms)));

    try:
        Op = parms["help"]
        usage()
        return
    except KeyError:
        pass

    try:
        output = parms["out"]
        fp = open(output, "w")
    except KeyError:
        fp = cout

    try: So = float( parms["So"] )
    except KeyError: pass

    try: T = float( parms["T"] )
    except KeyError: pass

    try: r  = float( parms["r"] )
    except KeyError: pass

    try: sigma  = float( parms["s"] )
    except KeyError: pass

    try: Bl = float( parms["Bl"] )
    except KeyError: pass

    try: Bh = float( parms["Bh"] )
    except KeyError: pass

    try: n = int( parms["n"] )
    except KeyError: pass

    if Bl >= Bh:
        raise Exception("Error: Bl (%6.4f) should be < Bh (%6.4f)\n\n"  %(Bl, Bh))


    fp.write("@ %-12s %8.4f\n" %("So", So))
    fp.write("@ %-12s %8.4f\n" %("Bl", Bl))
    fp.write("@ %-12s %8.4f\n" %("Bh", Bh))
    fp.write("@ %-12s %8.4f\n" %("T" , T))
    fp.write("@ %-12s %8.4f\n" %("r" , r))
    fp.write("@ %-12s %8.4f\n" %("sigma", sigma))

    M = cap_floor( So, r, T, sigma, Bl, Bh)
    fp.write("@ %-12s %8.6f\n" %("M", M))

    fp.write("\n")
    fp.write("    %8s %8s %3s %8s\n" %( "N", "E[X]", "+/-", "err"))
    N    = ( 1 << n )

    start = time()
    array = mc(N, So, r, T, sigma, Bl, Bh)
    m,s2  = stats(array)
    err   = 3.0*sqrt(s2/N)

    fp.write("    %8d %8.6f %3s %8.6f\n" %( N, m, "+/-", err))
    end = time()
    fp.write("@ %-12s: %8.4f sec.\n" %("Elapsed", (end-start)) )

    if output != None: 
        print("@ output written to: '%s'" %output)
        fp.close()
   
# --------------------------

if __name__ == "__main__":

    run(sys.argv)

    sleep(15)
