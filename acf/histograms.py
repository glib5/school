import numpy as np
from matplotlib.pyplot import hist, plot, title, show

def stats(array):
    "returns basic statistis of a vector of values"
    mu = np.mean(array)
    var = np.var(array)
    return mu, var

def is_in(x, mu, sd, l):
    "tells you if x is in the confidence interval (ampl is 1, 2, 3)"
    if mu-sd*ampl < x < mu+sd*ampl:
        return True
    else:
        return False

def gen_linear(a, b, N):
    "generate N linearly distr. numbers between a and b"
    Y = []
    for i in range(N):
        u = np.sqrt(np.random.uniform()*(b**2 - a**2) + a**2)
        Y.append(u)
    return Y

def linear_dens(a, b, N):
    "generates N values for the linear density (to plot)"
    x = np.linspace(a, b, N)
    f = x*(2/(b**2 - a**2))
    return x, f

# following functions do the same with other distributions

def gen_exponential(lambd, N):
    Y = []
    for i in range(N):
        e = -lambd*np.log(np.random.uniform())
        Y.append(e)
    return Y

def exp_dens(lambd, N):
    x = np.linspace(0, lambd*10, N)
    f = (np.exp(-x/lambd))/lambd
    return x, f

def gen_pareto(a, v, N):
    Y = []
    for i in range(N):
        p = a/(np.random.uniform()**(1/v))
        Y.append(p)
    return Y

def pareto_dens(a, v, N):
    x = np.linspace(a, a*5, N)
    f = (v*a**v)/x**(1+v)
    return x, f

def gen_norm(mu, sd, N):
    Y = []
    for i in range(N):
        n = np.random.normal(mu, sd)
        Y.append(n)
    return Y

def norm_dens(mu, sd, N):
    x = np.linspace(mu-sd*4, mu+sd*4, 100)
    f = (np.exp((-(x-mu)**2)/(2*sd**2)))/(np.sqrt(np.pi*2)*sd)
    return x, f

################################## MAIN ##################################

#set random seed
np.random.RandomState()
np.random.seed(2)

# number of repetitions(for the plot)
reps2 = [1<<14] # 4**11 is about 4 millions

# number of repetitions (for montecarlo)
reps = []
for i in range(4, 9): # increase upper bound for better proxy
    reps.append(4**i)
#bins (and number of points to plot for the density functions)
bins = 100
# sets the amplitude of the confidence interval (1, 2, 3), see is_in function
ampl = 2

# es 1.4.1 (linear density) in [a, b]
a = 2
b = 5
# plot
x, y = linear_dens(a, b, bins)
for r in reps2:
    Y = gen_linear(a, b, r)
    plot(x, y)
    hist(Y, bins=bins, density=True)
    title("linear")
    show()
# montecarlo
real_mean = (2/3)*((b**3-a**3)/(b**2-a**2))
real_var = (2/(b**2-a**2))*(b**4/4-a**4/4) - real_mean**2
print("\nLinear\nE[X]=", real_mean, "\nVar(X)=", real_var)
for r in reps:
    Y = gen_linear(a, b, r)
    m, var = stats(Y)
    err = np.sqrt(var/r)
    m_err = abs(real_mean - m)
    print("%8.4d %8.4f %8.4f %8.4f %8.4f %-4s" %(r, m, var, err, m_err, is_in(real_mean, m, err, ampl)))

# es 1.4.2 (exponential density)
lambd = 3
#plot
x, y = exp_dens(lambd, bins)
for r in reps2:
    Y = gen_exponential(lambd, r)
    plot(x, y)
    hist(Y, bins=bins, density=True)
    title("exponential")
    show()
# montecarlo
print("\nExponential\nE[X]=", lambd, "\nVar(X)=", lambd**2)
for r in reps:
    Y = gen_exponential(lambd, r)
    m, var = stats(Y)
    err = np.sqrt(var/r)
    m_err = abs(lambd - m)
    print("%8.4d %8.4f %8.4f %8.4f %8.4f %-4s" %(r, m, var, err, m_err, is_in(lambd, m, err, ampl)))
    
# es 1.4.4 (pareto)
a = 30
v = 8
# plot
x, y = pareto_dens(a, v, bins)
for r in reps2:
    Y = gen_pareto(a, v, r)
    plot(x, y)
    hist(Y, bins=bins, density=True)
    title("pareto")
    show()
# montecarlo
print("\nPareto\nE[X]=", (v*a)/(v-1), "\nVar(X)=", (v*a**2)/(v-2) - (v**2 * a**2)/((v-1)**2))
for r in reps:
    Y = gen_pareto(a, v, r)
    m, var = stats(Y)
    err = np.sqrt(var/r)
    m_err = abs((v*a)/(v-1) - m)
    print("%8.4d %8.4f %8.4f %8.4f %8.4f %-4s" %(r, m, var, err, m_err, is_in((v*a)/(v-1), m, err, ampl)))

# es 1.4.5 (gaussian)
mu = 1
sd = 1
# plot
x, y = norm_dens(mu, sd, bins)
for r in reps2:
    Y = gen_norm(mu, sd, r)
    plot(x, y)
    hist(Y, bins=bins, density=True)
    title("normal")
    show()
# montecarlo
print("\nNormal\nE[X]=", mu, "\nVar(X)=", sd**2)
for r in reps:
    Y = gen_norm(mu, sd, r)
    m, var = stats(Y)
    err = np.sqrt(var/r)
    m_err = abs(mu - m)
    print("%8.4d %8.4f %8.4f %8.4f %8.4f %-4s" %(r, m, var, err, m_err, is_in(mu, m, err, ampl)))

# chi-squared
x = np.random.normal(size=(1<<14))
x = x*x
hist(x, bins=bins, density=True)
title("$\chi^2 _i$")
show()

input("\npress enter to end")
