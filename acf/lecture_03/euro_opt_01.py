#!/usr/bin/env python3

import sys
from sys import stdout as cout
from scipy.stats import norm
from math import *

def euro_put(So, r, T, sigma, k):
    kT   = exp(-r*T)*k
    kHat = kT/So
    d    = ( log(kHat) + .5*sigma*sigma*T)/sigma
    N1   = norm.cdf(d/sqrt(T))
    N2   = norm.cdf((d-sigma*T)/sqrt(T))

    return So*( kHat*N1 - N2 )
# -----------------------

def euro_call(So, r, T, sigma, k):
    put = euro_put( So, r, T, sigma, k)
    return So - exp(-r*T)*k + put
# -----------------------


def run():

    So     = 1.0
    k      = 1.0
    T      = 1.0
    r      = 0.0
    sigma  =  .40

    cout.write("@ %-24s %8.4f\n" %("So", So))
    cout.write("@ %-24s %8.4f\n" %("k", k))
    cout.write("@ %-24s %8.4f\n" %("T", T))
    cout.write("@ %-24s %8.4f\n" %("r", r))
    cout.write("@ %-24s %8.4f\n" %("sigma", sigma))

    put  = euro_put ( So, r, T, sigma, k)
    call = euro_call( So, r, T, sigma, k)
    cout.write("@ %-24s %8.4f\n" %("European Put ", put))
    cout.write("@ %-24s %8.4f\n" %("European Call", call))
# --------------------------

if __name__ == "__main__":

    run()
