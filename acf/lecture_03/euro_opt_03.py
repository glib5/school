#!/usr/bin/env python3

import sys
from sys import stdout as cout
from scipy.stats import norm
from math import *
from config import get_input_parms
from time import sleep

def euro_put(So, r, T, sigma, k):
    kT   = exp(-r*T)*k
    kHat = kT/So
    d    = ( log(kHat) + .5*sigma*sigma*T)/sigma
    N1   = norm.cdf(d/sqrt(T))
    N2   = norm.cdf((d-sigma*T)/sqrt(T))

    return So*( kHat*N1 - N2 )
# -----------------------

def euro_call(So, r, T, sigma, k):
    put = euro_put( So, r, T, sigma, k)
    return So - exp(-r*T)*k + put
# -----------------------


def run(args):

    output = None
    So     = 1.0
    k      = 1.0
    T      = 1.0
    r      = 0.0
    sigma  =  .40
    inpts  = get_input_parms(args)

    try:
        output = inpts["out"]
        fp     = open(output, "w")
    except KeyError:
        fp     = cout

    fp.write("@ %-24s %8.4f\n" %("So", So))
    fp.write("@ %-24s %8.4f\n" %("k", k))
    fp.write("@ %-24s %8.4f\n" %("T", T))
    fp.write("@ %-24s %8.4f\n" %("r", r))
    fp.write("@ %-24s %8.4f\n" %("sigma", sigma))

    put  = euro_put ( So, r, T, sigma, k)
    call = euro_call( So, r, T, sigma, k)
    fp.write("@ %-24s %8.4f\n" %("European Put ", put))
    fp.write("@ %-24s %8.4f\n" %("European Call", call))

    if output != None: fp.close()
# --------------------------

if __name__ == "__main__":

    run(sys.argv)

    sleep(10)
