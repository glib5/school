#!/usr/bin/env python3
from sys import stdout as cout
from time import time
from math import *
import numpy as np
from euro_opt import euro_call, euro_put
from stats import stats
# ------------------------------------------------
def mc_put(rand, N, So, r, T, sigma, kl):
    put = {}
    eT  = exp(-r*T)

    for k in kl:

        # The usual martingale
        X = rand.normal( loc = -.5*sigma*sigma*T, scale = sigma*sqrt(T), size=N)

        # the discounted strike
        kT   = eT*k

        # the payoff ...
        payoff = kT - So*np.exp(X)

        # the option value
        put[k] = np.where( payoff > 0.0, payoff, 0.0)

    return  put
# -----------------------------------------------------

def run(So=1.0, T=1.0, r = 0.0, sigma=.20, kl=[1.0], Ns=[10]):

    rand = np.random.RandomState()
    rand.seed(92823)

    put = {}
    for k in kl: put[k] = euro_put( So, r, T, sigma, k)

    array = {}
    prfx=""
    for n in Ns:
        N = ( 1 << n )
        array[n] = mc_put(rand, N, So, r, T, sigma, kl)
        cout.write("%s2^%d" %(prfx, n)); cout.flush(); prfx=", "
    cout.write("\n")

    print("\n#----")
    for k in kl:
        M  = put[k]
        print("\n#-- (S-%5.2f)^+ = %8.4f" %(k, M))
        print("%9s    %10s %3s %6s -- %5s" %("N", "E[(S-k)^+]", "+/-", "MC-err", "Op"))
        for n in Ns:
            N = ( 1 << n )
            x = array[n][k]
            m, S2 = stats(x)
            err   = sqrt(S2/N)
            Op  = ( abs(m-M) < 3.*err )
            print("%9d    %10.4f %3s %6.4f -- %5s" %( N, m, "+/-", err, Op))
# --------------------------

if __name__ == "__main__":

    So     = 1.4
    kl     = [ 0.9, 1.0, 1.2, 1.3, 1.4]
    T      = 2.0
    r      = 0.01
    sigma  =  .40
    Ns     = [10, 12, 14, 16, 18, 20]

    print("@ %-12s %8.4f" %("So", So))
    print("@ %-12s %8.4f" %("T" , T))
    print("@ %-12s %8.4f" %("r" , r))
    print("@ %-12s %8.4f" %("sigma", sigma))
    print("\n");

    start = time()
    run(So=So, T=T, r=r, sigma=sigma, kl=kl, Ns=Ns)
    end = time()
    print("Elapsed: %10.4f sec." %( end -start));
