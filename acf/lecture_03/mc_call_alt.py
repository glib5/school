#!/usr/bin/env python3
from sys import stdout as cout
from time import time
from math import *
import numpy as np
from euro_opt import euro_call, euro_put
from mc_put import mc_put
from stats import stats

def run(So=1.0, T=1.0, r = 0.0, sigma=.20, kl=[1.0], Ns=[10]):

    rand = np.random.RandomState()
    rand.seed(92823)

    call = {}
    for k in kl:
        call[k] = euro_call( So, r, T, sigma, k)

    array = {}
    prfx=""
    for n in Ns:
        N = ( 1 << n )
        array[n] = mc_put(rand, N, So, r, T, sigma, kl)
        cout.write("%s2^%d" %(prfx, n)); cout.flush(); prfx=", "
    cout.write("\n")

    cout.write("\n#----\n")
    for k in kl:
        M  = call[k]
        F  = So - exp(-r*T)*k
        cout.write("\n#-- (S-%5.2f)^+ = %8.4f\n" %(k, M))
        cout.write("%9s    %10s %3s %6s -- %5s\n" %("N", "E[(S-k)^+]", "+/-", "MC-err", "Op"))
        for n in Ns:
            N = ( 1 << n )
            x = array[n][k]
            m, S2 = stats(x)
            m += F
            err   = sqrt(S2/N)
            Op  = ( abs(m-M) < 3.*err )
            cout.write("%9d    %10.4f %3s %6.4f -- %5s\n" %( N, m, "+/-", err, Op))
# --------------------------

if __name__ == "__main__":

    So     = 1.4
    kl     = [ 0.9, 1.0, 1.2, 1.3, 1.4]
    T      = 2.0
    r      = 0.01
    sigma  =  .40
    Ns     = [10, 12, 14, 16, 18, 20]

    cout.write("@ %-12s %8.4f\n" %("So", So))
    cout.write("@ %-12s %8.4f\n" %("T" , T))
    cout.write("@ %-12s %8.4f\n" %("r" , r))
    cout.write("@ %-12s %8.4f\n" %("sigma", sigma))
    cout.write("\n");

    start = time()
    run(So=So, T=T, r = r, sigma=sigma, kl=kl, Ns=Ns)
    end = time()
    cout.write("Elapsed: %10.4f sec.\n" %( end -start));

