#!/usr/bin/env python3

import sys
from sys import stdout as cout
from math import *
import numpy as np
from time import time
from P_0T import discount_curve as zc
from config import get_input_parms, loadConfig
import matplotlib.pyplot as plt
# -----------------------------------------------------

def mc_trj(rand, hw, Dt, N):

    hw.cov(Dt)

    m  = np.ndarray(shape = (2 ), dtype=np.double)
    X  = np.ndarray(shape = ( N+1, 2 ), dtype=np.double ) # X[2,N]
    xi = rand.normal( loc = 0.0, scale = 1.0, size=(2, N)) # xi[N,2]

    sx  = hw.sx
    sf  = hw.sf
    rho = hw.rho
    gamm = hw.gamma
    g    = 1 - exp(-gamm*Dt)

    xi[1] = sf*( rho*xi[0] + sqrt( 1. - rho*rho)*xi[1] )
    xi[0] = sx*xi[0]
    xi    = xi.transpose()  # xi[2,N]


    X[0] = 0.0

    for n in range(N):
        m[0] = - g*(X[n][0])
        m[1] =  (g/gamm)*(X[n][0])
        X[n+1] = X[n] + m + xi[n]

    return X.transpose() # X[ N, 2]
# ----------------------------------------

class HW:

    def __init__(self, **kwargs):
        self.gamma = kwargs["gamma"] 
        self.sigma = kwargs["sigma"] 

    # --------------------


    def show(self):
        print("@ %-12s: %-8s %8.4f" %("Info", "gamma", self.gamma))
        print("@ %-12s: %-8s %8.4f" %("Info", "sigma", self.sigma))
    # --------------------

    def S2_f(self, t):
        g = self.gamma
        s = self.sigma
        h = exp( -g*t)
        return  ( (s*s)/(g*g ) )* ( t - (2/g)*( 1. - h ) + (1./(2*g)) * (1. - h*h ) ) 
    # ---------------------------

    def S2_X(self, t):
        g = self.gamma
        s = self.sigma
        h = exp( -g*t)
        return ( (s*s)/( 2 * g ) )* ( 1. - h*h )
    # ---------------------------

    def cov(self, t):
        g = self.gamma
        s = self.sigma
        h = exp( -g*t)

        sx  = sqrt( self.S2_X(t) )
        sf  = sqrt( self.S2_f(t) )
        C_xf= ( (s*s)/(2.*g*g) ) * ( 1 - h ) * ( 1 - h )

        self.sx  = sx
        self.sf  = sf
        self.rho = C_xf/(sx*sf)
    # ----------------------------------------

    def IntPhi( self, dc, Dt, N):
        i_phi  = np.ndarray(shape = N+1, dtype=np.double)

        i_phi[0] = 0
        for n in range(N):
            i_phi[n+1] = i_phi[n] + log( dc.P_0t((n+1)*Dt)/dc.P_0t( n*Dt)) - .5*self.S2_f((n+1)*Dt) + .5 * self.S2_f(n*Dt)

        return i_phi
# ----------------------------------------

def usage():
    print("Usage: ./main.py -in input_file [ -- help ] [ -nr exp] [-y year] [-nt intrvls] [-gm gamma] [-s sigma]")
    print("        input_file: the input file holding interest rates")
    print("        exp       : log base 2 of the number of MC trajectories")
    print("                    the number of MC trajectories will be 2^exp.")
    print("                    If undefined will default to 10")
    print("        gamma     : the gamma paramter of the HW model")
    print("                    defaults to .5")
    print("        sigma     : the vol paramter of the HW model")
    print("                    defaults to .05")
    print("        year      : the number of years of teh simulation")
    print("                    defaults to 10")
    print("        intrvl    : the number of intervals we are going to measure")
    print("                    defaults to 30")
#-------------


def run(argv):

    # initial values
    nr    = 10
    gamma = .5
    sigma = .05
    Ny    = 10.
    Nt    = 30

    parms = get_input_parms(argv)

    try:
        Op = parms["help"]
        usage()
        return
    except KeyError:
        pass

    inpt = "ir.py" # to bypass the prompt input procedure
##    inpt  = parms["in"]
    PAR   = loadConfig(inpt)


    try:
        output = parms["out"]
        fp = open(output, "w")
    except KeyError:
        fp = cout

    try: nr = int(parms["nr"] )
    except KeyError: pass
    N = (1 << nr)

    try: gamma = float(parms["gm"] )
    except KeyError: pass

    try: sigma = float(parms["s"] )
    except KeyError: pass

    try: Ny = float(parms["y"] )
    except KeyError: pass

    try: Nt = int(parms["nt"] )
    except KeyError: pass
    Dt = Ny/Nt

    Obj = np.random.RandomState()
    Obj.seed(92823)
    # -------------------------------


    # build the discount curve
    # consistent with zero coupon rates given
    # in the input file.
    dc = zc(curve = PAR.curve)

    P_0T  = np.ndarray(shape = ( Nt+1 ), dtype=np.double ) 
    for n in range(Nt+1):
       P_0T[n] = dc.P_0t( n*Dt)

    # the HW model
    hw = HW(gamma=gamma, sigma=sigma)

    # The integral of the Phi function
    i_phi = hw.IntPhi( dc, Dt, Nt)

    # Begin MC
    P_tT  = 0.0
    P2_tT = 0.0
    n = N
    while n > 0:
        res = mc_trj( Obj, hw, Dt, Nt)
        P_tT += np.exp( res[1] )/N
        P2_tT += np.exp( 2*res[1] )/N
        n -= 1
    Err = np.sqrt(P2_tT/N)
    P_tT *= np.exp(i_phi)
    # End MC

    for n in range(Nt+1):
        fp.write( "%8.4f  +/- %8.4f   %8.4f\n" %( P_tT[n], Err[n], dc.P_0t( n*Dt )))

    
    # plot MC vs discount curve ....
    t = np.arange(0, Nt+1, 1, dtype = np.double)
    t *= Dt
    plt.errorbar(t, P_tT, yerr=Err, fmt='x', color='g', label="MC")
    plt.title("HW $\gamma$=%4.2f   $\sigma$=%5.2f  Mc = $2^{%d}$ " %(gamma, sigma, nr))
    plt.ylim(top=1.2, bottom=.2)
    plt.xlabel("Years")
    plt.ylabel("P_0t")
    plt.plot( t, P_0T, color='r', label="Input curve")
    plt.legend(loc="best")

    plt.show()
    
# -------------------------------


if __name__ == "__main__":
    run(sys.argv)
    
