#!/usr/bin/env python3
from math import *
import numpy as np 
from stats import stats

def poisson(Obj, C, N):
    iv = Obj.poisson(C, N)
    return np.float64(iv)
# -----------------------------------------------------

def run():

    ll = [ .2, 1.1, 3.4, 5.7, 12., 35., 128.]
    rand = np.random.RandomState()
    rand.seed(92823)

    N = ( 1 << 20 )
    print("%9s  %8s +/- %8s" %("l", "E[n]", "err"))
    for l in ll:
        array = poisson(rand, l, N)
        m, s2 = stats( array )
        err   = sqrt(s2/N)
        print("%9.4f  %8.4f +/- %8.4f" %(l, m, err))

if __name__ == "__main__":
    run()
    input("end\n")
