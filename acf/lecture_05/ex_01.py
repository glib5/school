#!/usr/bin/env python3
from math import *
import numpy as np 
import matplotlib.pyplot as plt
#from stats import stats

def md_gauss(Obj, C, N):
    p = C.shape[0]

    # X(p,N)
    X = Obj.normal( loc = 0.0, scale = 1.0, size=(p, N))
    Y = np.matmul(C, X)
    return Y.getT()
# -----------------------------------------------------

    Z  = 2./(b**2 - a**2)
    return Z*x
#-------------------

def sanity_check( M, C):
    Ct = C.getT()
    X  = np.matmul(C, Ct)
    X -= M
    print("X\n%s" %str(X))
    print("#--")

# -----------------------

def covMatrix(rand, p):

    # M_ij is positive definite if, 
    # for any vector X
    # Sum_{ij} X_i M_ij X_j >= 0
    #
    # Equality holds only if  X = 0.
    #
    # This form of M
    # M_ij = eta_i eta_j
    # is clearly positive definite, in fact
    #
    # Sum_{ij} X_i M_ij X_j  = Sum_{ij} X_i eta_i eta_j X_j 
    #                        = ( Sum_{i} X_i eta_i )^2 
    #
    # we take eta_i distributed uniform in [0,1)
    #
    Y  = np.matrix( rand.uniform( low = 0.0, high = 1.0, size=(p,p)))
    Yt = Y.getT()
    return Y*Yt
# ----------------------------

def run():

    p  = 4
    rand = np.random.RandomState()
    rand.seed(92823)

    # Build a random Covariance matrix
    M = covMatrix(rand, p)
    
    print("M\n%s" %str(M))
    print("#--")

    C = np.linalg.cholesky(M)
    sanity_check(M, C)
    
    N = ( 1 << 16 )
    array = md_gauss(rand, C, N)

    S = None
    S2 = None
    for n in range(N):
        x = np.matmul( array[n].getT(), array[n] )
        if n == 0: 
            S = x
            S2 = x*x
        else:      
            S += x
            S2 += x*x

    S  *= 1./N
    S2 *= 1./N
    Err = np.sqrt(S2/N)

    print("S\n%s" %str(S))
    print("Err\n%s" %str(Err))

if __name__ == "__main__":
    run()
