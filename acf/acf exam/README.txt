	  Student          |    ID
	  ------------------------------
	  Beggiato Guido   |  0000882941
	  Idone    Davide  |  0000901094
	  Peroni   Alessio |  0000884954

	#####################################
	#				    #
	#  Advanced Computational Finance   #
	#       Exam - Exercise 10.3        #
	#				    #
	#####################################

file: acf_exam.py

get help:  		  cd ... >python3 acf_exam.py --help
run with default values:  cd ... >python3 acf_exam.py

-default values as given in the exercise text
-unfortunately, all plots are lost when the program ends

---------------------------------------------------------
if it is not working with 'python3', try just 'python'