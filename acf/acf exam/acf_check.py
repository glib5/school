#--- imports
from sys import argv, exit
from sys import stdout as cout
import numpy as np
from matplotlib.pyplot import plot, errorbar, show, legend, ylim, title, ylabel, xticks

#-------------------------------------------------------------------------------------------------------

def mc_stats(matrix, reps):
    '''takes a montecarlo matrix (each row is a process,
     same column -> same time) and returns mean in each period
      and the associated mc error'''
    mu = np.mean(matrix, axis=0)
    err = np.var(matrix, axis=0)
    err = np.sqrt(err/reps)
    return mu, err

def read_parameters(args):
    '''parses the input line
    line option is of the form:  -option_one value_one (and so on, for all options)
    returns a dictionary like: {"option_one" : value_one, ...}
    special key "err" '''
    fin = {}
    n=0
    while 1:
        if n==len(args):
            break
        key = args[n]
        if n==0:
            fin["prog"]=key
        elif key[0:2]=="--":
            fin[key[2:]]=True
        elif key[0]=="-":
            n+=1
            if n==len(args):
                fin["err"]="Missing value for option '%s'" %key
                break
            fin[key[1:]]=args[n]
        else:
            fin["err"]="Illegal option '%s'" %key
            break
        n+=1
    return fin

def usage():
    print("Options:")
    print("    %-24s: this output" %("--help"))    
    print("\nParameters (default value in parenthesis)")
    print("    %-24s: length of the jump (1)"%("T"))
    print("    %-24s: interest rate (0.0)" %("r"))
    print("    %-24s: MC repetitions (100000)" %("reps"))
    print("    %-24s: volatility of the BS model (0.24)" %("sigmaBS"))
    print("    %-24s: starting value of the underlying (1)" %("s0"))
    print("%-16s "%("for Variance-Gamma"))
    print("    %-24s (0.1664)" %("eta_vg"))
    print("    %-24s (0.0622)" %("nu_vg"))
    print("    %-24s (-0.7678)" %("theta_vg"))
    print("%-16s "%("for Heston"))
    print("    %-24s (0.1153)" %("k"))
    print("    %-24s (0.0635)" %("nu0"))
    print("    %-24s (0.2125)" %("rho"))
    print("    %-24s (0.0100)" %("eta"))
    print("    %-24s (0.0240)" %("theta"))

#--- PROCESS GENERATORS

def gen_lognorm(lenght, s0, dt, sigma, r, rand):
    '''creates a log-normal trajectory,
     considers the interest rate'''
    values = [s0]
    for i in range(lenght):
        values.append(values[i]*np.exp((r - (sigma**2/2))*dt + sigma*np.sqrt(dt)*rand.normal()))
    return values[-1]

def gen_VG(S0, T, r, eta, nu, theta, size, rand):
    '''creates a variance gamma process of given lenght'''
    gamma = rand.gamma(T/nu, nu, size=size)
    W = rand.normal(size=size)*np.sqrt(gamma)
    Z = theta*gamma + eta*W
    ST = S0*np.exp((T/nu)*np.log(1 - nu*theta - ((nu*eta**2)/2)) + Z)*np.exp(r*T)  
    return ST

def gen_hes(vol_steps, v0, k, theta, eta, s_steps, s0, s_rep, rho, r, T, rand):
    ''' returns many heston processes, with a single underlying vol process
    ---
    vol_steps: how many steps for the volatility process
    v0: starting volatility
    k: how strongly vol goes back to theta
    theta: mean-reverting value of vol
    eta: volatility of the vol process
    s_steps: steps for the underlying process
    s0: underlying starting value
    reps: how many trajectories for s with a single vol
    rho: correlation of the 2 processes
    r: interest rate
    rand: fixed rng '''
    # create volatility and its integral
    dt = T/vol_steps
    vol = np.zeros(vol_steps+1)
    vol[0] = v0
    integr = np.zeros(vol_steps+1)
    integr[0] = v0*dt
    for t in range(vol_steps):
        v = vol[t]
        new_v = v + k*(theta-v)*dt + eta*np.sqrt(v*dt)*rand.normal()
        new_v = max(new_v, 0)
        vol[t+1] = new_v
        integr[t+1] = integr[t] + dt*new_v
    # create many processes expoiting this volatility
    proc = np.zeros(shape=(s_rep, s_steps+1))
    # fist value is always s0, all rows
    proc[:, 0] = s0
    #--- just 1 big jump (s_steps=1)
    # split vol
    dv = vol[-1] - vol[0]
    # split int
    di = integr[-1] - integr[0]
    for i in range(s_rep):
        for t in range(s_steps):
            old_s = proc[i][t]
            new_s = old_s*np.exp(-0.5*di+(rho/eta)*(dv-k*(theta*T-di))+np.sqrt((1-rho*rho)*di)*rand.normal())
            proc[i][t+1] = new_s*np.exp(r*T/s_steps)
    return proc

#-------------------------------------------------------------------------------------------------------

if __name__=="__main__":
    # declare variables
    T = 1
    reps = 100000
    strike = 1.03
    sigmaBS = 0.24
    s0 = 1
    # for VG
    eta_vg = 0.1664
    nu_vg = 0.0622
    theta_vg = -0.7678
    # for heston
    k = 0.1153
    nu0 = 0.0635
    rho = 0.2125
    eta = 0.010
    theta = 0.0240
    # interest rate
    r = 0.0
    
    # read new (eventually given) values
    output = None
    inputs = read_parameters(argv)
    try:
        op = inputs["help"]
        usage()
        exit(" ")
    except KeyError:
        pass
    try:
        output = inputs["out"]
        fp = open(output, "w")
    except KeyError:
        fp = cout
    #-------------------------------
    # general
    try: T = int(inputs["T"])
    except KeyError: pass
    try: reps = int(inputs["reps"])
    except KeyError: pass   
    try: s0 = float(inputs["s0"])
    except KeyError: pass    
    try: sigmaBS = float(inputs["sigmaBS"])
    except KeyError: pass
    # for VG
    try: eta_vg = float(inputs["eta_vg"])
    except KeyError: pass
    try: nu_vg = float(inputs["nu_vg"])
    except KeyError: pass
    try: theta_vg = float(inputs["theta_vg"])
    except KeyError: pass
    # for hes
    try: k = float(inputs["k"])
    except KeyError: pass
    try: nu0 = float(inputs["nu0"])
    except KeyError: pass
    try: rho = float(inputs["rho"])
    except KeyError: pass
    try: eta = float(inputs["eta"])
    except KeyError: pass
    try: theta = float(inputs["theta"])
    except KeyError: pass
    try: r = float(inputs["r"])
    except KeyError: pass
    
    #---  
    strikes = [s0-0.30, s0-0.20, s0-0.10, s0, s0+0.03, s0+0.10, s0+0.2, s0+0.30]
    
    rand = np.random.RandomState()
    rand.seed(2)

    #----------------------------------------
    # start the computations here

    fp.write("Simulation with %d repetitions\n" %reps)
    fp.write("Time horizon is %4.4f\n" %T)
    fp.write("Interest rate is %.4f\n" %r)
    fp.write("sigmaBS = %8.4f\n"%sigmaBS)
    fp.write("s0 = %8.4f\n"%s0)
    fp.write("-------------------\nVariance-Gamma params:\n")
    fp.write("eta_vg %8.4f\n"%eta_vg)
    fp.write("nu_vg %8.4f\n"%nu_vg)
    fp.write("theta_vg %8.4f\n"%theta_vg)
    fp.write("-------------------\nHeston params:\n")
    fp.write("k %8.4f\n"%k)
    fp.write("nu0 %8.4f\n"%nu0)
    fp.write("rho %8.4f\n"%rho)
    fp.write("eta %8.4f\n"%eta)
    fp.write("theta %8.4f\n"%theta)

    ###--- LOG NORMAL
    traj = np.zeros(reps)
    for i in range(reps):        
        traj[i] = gen_lognorm(1, s0, T, sigmaBS, r, rand)
    #--- plot
    muj, errj = mc_stats(traj, reps)
    errj *= 3
    errorbar([0, 1], [s0, muj], yerr=[0, errj], label="mc values +- 3 mc err")
    plot([0, 1], [s0, s0], label="s0:%4.2f"%s0)
    title("log-normal")
    span = abs(s0-muj)+(errj*1.3)
    ylim(s0-span, s0+span)
    legend(loc="upper left")
    xticks([])
    show()
    #--- prices
    fp.write("\nLOG NORMAL\n")
    for sk in strikes:        
        call_payoffs = np.where(traj > sk, (traj-sk), 0)
        cpr = np.exp(-r*T)*np.mean(call_payoffs)
        cerr = np.sqrt(np.var(call_payoffs)/reps)
        cerr *= 3
        put_payoffs = np.where(traj < sk, (sk-traj), 0)
        ppr = np.exp(-r*T)*np.mean(put_payoffs)
        perr = np.sqrt(np.var(put_payoffs)/reps)
        perr *= 3
        fp.write("%6s: %8.4f  |  %4s: %8.4f %3s %8.4f  |  %3s: %8.4f %3s %8.4f\n"%("Strike", sk, "Call", cpr, "+/-", cerr, "Put", ppr, "+/-", perr))
    fp.write("\n")

    ###--- VG
    traj = np.zeros(reps)
    for i in range(reps):
        traj[i] = gen_VG(s0, T, r, eta_vg, nu_vg, theta_vg, 1, rand)
    #--- plot
    muj, errj = mc_stats(traj, reps)
    errj *= 3
    errorbar([0, 1], [s0, muj], yerr=[0, errj], label="mc values +- 3 mc err")
    plot([0, 1], [s0, s0], label="s0:%4.2f"%s0)
    title("VG")
    span = abs(s0-muj)+(errj*1.3)
    ylim(s0-span, s0+span)
    legend(loc="upper left")
    xticks([])
    show()
    #--- prices
    fp.write("\nVARIANCE GAMMA\n")
    for sk in strikes:        
        call_payoffs = np.where(traj > sk, (traj-sk), 0)
        cpr = np.exp(-r*T)*np.mean(call_payoffs)
        cerr = np.sqrt(np.var(call_payoffs)/reps)
        cerr *= 3
        put_payoffs = np.where(traj < sk, (sk-traj), 0)
        ppr = np.exp(-r*T)*np.mean(put_payoffs)
        perr = np.sqrt(np.var(put_payoffs)/reps)
        perr *= 3
        fp.write("%6s: %8.4f  |  %4s: %8.4f %3s %8.4f  |  %3s: %8.4f %3s %8.4f\n"%("Strike", sk, "Call", cpr, "+/-", cerr, "Put", ppr, "+/-", perr))
    fp.write("\n")

    #--- HESTON
    s_rep = 100 # how many process for each simulated vol
    vol_steps = int(T*100) # how many steps for the vol
    print("\nsteps for the Heston's variance: %d"%vol_steps)
    s_steps = 1
    rep_hes = int(reps/s_rep) - 1
    traj = gen_hes(vol_steps, nu0, k, theta, eta, s_steps, s0, s_rep, rho, r, T, rand)
    for i in range(rep_hes):
        j = gen_hes(vol_steps, nu0, k, theta, eta, s_steps, s0, s_rep, rho, r, T, rand)
        traj = np.vstack((traj, j))
        cout.write("%d / %d\r"%(i+2, rep_hes+1))
        cout.flush()
    #--- plot
    traj = traj[:, -1]
    muj, errj = mc_stats(traj, reps)
    errj *= 3
    errorbar([0, 1], [s0, muj], yerr=[0, errj], label="mc values +- 3 mc err")
    plot([0, 1], [s0, s0], label="s0:%4.2f"%s0)
    title("Heston")
    span = abs(s0-muj)+(errj*1.3)
    ylim(s0-span, s0+span)
    legend(loc="upper left")
    xticks([])
    show()
    #--- prices
    fp.write("\nHESTON\n")
    for sk in strikes:        
        call_payoffs = np.where(traj > sk, (traj-sk), 0)
        cpr = np.exp(-r*T)*np.mean(call_payoffs)
        cerr = np.sqrt(np.var(call_payoffs)/reps)
        cerr *= 3
        put_payoffs = np.where(traj < sk, (sk-traj), 0)
        ppr = np.exp(-r*T)*np.mean(put_payoffs)
        perr = np.sqrt(np.var(put_payoffs)/reps)
        perr *= 3
        fp.write("%6s: %8.4f  |  %4s: %8.4f %3s %8.4f  |  %3s: %8.4f %3s %8.4f\n"%("Strike", sk, "Call", cpr, "+/-", cerr, "Put", ppr, "+/-", perr))
    fp.write("\n")

    # output part
    if output !=None:
        fp.close()
        print("\n\nresults written to file: %s" %output)
