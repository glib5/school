import numpy as np
from matplotlib.pyplot import plot, errorbar, show, legend, ylim, title, xticks, xlabel, ylabel, yticks, axvline
from sys import stdout as cout

#---------------------------------------------------------------------------------------------------

# working
def gen_hes2(steps, v0, s0, k, theta, eta, rand, r, q, rho):
    '''returns a single heston trajectory'''
    dt = 1/steps
    vol = np.zeros(steps+1)
    vol[0] = v0
    x = np.zeros(steps+1)
    for t in range(steps):
        v = vol[t]
        y = rand.normal()
        new_v = v + k*(theta-v)*dt + eta*np.sqrt(v*dt)*y
        new_v = max(new_v, 0)
        vol[t+1] = new_v

        x[t+1] = x[t] + (r-q-v/2)*dt + np.sqrt(v*dt)*(rho*y + np.sqrt(1-rho*rho)*rand.normal())
    x = s0*np.exp(x)
    return x

def gen_VG(s0, T, r, eta, nu, theta, size, rand):
    '''creates a variance gamma process of given lenght'''
    gamma = rand.gamma(T/nu,nu,size=size)
    W = rand.normal(size=size)
    Z = theta*gamma + eta*W
    ST = s0*np.exp((T/nu)*np.log(1 - nu*theta - ((nu*eta*eta)/2)) + Z)
    ST[0] = s0
    return ST

def gen_lognorm(lenght, s0, sigma, rand):
    "creates a log-normal trajectory (no drift)"
    dt = 1/lenght
    values = [s0]
    for i in range(lenght):
        values.append(values[i]*np.exp((-sigma**2/2)*dt + sigma*np.sqrt(dt)*rand.normal()))
    return values

def mc_stats(matrix, reps):
    '''takes "a montecarlo matrix" (each row is a process,
     same column -> same time) and returns mean in each period
      and the associated mc error'''
    mu = np.mean(matrix, axis=0)
    err = np.var(matrix, axis=0)
    err = np.sqrt(err/reps)
    return mu, err

#---------------------------------------------------------------------------------------------------

print("With this script we simply test the martingale property of our processes")
print("All parameters are the same of the exam, but not the lenght of the processes and the starting price\n")

rand = np.random.RandomState()
rand.seed(2)


types = ["ln", "vg", "hes"]
reps = int(input("reps? "))
print("reps: ", reps)
s0 = 10
r = 0.0

#- create data
traj = np.zeros(shape=(reps, 13*3))
for i in range(reps):
    a = gen_lognorm(12, s0, 0.24, rand)
    b = gen_VG(s0, 1, r, 0.1664, 0.0622, -0.7678, 13, rand)
    c = gen_hes2(12, 0.0635, s0, 0.1153, 0.024, 0.01, rand, r, 0, 0.2125)          
    traj[i] = np.hstack((a,b,c))
    cout.write("%d\r"%(i+1))
    cout.flush()

print(np.shape(traj))

mu, err = mc_stats(traj, reps)
conf = 2
err *= conf

# plot
plot([0, 38], [s0, s0], "r", label="s0: %.2f"%s0)
errorbar(range(39), mu, yerr=err, label="mc values +/- err*%d"%conf, color="blue", marker=None)
legend()
span = s0*0.01
ylim(s0-span, s0+span)
xticks([])   
xlabel("time")
ylabel("$S_t$", rotation="horizontal")
axvline(13, color="k", linestyle="dashed")
axvline(26, color="k", linestyle="dashed")
show()
            
   


















