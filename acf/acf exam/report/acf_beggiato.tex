\documentclass[10pt]{article}
\usepackage[utf8]{inputenc} 
\usepackage{geometry} % to change the page dimensions
\geometry{a4paper} % or letterpaper (US) or a5paper or....
\geometry{margin=1.1in}
\usepackage{booktabs} % for much better looking tables
\usepackage{array} % for better arrays (eg matrices) in maths
\usepackage{paralist} 
\usepackage{verbatim} 
\usepackage{amsmath} 
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{float}
\usepackage{wrapfig}
\usepackage{mathtools}

%%% HEADERS & FOOTERS
\usepackage{fancyhdr} 
\pagestyle{fancy} % options: empty , plain , fancy
\renewcommand{\headrulewidth}{0pt} % customise the layout...
\lhead{}\chead{}\rhead{}
\lfoot{}\cfoot{\thepage}\rfoot{}

\title{Advanced Computational Finance\\Final Exam, exercise 10.3}
\author{Guido Beggiato\\0000882941}
\date{} % Activate to display a given date or no date (if empty),
         % otherwise the current date is printed 

\begin{document}
\maketitle

\section{Introduction}
Our task is to produce data according to 3 different stochastic processes and then apply the same analysis to these datasets. The first step will be the computation of the theorical price of an european put option with the well-known Black-Sholes formula:
%\begin{equation*}
%Put(S, T) = Ke^{-rT}N(-d_1) - S_tN(-d_2)
%\end{equation*}
this is going to be one of our points of interest $\Pi(0)=0.1065$, when conducting our risk assesment.

Given that we are in a simulation, for every model we employ we will have $J$ trajectories and so $J$ values for the underlying $S$ in $t_M=0.5$ (6 months): this will allow us to have $J$ values for $\mathcal{V}$, our key variable representing risk.
\begin{equation*}
\mathcal{V}^j= \Pi(0) - P(0, t_M)\Pi(t_M)^j
\end{equation*}
Where $P(0, t_M)$ is the discount factor to use for prices from $t_M$ to 0 and $\Pi(t_M)$ is the Black-Sholes price of a put option with starting value of the underlying $S_{t_M}$ and maturity in $T$.

We will finally calculate the value at risk (VaR) from the empirical distribution of $\mathcal{V}$ at 1\%, 5\% and 10\%.

\subsection{Interest rates}
We are interested in just one period besides now and the maturity: $t_M=$ 6 months; this means we will use just 2 zero rates (from 0 up to $t_M$ or $T$) and the forward rate connecting $t_M$ and $T$.\\
\begin{center}
\begin{tabular}{c c c c c}
0 & $\xrightarrow{r(0, t_M)}$ & $t_M$ & $\xrightarrow{r(t_M, T)}$ & $T$\\
&&&&\\
0 & $\rightarrow$    & $\xrightarrow{r(0, T)} $ & $\rightarrow$    & $T$
\end{tabular}
\end{center}
We adopt a regime of deterministic interest rates and we have the relevant data for $t=0.5, 1, 2$ (6 months, 1 and 2 years). Starting from this we can linearly interpolate the correct rate for $T=1.13$ and, once we know it, we can get the forward rate between $t_M$ and $T$:
\begin{equation*}
r(t_M, T) = \frac{r(0, T)T - r(0, t_M)t_M}{T-t_M}
\end{equation*}
note that we imply continously compounded interest rates with this formula.\\ 
Numerical results are:
\begin{tabular}{l r}
\hline
$r(0, t_M)$&0.015414\\
$r(0, T)$&0.018335\\
$r(t_M, T)$&0.020653\\
\hline
\end{tabular}\\ Since the discount curve is increasing, it makes sense that the forward rate $r(t_M, T)$ is bigger than both the zero rates.

\subsection{Martingales}
\begin{figure}[H]
\centering
\includegraphics[width=\textwidth]{mart}
\caption{Testing the martingale property of the processes (order of the sections: log-normal, variance-gamma, Heston. Dashed lines as separators. Each section is a 1 year period, starting from $S_0$.). Result is, at least \emph{graphically}, satisfying.}
\label{mart}
\end{figure}

We simulate just 1 long jump of 6 months for the log-normal and the variance-gamma processes, while for the Heston model we adopt the strategy of letting the volatility evolve with a very small $dt$ and we carry with us only the relevant values of it and its integral:
\begin{eqnarray}
\nu_{t+1}&=&\nu_t + k(\theta-\nu_t)dt + \eta\sqrt{\nu_t dt}N(0, 1) \label{stochvol}\\
I_{t+1}&=&I_t + \frac{\nu_{t+1} + \nu_t}{2} dt \nonumber
\end{eqnarray}
that in our case are just the first and the last values of both arrays: so we can once more just simulate a jump of the process of length 6 months for the underlying.

Just a small check regarding the Feller condition: $\eta^2 \leq 2k\theta\rightarrow 0.0001\leq0.0027672\Rightarrow True$; that is, we are sure our volatility process never reaches 0.

To test the martingale property we set $r=0$ (see table (\ref{prices}) and figure (\ref{mart})) but in generating the data for the exam we use the correct $r(0, t_M)$.
\begin{table}[H]
\begin{tabular}{l| l r c| r c}
Model &Strike &Call price & $\pm$ 3err & Put price &$\pm$ 3err\\
\hline\\
Log-normal&1.0300&0.0826&0.0014&0.1132&0.0013\\
Variance-Gamma&1.0300&0.0836&0.0013&0.1142&0.0014\\
Heston&1.0300&0.0866&0.0015&0.1144&0.0013\\
\end{tabular}
\centering
\caption{Montecarlo prices, 100000 repetitions, parameters as in the exam, interest rate is 0 and time horizon is 1 year. (More tables below)}
\label{prices}
\end{table}

\newpage
\begin{verbatim}
Simulation with 100000 repetitions
Time horizon is 1.0000
Interest rate is 0.0000
sigmaBS =   0.2400
s0 =   1.0000
-------------------
Variance-Gamma params:
eta_vg   0.1664
nu_vg   0.0622
theta_vg  -0.7678
-------------------
Heston params:
k   0.1153
nu0   0.0635
rho   0.2125
eta   0.0100
theta   0.0240

LOG NORMAL
Strike:   0.7000  |  Call:   0.3054 +/-   0.0022  |  Put:   0.0061 +/-   0.0002
Strike:   0.8000  |  Call:   0.2198 +/-   0.0021  |  Put:   0.0204 +/-   0.0005
Strike:   0.9000  |  Call:   0.1490 +/-   0.0018  |  Put:   0.0496 +/-   0.0008
Strike:   1.0000  |  Call:   0.0954 +/-   0.0015  |  Put:   0.0960 +/-   0.0012
Strike:   1.0300  |  Call:   0.0826 +/-   0.0014  |  Put:   0.1132 +/-   0.0013
Strike:   1.1000  |  Call:   0.0581 +/-   0.0012  |  Put:   0.1587 +/-   0.0015
Strike:   1.2000  |  Call:   0.0339 +/-   0.0010  |  Put:   0.2346 +/-   0.0017
Strike:   1.3000  |  Call:   0.0192 +/-   0.0007  |  Put:   0.3198 +/-   0.0019


VARIANCE GAMMA
Strike:   0.7000  |  Call:   0.3092 +/-   0.0022  |  Put:   0.0097 +/-   0.0004
Strike:   0.8000  |  Call:   0.2249 +/-   0.0020  |  Put:   0.0254 +/-   0.0006
Strike:   0.9000  |  Call:   0.1533 +/-   0.0017  |  Put:   0.0539 +/-   0.0009
Strike:   1.0000  |  Call:   0.0972 +/-   0.0014  |  Put:   0.0978 +/-   0.0013
Strike:   1.0300  |  Call:   0.0836 +/-   0.0013  |  Put:   0.1142 +/-   0.0014
Strike:   1.1000  |  Call:   0.0571 +/-   0.0011  |  Put:   0.1577 +/-   0.0016
Strike:   1.2000  |  Call:   0.0308 +/-   0.0008  |  Put:   0.2314 +/-   0.0018
Strike:   1.3000  |  Call:   0.0152 +/-   0.0005  |  Put:   0.3158 +/-   0.0020


HESTON
Strike:   0.7000  |  Call:   0.3087 +/-   0.0023  |  Put:   0.0065 +/-   0.0003
Strike:   0.8000  |  Call:   0.2234 +/-   0.0021  |  Put:   0.0213 +/-   0.0005
Strike:   0.9000  |  Call:   0.1530 +/-   0.0019  |  Put:   0.0508 +/-   0.0008
Strike:   1.0000  |  Call:   0.0994 +/-   0.0016  |  Put:   0.0973 +/-   0.0012
Strike:   1.0300  |  Call:   0.0866 +/-   0.0015  |  Put:   0.1144 +/-   0.0013
Strike:   1.1000  |  Call:   0.0617 +/-   0.0013  |  Put:   0.1596 +/-   0.0015
Strike:   1.2000  |  Call:   0.0369 +/-   0.0010  |  Put:   0.2347 +/-   0.0018
Strike:   1.3000  |  Call:   0.0212 +/-   0.0008  |  Put:   0.3191 +/-   0.0020
\end{verbatim}
\newpage
\begin{verbatim}
Simulation with 100000 repetitions
Time horizon is 1.0000
Interest rate is 0.0500
sigmaBS =   0.2400
s0 =   1.0000
-------------------
Variance-Gamma params:
eta_vg   0.1664
nu_vg   0.0622
theta_vg  -0.7678
-------------------
Heston params:
k   0.1153
nu0   0.0635
rho   0.2125
eta   0.0100
theta   0.0240

LOG NORMAL
Strike:   0.7000  |  Call:   0.3371 +/-   0.0024  |  Put:   0.0037 +/-   0.0002
Strike:   0.8000  |  Call:   0.2517 +/-   0.0023  |  Put:   0.0133 +/-   0.0004
Strike:   0.9000  |  Call:   0.1780 +/-   0.0021  |  Put:   0.0347 +/-   0.0007
Strike:   1.0000  |  Call:   0.1193 +/-   0.0018  |  Put:   0.0712 +/-   0.0010
Strike:   1.0300  |  Call:   0.1048 +/-   0.0017  |  Put:   0.0852 +/-   0.0011
Strike:   1.1000  |  Call:   0.0762 +/-   0.0015  |  Put:   0.1232 +/-   0.0014
Strike:   1.2000  |  Call:   0.0467 +/-   0.0012  |  Put:   0.1888 +/-   0.0017
Strike:   1.3000  |  Call:   0.0276 +/-   0.0009  |  Put:   0.2649 +/-   0.0019


VARIANCE GAMMA
Strike:   0.7000  |  Call:   0.3402 +/-   0.0023  |  Put:   0.0066 +/-   0.0003
Strike:   0.8000  |  Call:   0.2565 +/-   0.0022  |  Put:   0.0180 +/-   0.0005
Strike:   0.9000  |  Call:   0.1829 +/-   0.0019  |  Put:   0.0396 +/-   0.0008
Strike:   1.0000  |  Call:   0.1226 +/-   0.0017  |  Put:   0.0743 +/-   0.0012
Strike:   1.0300  |  Call:   0.1073 +/-   0.0016  |  Put:   0.0876 +/-   0.0013
Strike:   1.1000  |  Call:   0.0768 +/-   0.0013  |  Put:   0.1237 +/-   0.0015
Strike:   1.2000  |  Call:   0.0447 +/-   0.0010  |  Put:   0.1867 +/-   0.0018
Strike:   1.3000  |  Call:   0.0240 +/-   0.0007  |  Put:   0.2612 +/-   0.0020


HESTON
Strike:   0.7000  |  Call:   0.3402 +/-   0.0024  |  Put:   0.0039 +/-   0.0002
Strike:   0.8000  |  Call:   0.2552 +/-   0.0023  |  Put:   0.0140 +/-   0.0004
Strike:   0.9000  |  Call:   0.1819 +/-   0.0021  |  Put:   0.0358 +/-   0.0007
Strike:   1.0000  |  Call:   0.1235 +/-   0.0018  |  Put:   0.0725 +/-   0.0011
Strike:   1.0300  |  Call:   0.1089 +/-   0.0017  |  Put:   0.0865 +/-   0.0012
Strike:   1.1000  |  Call:   0.0801 +/-   0.0015  |  Put:   0.1243 +/-   0.0014
Strike:   1.2000  |  Call:   0.0501 +/-   0.0012  |  Put:   0.1894 +/-   0.0017
Strike:   1.3000  |  Call:   0.0303 +/-   0.0010  |  Put:   0.2647 +/-   0.0019
\end{verbatim}
\newpage

\section{The Log-Normal case}
\begin{figure}[H]
\includegraphics[width=0.53\textwidth]{histln}
\includegraphics[width=0.53\textwidth]{vln2}
\caption{Log-normal data}
\label{ln}
\end{figure}

The process adopted here is a simple Geometric Brownian Motion: exactly what we believe in\footnote{according to the exercise!}
\begin{equation*}
S_t = S_0e^{\biggl\{r -\frac{\sigma^2}{2} + \sigma W_t\biggr\}}
\end{equation*}
that is distributed as the first image in Figure \ref{ln}.

The second image is an histogram showing us the p.d.f. of the random variable $\mathcal{V}$, based on the empirical data. This will be our benchmark, since we will use an analitical pricing rule that is based exactly on this process. 

Also note that in this setting (deterministic interest rate and volatility), if we know the variance of $\Pi(t_M)$, we can compute the variance of $\mathcal{V}$ because all the other terms in its definition are deterministic constants.

\section{The Variance-Gamma case}
\begin{figure}[H]
\includegraphics[width=0.53\textwidth]{histvg}
\includegraphics[width=0.53\textwidth]{vvg2}
\caption{Variance-Gamma data}
\label{vg}
\end{figure}
To generate this data we employ the approach of the time changed Brownian Motion: we evaluate a simple diffusion process at stochastic times given by the funcion $\gamma$
\begin{eqnarray*}
Z_t &=& \theta \gamma(t) + \eta W_{\gamma(t)}\\
S_t &=& e^{\biggl\{ \frac{t}{\nu} log(1-\nu\theta - \frac{\nu\eta^2}{2}) + Z_t \biggr\} } e^{rt} 
\end{eqnarray*}
We can see that the distribution of $S$ is less skewed than the previous one and this is also reflected in a different shape for $\mathcal{V}$: more mass is on the left so all the corresponding VaRs are lower, resulting in a different outcome if we stick to our assumption of a Black-Sholes world and the corresponding pricing rule.

\section{The Heston case}
\begin{figure}[H]
\includegraphics[width=0.53\textwidth]{histhes}
\includegraphics[width=0.53\textwidth]{vhes2}
\caption{Heston data}
\label{hes}
\end{figure}
Finally we generate data with the following method that uses the $\Delta$ of the integral of the volatility's process:
\begin{eqnarray*}
{S}(t_0)&=& \hat{S}_0\nonumber \\
\hat{S}_{n+1}&=& \hat{S}_n e^{\biggl\{ -\frac{1}{2}\Delta I_n + \frac{\rho}{\eta}[\Delta\nu_{n} - \kappa (\theta dt - \Delta I_n)] + \sqrt{(1-\rho^2)\Delta I_n} N(0, 1) \biggr\}}\nonumber \\
S(t_n)&=& \hat{S}_n e^{rt_n}\nonumber
\end{eqnarray*}
Where now $dt$ refers to the step of the underlying and not the one of the volatility.\\


The resulting distribution of $S$ is very similar to the one coming from the log-normal case, but the resulting values at risk are higher: this could be explained by the fact that we are still pricing with Black-Sholes but the volatility is not constant anymore; it actually evolves according to the stochastic process in \eqref{stochvol}.The resulting distribution might be similar to the log-normal case because the volatility does not vary too much, as the image (\ref{hesvol}) shows.

\begin{figure}[H]
\centering
\includegraphics[width=\textwidth]{hesvol}
\caption{Stochastic volatility for Heston, example.}
\label{hesvol}
\end{figure}

In Figure \ref{hesvol} we can see a realization of the stochastic process for the volatility in the Heston model: the steps are $4320=180\times24$ that are the hours in 6 months. The process seems decreasing, but this is just because the mean-reverting value of the volatility $\theta=0.0240$ is far away from the starting value 0.0635.

\section{Comparing VaR}

We decide to analize the right tail of the distribution for our VaR: a VaR at 1\% equal to 0.1042 means that in the 99\% of the cases $\mathcal{V}$ will be lower than such value. An equivalent way to say this is: in 99 \% of the cases, the maximum possible loss is $0.1042$. We are simply looking for quantiles in our empirical distribution, since we can't know the theorical formula. Recall that $\mathcal{V}$ is the difference in prices today and in 6 months: we assume we intend to sell today and buy back in the future.
\begin{table}[H]
\centering
\begin{tabular}{l | c c c}
&1\%&5\%&10\%\\
\hline\\
Log-normal			&0.1042&0.0974&0.0895\\
Variance-Gamma	&0.1016&0.0951&0.0886\\
Heston			     &0.1044&0.0977&0.0895
\end{tabular}
\caption{Right tail: data relative to a simulation with 10 millions of trajectories.}
\end{table}

It happears that Heston is the more risky world, while Variance-Gamma is the safest; this might depend on the skewness of the distribution for VG and the fact that the volatility in Heston is not constant.

Please note that the 3 histograms of $\mathcal{V}$ were realized keeping the $x$ axis constant so one can compare the shapes exactly: the 0 is always on the same spot but the other vertical lines, representing different VaRs, are in different positions and match the values reported in the above table.

\section{Some notes about the code}
\subsection{A brief list of functions}
\begin{itemize}
\item \verb eu_put: returns analitical price of an european put option
\item \verb VaR: returns (given a \%) the value at risk of a sorted array
\item \verb mc_stats: from a matrix returns mean and mc error of each column
\item \verb interpolate: returns the linearly interpolated interest rate from the curve
\item \verb read_parameters, \verb usage: used to parse the input line
\item \verb gen_log_norm, \verb gen_VG, \verb gen_hes: process generators.The first 2 return just the last value, while the last generates a volatility trajectory (of length $4320=180\times60=$ one step per hour, 6 months) and returns a given number (100) of processes for $S$ created using the same volatility.
\end{itemize}
\subsection{The procedure}
After the imports and the function definitions, we initialize the parameters with default values. Then there is the section devoted to the parsing of the input line, where the value of the parameters is changed, if a new one is given.

Then the computation starts: first of all we compute the relevant interest rates ($r(0, T), r(t_M, T)$) and $\Pi(0)$ with the appropriate functions descripted above and the discount factor $e^{-r(0, t_M)t_M}$ that we name simply \textbf{d}.

Then the code is divided in 3 sections, one for each process; in each section the procedure followed is the same:
\begin{enumerate}
\item with a \textbf{for} loop we create an array of data named \textbf{traj} : the values of our process in 6 months.
\item then we create the histogram of this data, highlighting the mean of the empirical distribution
\item we then calculate $\Pi(t_M)$: we just need to call the function \textbf{eu\_put} with the correct arguments and it will evaluate the full array \textbf{traj}, returning a new array we name \textbf{pim}.
\item finally we can obtain an array of values $\mathcal{V}^j$ with its definition: $\mathcal{V} = \Pi(0) - d*\Pi(t_M)$.
\item the VaR is calculated by taking the values in the corresponding position from the sorted array of $\mathcal{V}^j$
\item these results are shown in the second plot produced and VaRs are also printed.
\end{enumerate}

\subsection{A small comment}
The algorithm that we used to generate the data for the Heston model is surely faster than the other option, that is the Euler's discretization scheme, but is still slower compared with the other two processes generators because it has to generate the detailed process for the volatility. Also some improvment could be done in the use of structures to store the produced data: here the function  produces a matrix of dimension $(100 \times 2)$ where the first column is always $S_0$ and then proceeds to repeatedly stack matrix of this type one on top of the other and in the end it discards the first column, because we are interested only in terminal values; finally it evaluates the length of this final vector\footnote{Note that this will produce a number of values equal to the input \textbf{reps} only if such input is a multiple of 100. However this `100' is also a parameter in the code, so things are still quite flexible.} and uses that as a denominator in computing Montecarlo statistics.
\end{document}
%%%%%%%%%%%%%
\begin{enumerate}
\item
\end{enumerate}
%---------------------------
\begin{equation*}
\end{equation*}
%---------------------

\begin{eqnarray*}
&=& \nonumber \\
&=& \nonumber \\
\end{eqnarray*}