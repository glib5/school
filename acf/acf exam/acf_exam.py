# Beggiato 0000882941
# Idone    0000901094
# Peroni   0000884954

#-------------------------------------------------------------------------------------------------------
#--- imports
from sys import argv, exit
from sys import stdout as cout
import numpy as np
from matplotlib.pyplot import plot, errorbar, show, legend, xlim, ylim, title, hist, xlabel, ylabel, xticks, axvline
from scipy.stats import norm
#-------------------------------------------------------------------------------------------------------

def eu_put(s0, k, sigma, T, r):
    "Black-Sholes european put price"
    kT = np.exp(-r*T)*k
    k_hat = kT/s0
    d = (np.log(k_hat)+ sigma*sigma*0.5*T)/sigma
    N1 = norm.cdf(d/np.sqrt(T))
    N2 = norm.cdf((d-sigma*T)/np.sqrt(T))
    return kT*N1 - s0*N2

def VaR(values, percent):
    '''takes an array of values and returns the
     specified % value at risk'''
    # percent is in [0, 1], is a list
    v = sorted(values)
    var = []
    for i in percent:
        idx = int(i*len(v))
        var.append(v[idx])
    return var

def mc_stats(matrix, reps):
    '''takes a montecarlo matrix (each row is a process,
     same column -> same time) and returns mean in each period
      and the associated mc error'''
    mu = np.mean(matrix, axis=0)
    err = np.var(matrix, axis=0)
    err = np.sqrt(err/reps)
    return mu, err

def interpolate(t,t0,t1):
    '''returns linearly interpolated interest rate'''
    # index for 6 months = 0, index for 1y = 1
    curve = [ [ 0.500000, 0.015414]
            , [ 1.000000, 0.017795]
            , [ 2.000000, 0.021946]]
    
    t0 = int(curve[t0][0])         
    t1 = int(curve[t1][0])
    r1 = curve[t0][1]
    r2 = curve[t1][1]
    w1 = (t1 - t) / (t1 - t0)
    w2 = (t - t0) / (t1 - t0)
    res = w1 * r1 + w2 * r2
    return res

def read_parameters(args):
    '''parses the input line
    line option is of the form:  -option_one value_one (and so on, for all options)
    returns a dictionary like: {"option_one" : value_one, ...}
    special key "err" '''
    fin = {}
    n=0
    while 1:
        if n==len(args):
            break
        key = args[n]
        if n==0:
            fin["prog"]=key
        elif key[0:2]=="--":
            fin[key[2:]]=True
        elif key[0]=="-":
            n+=1
            if n==len(args):
                fin["err"]="Missing value for option '%s'" %key
                break
            fin[key[1:]]=args[n]
        else:
            fin["err"]="Illegal option '%s'" %key
            break
        n+=1
    return fin

def usage():
    print("Options:")
    print("    %-24s: this output" %("--help"))
    print("    %-24s: output file" %("-out"))
    
    print("\nParameters (default value in parenthesis)")
    print("    %-24s: MC repetitions (10000)" %("reps"))
    print("    %-24s: put strike (1.03)" %("strike"))
    print("    %-24s: volatility of the BS model (0.24)" %("sigmaBS"))
    print("    %-24s: starting value of the underlying (1)" %("s0"))
    print("%-16s "%("for Variance-Gamma"))
    print("    %-24s (0.1664)" %("eta_vg"))
    print("    %-24s (0.0622)" %("nu_vg"))
    print("    %-24s (-0.7678)" %("theta_vg"))
    print("%-16s "%("for Heston"))
    print("    %-24s (0.1153)" %("k"))
    print("    %-24s (0.0635)" %("nu0"))
    print("    %-24s (0.2125)" %("rho"))
    print("    %-24s (0.0100)" %("eta"))
    print("    %-24s (0.0240)" %("theta"))

#--- PROCESS GENERATORS

def gen_lognorm(lenght, s0, dt, sigma, r, rand):
    '''creates a log-normal trajectory,
     considers the interest rate'''
    values = [s0]
    for i in range(lenght):
        values.append(values[i]*np.exp((r - (sigma**2/2))*dt + sigma*np.sqrt(dt)*rand.normal()))
    return values[-1]

def gen_VG(S0, T, r, eta, nu, theta, size, rand):
    '''creates a variance gamma process of given lenght'''
    gamma = rand.gamma(T/nu, nu, size=size)
    W = rand.normal(size=size)*np.sqrt(gamma)
    Z = theta*gamma + eta*W
    ST = S0*np.exp((T/nu)*np.log(1 - nu*theta - ((nu*eta**2)/2)) + Z)*np.exp(r*T)  
    return ST

def gen_hes(vol_steps, v0, k, theta, eta, s_steps, s0, s_rep, rho, r, T, rand):
    '''
    returns many heston processes, with a single underlying vol process
    ---
    vol_steps: how many steps for the volatility process
    v0: starting volatility
    k: how strongly vol goes back to theta
    theta: mean-reverting value of vol
    eta: volatility of the vol process
    s_steps: steps for the underlying process
    s0: underlying starting value
    reps: how many trajectories for s with a single vol
    rho: correlation of the 2 processes
    r: interest rate
    rand: fixed rng
    '''

    # create volatility and its integral
    dt = T/vol_steps
    vol = np.zeros(vol_steps+1)
    vol[0] = v0
    integr = np.zeros(vol_steps+1)
    integr[0] = 0
    for t in range(vol_steps):
        v = vol[t]
        new_v = v + k*(theta-v)*dt + eta*np.sqrt(v*dt)*rand.normal()
        new_v = max(new_v, 0)
        vol[t+1] = new_v
        integr[t+1] = integr[t] + dt*0.5*(new_v + v)
    # create many processes expoiting this volatility
    proc = np.zeros(shape=(s_rep, s_steps+1))
    # first value is always s0, all rows
    proc[:, 0] = s0
    #--- for now, just 1 big jump (s_steps=1)
    # split vol
    dv = vol[-1] - vol[0]
    # split int
    di = integr[-1] - integr[0]
    for i in range(s_rep):
        for t in range(s_steps):
            old_s = proc[i][t]
            new_s = old_s*np.exp(-0.5*di+(rho/eta)*(dv-k*(theta*T-di))+np.sqrt((1-rho*rho)*di)*rand.normal())
            proc[i][t+1] = new_s*np.exp(r*T/s_steps)                                
    return proc

#-------------------------------------------------------------------------------------------------------

if __name__=="__main__":
    # declare variables
    reps = 10000
    strike = 1.03
    sigmaBS = 0.24
    s0 = 1
    # for VG
    eta_vg = 0.1664
    nu_vg = 0.0622
    theta_vg = -0.7678
    # for heston
    k = 0.1153
    nu0 = 0.0635
    rho = 0.2125
    eta = 0.010
    theta = 0.0240
    
    # read new (eventually given) values
    output = None
    inputs = read_parameters(argv)
    try:
        op = inputs["help"]
        usage()
        exit(" ")
    except KeyError:
        pass
    try:
        output = inputs["out"]
        fp = open(output, "w")
    except KeyError:
        fp = cout
    #-------------------------------
    # general
    try: reps = int(inputs["reps"])
    except KeyError: pass   
    try: strike = float(inputs["strike"])
    except KeyError: pass
    try: s0 = float(inputs["s0"])
    except KeyError: pass    
    try: sigmaBS = float(inputs["sigmaBS"])
    except KeyError: pass
    # for VG
    try: eta_vg = float(inputs["eta_vg"])
    except KeyError: pass
    try: nu_vg = float(inputs["nu_vg"])
    except KeyError: pass
    try: theta_vg = float(inputs["theta_vg"])
    except KeyError: pass
    # for hes
    try: k = float(inputs["k"])
    except KeyError: pass
    try: nu0 = float(inputs["nu0"])
    except KeyError: pass
    try: rho = float(inputs["rho"])
    except KeyError: pass
    try: eta = float(inputs["eta"])
    except KeyError: pass
    try: theta = float(inputs["theta"])
    except KeyError: pass
    


    
    # non-changable variables
    T = 1.13
    tM = 0.5
    r_M0 = 0.015414
    
    rand = np.random.RandomState()
    rand.seed(2)

    #----------------------------------------
    # start the computations here

    fp.write("Simulation with %d repetitions\n\n" %reps)
    fp.write("strike = %8.4f\n"%strike)
    fp.write("sigmaBS = %8.4f\n"%sigmaBS)
    fp.write("s0 = %8.4f\n"%s0)
    fp.write("-------------------\nVariance-Gamma params:\n")
    fp.write("eta_vg %8.4f\n"%eta_vg)
    fp.write("nu_vg %8.4f\n"%nu_vg)
    fp.write("theta_vg %8.4f\n"%theta_vg)
    fp.write("-------------------\nHeston params:\n")
    fp.write("k %8.4f\n"%k)
    fp.write("nu0 %8.4f\n"%nu0)
    fp.write("rho %8.4f\n"%rho)
    fp.write("eta %8.4f\n"%eta)
    fp.write("theta %8.4f\n"%theta)
    
    r_T0 = interpolate(T, 1, 2)
    r_TM = (r_T0*T - r_M0*tM)/(T-tM)
    d = np.exp(-r_M0*(tM))

    fp.write("\nInterest rates\n")
    fp.write("r_T0: %3.6f\n" %r_T0)
    fp.write("r_M0: %3.6f\n" %r_M0)
    fp.write("r_TM: %3.6f\n\n" %r_TM)

    dt1 = tM
    dt2 = T-tM

    #--- theoretical price with BS (just 1)
    pi0 = eu_put(s0, strike, sigmaBS, T, r_T0)
    fp.write("BS theorical price (from 0 to T) %8.4f\n\n"%pi0)

    ###--- LOG NORMAL
    #--- create log-normal data
    print("\n-----------------------------\nCREATING LOG-NORMAL DATA...")
    traj = np.zeros(reps)
    for i in range(reps):
        traj[i] = gen_lognorm(1, s0, dt1, sigmaBS, r_M0, rand)
    #--- histogram in 6 months
    hist(traj, density=True, bins=150, label="data")
    muj, errj = mc_stats(traj, reps)
    axvline(muj, color="r", label="mc mean: %.2f +/- %.2f"%(muj, errj))
    title("log-normal p.d.f in 6 months")
    legend()
    show()  
    #--- "forward" price: $\Pi(t_M)$
    print("CALCULATING PI(tM)...")
    pim = eu_put(traj, strike, sigmaBS, dt2, r_TM)
    #--- matcal V
    print("CREATING V...")
    V = pi0 - d*pim
    #muv, errv = mc_stats(V, reps) # mean and error of V
    v1, v5, v10 = VaR(V, [1-0.01, 1-0.05, 1-0.10])
    fp.write("(Log-normal) Value at Risk at 1, 5, 10 percent: %8.4f %8.4f %8.4f\n\n" %(v1, v5, v10))
    #--- histogram of V
    hist(V, density=True, bins=100)
    axvline(v1, color='k', linewidth=1, label="VaR at 1 perc: %.3f"%v1)
    axvline(v5, color='green', linewidth=1, label="VaR at 5 perc: %.3f"%v5)
    axvline(v10, color='r', linewidth=1, label="VaR at 10 perc: %.3f"%v10)
    axvline(0, color='y', linewidth=1, label="0")
    title("p.d.f. of V with log-normal")
    legend(loc="upper left")
    xlabel("min: %.4f --- max: %.4f"%(min(V), max(V)))
    xticks([])
    xlim(min(V)-0.1, max(V)+0.1)
    show()

    ###--- VARIANCE GAMMA
    print("\n-----------------------------\nCREATING VARIANCE-GAMMA DATA...")
    traj = np.zeros(reps)
    for i in range(reps):
        traj[i] = gen_VG(s0, dt1, r_M0, eta_vg, nu_vg, theta_vg, 1, rand)        
    #--- histogram in 6 months
    hist(traj, density=True, bins=150, label="data")
    muj, errj = mc_stats(traj, reps)
    axvline(muj, color="r", label="mc mean: %.2f +/- %.2f"%(muj, errj))
    title("variance-gamma p.d.f in 6 months")
    legend()
    show()  
    #--- "forward" price: $\Pi(t_M)$
    print("CALCULATING PI(tM)...")
    pim = eu_put(traj, strike, sigmaBS, dt2, r_TM)
    #--- matcal V
    print("CREATING V...")
    V = pi0 - d*pim
    v1, v5, v10 = VaR(V, [1-0.01, 1-0.05, 1-0.10])
    fp.write("(Variance-Gamma) Value at Risk at 1, 5, 10 percent: %8.4f %8.4f %8.4f\n\n" %(v1, v5, v10))
    # histogram of V
    hist(V, density=True, bins=100)
    axvline(v1, color='k', linewidth=1, label="VaR at 1 perc: %.3f"%v1)
    axvline(v5, color='green', linewidth=1, label="VaR at 5 perc: %.3f"%v5)
    axvline(v10, color='r', linewidth=1, label="VaR at 10 perc: %.3f"%v10)
    axvline(0, color='y', linewidth=1, label="0")
    title("p.d.f. of V with VG")
    legend(loc="upper left")
    xlabel("min: %.4f --- max: %.4f"%(min(V), max(V)))
    xticks([])
    xlim(min(V)-0.1, max(V)+0.1)
    show()

    ###--- HESTON
    print("\n-----------------------------\nCREATING HESTON DATA...")
    s_rep = 100 # how many process for each simulated vol
    vol_steps = 180*24 # how many steps for the vol
    fp.write("\nsteps for the Heston's variance: %d\n"%vol_steps)
    s_steps = 1 # just 1 step for S
    rep_hes = int(reps/s_rep) - 1
    traj = gen_hes(vol_steps, nu0, k, theta, eta, s_steps, s0, s_rep, rho, r_M0, tM, rand)
    for i in range(rep_hes):
        j = gen_hes(vol_steps, nu0, k, theta, eta, s_steps, s0, s_rep, rho, r_M0, tM, rand)
        traj = np.vstack((traj, j))
        cout.write("creating heston... %d / %d\r"%(i+3, rep_hes+1))
        cout.flush()        
    traj = traj[:, -1]
    fp.write("Heston trajectories amount: %d\n"%np.shape(traj))
    fp.write("with %d trajectories for each simulated volatility\n"%s_rep)
    #--- histogram in 6 months
    hist(traj, density=True, bins=150, label="data")
    muj, errj = mc_stats(traj, len(traj))
    axvline(muj, color="r", label="mc mean: %.2f +/- %.2f"%(muj, errj))
    title("Heston p.d.f in 6 months")
    legend()
    show()  
    #--- "forward" price: $\Pi(t_M)$
    print("CALCULATING PI(tM)...")
    pim = eu_put(traj, strike, sigmaBS, dt2, r_TM)
    #--- matcal V
    print("CREATING V...")
    V = pi0 - d*pim
    v1, v5, v10 = VaR(V, [1-0.01, 1-0.05, 1-0.10])
    fp.write("(Heston) Value at Risk at 1, 5, 10 percent: %8.4f %8.4f %8.4f\n\n" %(v1, v5, v10))
    # histogram of V
    hist(V, density=True, bins=100)
    axvline(v1, color='k', linewidth=1, label="VaR at 1 perc: %.3f"%v1)
    axvline(v5, color='green', linewidth=1, label="VaR at 5 perc: %.3f"%v5)
    axvline(v10, color='r', linewidth=1, label="VaR at 10 perc: %.3f"%v10)
    axvline(0, color='y', linewidth=1, label="0")
    title("p.d.f. of V with Heston")
    legend(loc="upper left")
    xlabel("min: %.4f --- max: %.4f"%(min(V), max(V)))
    xticks([])
    xlim(min(V)-0.1, max(V)+0.1)
    show()

    # end of computations
    #--------------------------------------------------------  
    # output part
    if output !=None:
        fp.close()
        print("\n\nresults written to file: %s" %output)
  

