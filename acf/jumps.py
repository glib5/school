import numpy as np
from matplotlib.pyplot import plot, show

# np.random.poisson(lambda, reps)
#--------------------------
def phij_merton(m, eta):
    return np.exp(m + eta*eta/2)

def phij_ber(val, probs):
    phi = 0
    for i in range(len(val)):
        phi += np.exp(val[i])*probs[i]
    return phi

def phij_kou(pi, eta1, eta2):
    return pi*(eta1/(eta1-1)) + (1-pi)*(eta2/(eta2+1))

def Jump(lamb, rand, mu=0, sd=1):
    '''gaussian jumps process'''
    numj = rand.poisson(lamb)
    # change jump shape here
    j = np.sum(rand.normal(loc=mu, scale=sd, size=numj))
    return j
#-----------------------------
##### M = exp(A-B+C+J)
# A = lambda*t*(1-phij)
# B = int_0 ^t sigma*sigma/2 ds     = (t/3)*sigma**3
# C = int_0 ^t sigma dW_s           = sigma*N(0, sqrt(t))
# J = sum of jumps
# needs the poisson process simulation
# simulate jump process
# J = sum(j) and number of js is realization of N(lamb*t)

rand = np.random.RandomState()
rand.seed(2)

reps = (1<<16)
lambdas = [1,2,3,5]
for l in lambdas:
    x = []
    for i in range(reps):
        j = Jump(l, rand)
        x.append(np.exp(j))
    m = np.mean(x)
    v = np.var(x)/reps
    print("\nlambda:%d\nmc for E[e^J]: %8.4f +/- %8.4f" %(l,m, v))
