import numpy as np
import matplotlib.pyplot as plt
from time import perf_counter


def mc_GCIR(rand, reps, periods, v0, k, theta, dt, eta):
    '''returns N cir trajs and the shocks that correlate in the Heston model'''
    V = np.empty(shape=(periods+1, reps))
    G = rand.normal(size=(periods, reps))
    V[0] = v0
    for i in range(periods):
        v = V[i]
        g = G[i]
        V[i+1] = v + k*(theta - v)*dt + eta*np.sqrt(v*dt)*g
        np.maximum(V[i+1], .0, out=V[i+1])
    return V, G


def mc_GHes(rand, reps, V, G, s0, mu, periods, dt, rho):
    '''returns N heston trajs, starting from a single vol and the correlated shocks'''
    X = np.empty(shape=(periods+1, reps))
    W = rand.normal(size=(periods, reps), scale=np.sqrt(1-rho*rho))
    X[0] = s0
    for i in range(periods):
        v = V[i]
        g = G[i]
        w = W[i]
        X[i+1] = (mu - v/2)*dt + np.sqrt(v*dt)*(rho*g + w)
        X[i+1] = X[i]*np.exp(X[i+1])
    return X


def MC(rand, v_reps, pv_reps, periods, v0, k, theta, dt, eta, s_reps, s0, mu, rho):
    fm = np.zeros(shape=periods+1)
    sm = np.zeros(shape=periods+1)
    print("%30d"%(v_reps*s_reps))
    tim = perf_counter()
    for a in range(v_reps//pv_reps):
        VV, GG = mc_GCIR(rand, pv_reps, periods, v0, k, theta, dt, eta)
        for r in range(pv_reps):
            V = VV[:, r]
            G = GG[:, r]
            s = mc_GHes(rand, s_reps, V, G, s0, mu, periods, dt, rho)
            fm += np.mean(s, axis=1)/v_reps
            sm += np.mean(s*s, axis=1)/v_reps
        print("%30d -- %2.2f\r"%((1+a)*pv_reps*s_reps, perf_counter()-tim), end='', flush=True)
    sm = 3*np.sqrt((sm-fm*fm)/v_reps)
    return fm, sm


def mc_plot(dt, perf_counters, ES, ERR, s0):
    '''simple MC plotting routine'''
    ax = [dt*i for i in range(perf_counters+1)]
    plt.errorbar(x=ax, y=ES, yerr=ERR)
    plt.ylim(s0*.99, s0*1.01)
    plt.plot([0, ax[-1]], [s0, s0], "r")
    plt.title("Heston -- martingale")
    plt.show()



def main():
    rand = np.random.RandomState()
    rand.seed(45567)

    print("USING SAME VOLATILITY FOR MORE SIMPLE MC")
    v_reps = 13#int(input("log_2 of vols? "))
    if v_reps<10:
        v_reps=10
    v_reps = 1<<v_reps
    pv_reps = 1<<10
    s_reps = 10#int(input("log_2 of trajs per vol? "))
    s_reps = 1<<s_reps
    if s_reps<10:
        s_reps=10
    periods = 12
    dt = 1/12
    v0 = .024
    theta = .024
    k = .2
    eta = .4
    rho = -.5
    s0 = 4.
    mu = .0

    ES, ERR = MC(rand, v_reps, pv_reps, periods, v0, k, theta, dt, eta, s_reps, s0, mu, rho)

    mc_plot(dt, periods, ES, ERR, s0)


if __name__=="__main__":
    main()
