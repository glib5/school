import numpy as np
import matplotlib.pyplot as plt
from time import perf_counter, sleep
from os import system
from numba import njit


@njit
def add_jumps(reps, times, dt, lda, muj, sigmaj, J, jumps):
    '''returns the jump component for a diffusion process'''
    J = np.cumsum(J)
    jumps += muj-sigmaj*sigmaj*.5
    s = 0
    for i in range(times*reps): # slow as f***
        e = int(J[i])
        a = e-s
        if a==0:
            J[i] = 0
        elif a==1:
            J[i] = jumps[s]
        else:
            J[i] = sum(jumps[s:e]) #np.sum is better with LARGE arrays
        s = e
    return np.reshape(J, (times, reps))


@njit
def MC_heston_j(reps, periods, dt, v0, k, theta, eta, rho, mu, s0, lda, muj, sigmaj, J, G, jumps):
    ''' returns N trajectories from an Heston model, with jumps'''
    v = np.full(shape=reps, fill_value=v0)
    X = np.empty(shape=(periods+1, reps))
    G[1] *= np.sqrt(1-rho*rho)
    X[0] = 0
    F = add_jumps(reps, periods, dt, lda, muj, sigmaj, J, jumps)
    for i in range(periods):
        g = G[0][i]
        w = G[1][i]
        vdt = np.sqrt(v*dt)
        X[i+1] = X[i] + (mu - v/2)*dt + vdt*(rho*g + w) + F[i]
        v += k*(theta - v)*dt + eta*vdt*g
        v = np.maximum(v, .0)
    return s0*np.exp(X)


def MC(rand, reps, t_reps, periods, dt, v0, k, theta, eta, rho, mu, s0, lda, muj, sigmaj):
    ES = np.zeros(shape=periods+1, dtype=np.double)
    ERR = np.zeros(shape=periods+1, dtype=np.double)
    print("%12d"%reps)
    tim = perf_counter()
    for t in range(reps//t_reps):
        # all the random shocks are here
        J = rand.poisson(lam=lda*dt, size=periods*t_reps).astype(np.float64) # num of jumps
        jumps = rand.normal(size=int(np.sum(J)), scale=sigmaj) # size of jumps (variable length !)
        G = rand.normal(size=(2, periods, t_reps)) # diffusion shocks
        X = MC_heston_j(t_reps, periods, dt, v0, k, theta, eta, rho, mu, s0, lda, muj, sigmaj, J, G, jumps)
        ES += np.sum(X, axis=1)
        ERR += np.sum(X*X, axis=1)
        i = (t+1)*t_reps
        if i%(1<<20)==0:
            print("%12d -- %2.2f\r"%(i, perf_counter()-tim), end='', flush=True)
    print("")
    # mean and variance, MC error
    ES /= reps
    ERR = ERR/reps - ES*ES
    ERR = 3*np.sqrt(ERR/reps)
    return ES, ERR


def mc_plot(dt, perf_counters, ES, ERR, s0):
    '''simple MC plotting routine'''
    ax = [dt*i for i in range(perf_counters+1)]
    plt.errorbar(x=ax, y=ES, yerr=ERR)
    plt.ylim(s0*.99, s0*1.01)
    plt.plot([0, ax[-1]], [s0, s0], "r")
    plt.title("Heston -- Martingale")
    plt.show()


#---------------------------------------------------------------------------


def main():
    _=system('cls')
    rand = np.random.RandomState(seed=34546)
    # params
    periods = 12
    dt = 1/12
    s0 = 4
    mu = .0
    sd = .3
    v0 = .24
    theta = .24
    k = .2
    eta = .4
    rho = -.5
    lda = 3
    muj = 0
    sigmaj = .06
    # MC 
    reps = 23#int(input("log_2 of reps? "))
    if reps<10:
        reps=10
    reps = 1<<reps
    t_reps = 1<<10
    ES, ERR = MC(rand, reps, t_reps, periods, dt, v0, k, theta, eta, rho, mu, s0, lda, muj, sigmaj)    
    mc_plot(dt, periods, ES, ERR, s0)
    return 0

def myplot():
    rand = np.random.RandomState(seed=34546)
    # params
    t_reps = 4
    periods = 1200
    dt = 1/120 
    s0 = 100
    mu = .0
    sd = .3
    v0 = .24
    theta = .24
    k = .2
    eta = .4
    rho = -.5
    lda = 3
    muj = 0
    sigmaj = .12   
    J = rand.poisson(lam=lda*dt, size=periods*t_reps).astype(np.float64)
    jumps = rand.normal(size=int(np.sum(J)), scale=sigmaj) 
    G = rand.normal(size=(2, periods, t_reps))   
    X = MC_heston_j(t_reps, periods, dt, v0, k, theta, eta, rho, mu, s0, lda, muj, sigmaj, J, G, jumps)   
    plt.plot(X, linewidth=.7)
    plt.show()
    return None

if __name__=="__main__":
    main()
    myplot()
