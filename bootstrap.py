import numpy as np
import matplotlib.pyplot as plt


def bootstrap_musd(rand, sample, b_reps=1<<17):
    '''returns bootstrap values for mean and sd of a sample'''
    s_size = len(sample)
    c = 0
    pr = 1<<10
    b_mu = 0
    b_sd = 0
    print("\nComputing Bootstrap...")
    print("%10d"%b_reps)
    while c<b_reps:
        if (b_reps-c)<pr:
            pr=(b_reps-c)
        tb = rand.choice(sample, size=(pr, s_size))
        b_mu += np.sum(np.mean(tb, axis=1))
        b_sd += np.sum(np.std(tb, axis=1))
        c += pr
        print("%10d\r"%c, end='', flush=True)
    print("")
    b_mu /= b_reps
    b_sd /= b_reps
    return b_mu, b_sd


def bootstrap_musd_obs(rand, sample, b_reps=1<<17):
    '''returns bootstrap samples for mean and sd of sample'''
    s_size = len(sample)
    c = 0
    pr = 1<<10
    print("\nComputing Bootstrap...")
    b_mu = np.empty(shape=b_reps)
    b_sd = np.empty(shape=b_reps)
    print("%10d"%b_reps)
    while c<b_reps:
        if (b_reps-c)<pr:
            pr=(b_reps-c)
        tb = rand.choice(sample, size=(pr, s_size))
        b_mu[c:c+pr] = np.mean(tb, axis=1)
        b_sd[c:c+pr] = np.std(tb, axis=1)
        c += pr
        print("%10d\r"%c, end='', flush=True)
    print("")
    return b_mu, b_sd
    

def ex(rand):
    b_reps = 20
    b_reps = 1<<b_reps
    pr = 1<<10
    if b_reps<pr:
        b_reps = pr
    c = 0
    tot = 50#int(input("How many data points? "))
    b_size = tot#int(input("Size of bootstrap sample? "))
    distr = "u"#str(input("Data distrib (n-normal, p-poisson, u-uniform)? "))
    if distr=="n":
        t_mu = 3
        t_sd = 2
        data = rand.normal(loc=t_mu, scale=t_sd, size=tot)
    elif distr=="p":
        t_mu = 5
        t_sd = np.sqrt(5)
        data = rand.poisson(lam=5, size=tot)
    elif distr=="u":
        t_mu = .5
        t_sd = np.sqrt(1/12)
        data = rand.uniform(size=tot)
    mu = np.mean(data)
    sd = np.std(data)
    print("\nComputing Bootstrap...\n")
    b_mu = np.empty(shape=b_reps)
    b_sd = np.empty(shape=b_reps)
    print("%10d"%b_reps)
    while c<b_reps:
        tb = rand.choice(data, size=(pr, b_size))
        b_mu[c:c+pr] = np.mean(tb, axis=1)
        b_sd[c:c+pr] = np.std(tb, axis=1)
        c += pr
        print("%10d\r"%c, end='', flush=True)
    print("") 
    B_mu = np.mean(b_mu)
    B_sd = np.mean(b_sd)
    print("\ndata points: %d"%tot)
    print("bootstrap reps: %d"%b_reps)
    print("bootstrap sample size: %d"%b_size)
    print("\nmean: %4.4f"%mu)
    print("sd: %4.4f"%sd)
    print("bootstrap mean: %4.4f"%B_mu)
    print("bootstrap sd: %4.4f"%B_sd)   
    bi = 100
    plt.subplot(1, 2, 1)
    plt.hist(b_mu, rwidth=.8, bins=bi, label="bootstrap data", density=True)
    plt.axvline(x=B_mu, label="bootstrap", color="r")
    plt.axvline(x=mu, label="arithmetic", color="k")
    plt.axvline(x=t_mu, label="true", color="green")
##    plt.legend()
    plt.title("MEAN")
    plt.subplot(1, 2, 2)
    plt.hist(b_sd, rwidth=.8, bins=bi, label="bootstrap data", density=True)
    plt.axvline(x=B_sd, label="bootstrap", color="r")
    plt.axvline(x=sd, label="arithmetic", color="k")
    plt.axvline(x=t_sd, label="true", color="green")
    plt.legend()
    plt.title("SD")
    plt.show()


def main():

    rand = np.random.RandomState()
    rand.seed(6741)

    ex(rand)


if __name__=="__main__": main()

#-------------------------------------------------------------------------

##    sample = rand.uniform(size=300)
##
##    MU, SD = bootstrap_musd_obs(rand, sample)
##    mu = np.mean(MU)
##    sd = np.mean(SD)
##
##    names = ["true", "arithmetic", "bootstrap"]
##    means = [0.5, np.mean(sample), mu]
##    sds = [np.sqrt(1/12), np.std(sample), sd]
##
##    for a,b,c in zip(names, means, sds):
##        print("%12s %4.8f %4.8f"%(a, b, c))

