import numpy as np
import matplotlib.pyplot as plt
from time import perf_counter

#----------------------------------------

### second best, more memory
##def MC_heston_simple(rand, reps, periods, dt, v0, k, theta, eta, rho, mu, s0):
##    ''' returns N trajectories from an Heston model
##    (and the corresponding volatility)'''
##    V = np.empty(shape=(periods+1, reps))
##    X = np.empty(shape=(periods+1, reps))
##    G = rand.normal(size=(2, periods, reps))
##    G[1] *= np.sqrt(1-rho*rho)
##    V[0] = v0
##    X[0] = s0
##    for i in range(periods):
##        v = V[i]
##        g = G[0][i]
##        w = G[1][i]
##        V[i+1] = v + k*(theta - v)*dt + eta*np.sqrt(v*dt)*g
##        V[i+1] = np.maximum(V[i+1], .0)
##        X[i+1] = (mu - v/2)*dt + np.sqrt(v*dt)*(rho*g + w)
##        X[i+1] = X[i]*np.exp(X[i+1])
##    return X


### little slower with smaller arrays,
### but great with memory manag and larger arrays
def MC_heston(rand, reps, periods, dt, v0, k, theta, eta, rho, mu, s0):
    ''' returns N trajectories from an Heston model'''
    v = np.full(shape=reps, fill_value=v0)
    X = np.empty(shape=(periods+1, reps))
    G = rand.normal(size=(2, periods, reps))
    np.multiply(G[1], np.sqrt(1-rho*rho), out=G[1])
    X[0] = 0
    for i in range(periods):
        g = G[0][i]
        w = G[1][i]
        vdt = np.sqrt(v*dt)
        X[i+1] = X[i] + (mu - v/2)*dt + vdt*(rho*g + w)
        v += k*(theta - v)*dt + eta*vdt*g
        np.maximum(v, .0, out=v)
    return s0*np.exp(X)

#-------------------------------------------

def MC(rand, reps, t_reps, periods, dt, v0, k, theta, eta, rho, mu, s0):
    ES = np.zeros(shape=periods+1, dtype=np.double)
    ERR = np.zeros(shape=periods+1, dtype=np.double)
    print("%12d"%reps)
    tim = perf_counter()
    for t in range(reps//t_reps):
        X = MC_heston(rand, t_reps, periods, dt, v0, k, theta, eta, rho, mu, s0)
        ES += np.sum(X, axis=1)
        ERR += np.sum(X*X, axis=1)
        i = (t+1)*t_reps
        if i%(1<<20)==0:
            print("%12d -- %2.2f\r"%(i, perf_counter()-tim), end='', flush=True)
    print("")
    # mean and variance, MC error
    ES /= reps
    ERR = ERR/reps - ES*ES
    ERR = 3*np.sqrt(ERR/reps)
    return ES, ERR


def mc_plot(dt, perf_counters, ES, ERR, s0):
    '''simple MC plotting routine'''
    ax = [dt*i for i in range(perf_counters+1)]
    plt.errorbar(x=ax, y=ES, yerr=ERR)
    plt.ylim(s0*.99, s0*1.01)
    plt.plot([0, ax[-1]], [s0, s0], "r")
    plt.title("Heston -- Martingale")
    plt.show()


#---------------------------------------------------------------------------


def main():
    rand = np.random.RandomState()
    rand.seed(45567)

    print("SIMPLE MC")
    # params
    periods = 12
    dt = 1/12
    s0 = 4
    mu = .0
    sd = .3
    v0 = .024
    theta = .024
    k = .2
    eta = .4
    rho = -.5
    # MC test
    reps = 21#int(input("log_2 of reps? "))
    if reps<10:
        reps=10
    reps = 1<<reps
    t_reps = 1<<10

    ES, ERR = MC(rand, reps, t_reps, periods, dt, v0, k, theta, eta, rho, mu, s0)

    mc_plot(dt, periods, ES, ERR, s0)

    t_reps = 4
    periods = 365*3
    dt = 1/365
    s0 = 87
    plt.plot(MC_heston(rand, t_reps, periods, dt, v0, k, theta, eta, rho, mu, s0), linewidth=.6)
    plt.show()


if __name__=="__main__":
    main()
