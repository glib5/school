from time import perf_counter
import numpy as np
import matplotlib.pyplot as plt


def mc_GBM(rand, reps, times, mu, sigma, dt, s0):
    '''returns N GMB trajs, as a matrix'''
    x = np.empty(shape=(times+1, reps)) 
    x[1:] = (mu-.5*sigma*sigma)*dt + rand.normal(size=(times, reps), scale=np.sqrt(dt)*sigma)
    np.add.accumulate(x[1:], out=x[1:], axis=0)
    np.exp(x[1:], out=x[1:])
    x[0] = s0
    np.multiply(x[1:], s0, out=x[1:])
    return x


def MC(rand, reps, p_reps, times, mu, sigma, dt, s0):
    print("%16d"%reps)
    ES = np.zeros(shape=times+1, dtype=np.double)
    ERR = np.zeros(shape=times+1, dtype=np.double)
    tim = perf_counter()
    flag = 0
    while flag != reps:
        x = mc_GBM(rand, p_reps, times, mu, sigma, dt, s0)
        x = x[1:]
        ES[1:] += np.sum(x, axis=1)
        ERR[1:] += np.sum(x*x, axis=1)
        flag += p_reps
        if flag%(1<<20)==0:
            print("%16d -- %2.2f\r"%(flag, perf_counter()-tim), end='', flush=True)
    print("\n")
    ES /= reps
    ES[0]=s0
    ERR[0]=s0*s0*reps
    conf = 3
    ERR = conf*np.sqrt( (ERR/reps - ES*ES)/reps )
    return ES, ERR


def mc_plot(dt, ES, ERR):
    '''simple MC plotting routine'''
    s0 = ES[0]
    ax = [dt*i for i in range(len(ES))]
    plt.errorbar(x=ax, y=ES, yerr=ERR)
    plt.ylim(s0*.99, s0*1.01)
    plt.plot([0, ax[-1]], [s0, s0], "r")
    plt.title("GBM -- martingale")
    plt.show()



#---------------------------------------------------------


def main():
    # params
    rand = np.random.RandomState(seed=2)
    
    reps = 23
    if reps<10:
        reps=10
    reps = 1<<reps
    p_reps = 10
    p_reps = 1<<p_reps
   
    times = 12
    dt = 1/12
    sigma = .3
    mu = 0
    s0 = 4
    
    ES, ERR = MC(rand, reps, p_reps, times, mu, sigma, dt, s0)

    mc_plot(dt, ES, ERR)


if __name__=="__main__":
    main()
