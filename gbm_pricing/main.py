import numpy as np

import gbm_MC
import eu_opt
import multiple_inpt as gui
import plotters as plt

def main():
    # RNG
    rand = np.random.RandomState(seed=4125)
    
    # user input
    ui = gui.INPUT_Values(None)
    ui.title('Values')
    ui.mainloop()
    # handle default values
    try:
        reps = int(ui.val1)
    except ValueError:
        reps = int(10)
    if reps<10:
        reps=10
    reps = 1<<reps
    try:
        times = int(ui.val2)
    except ValueError:
        times = int(12)
    try:
        dt = float(ui.val3)
    except ValueError:
        dt = float(12)
    dt = 1/dt
    try:
        mu = float(ui.val4)
    except ValueError:
        mu = float(.0)
    try:
        sigma = float(ui.val5)
    except ValueError:
        sigma = float(.3)
    try:
        s0 = float(ui.val6)
    except ValueError:
        s0 = float(4)
    try:
        strike = float(ui.val7)
    except ValueError:
        strike = float(3.4)
    try:
        r = float(ui.val8)
    except ValueError:
        r = float(.0)
    ui.destroy()
    del ui
    
    # naive optimization of the MC geometry
    p_reps = gbm_MC.opt_p_reps(times, reps)
    
    # show choosen parameters for the simulation
    print("%20s = %4d (%4d)"%("MC reps", reps, p_reps))
    print("%20s = %4.4f"%("s0", s0))
    print("%20s = %4d"%("times", times))
    print("%20s = %4.4f"%("dt", dt))
    print("%20s = %4.4f"%("mu", mu))
    print("%20s = %4.4f"%("sigma", sigma))
    print("%20s = %4.4f"%("strike", strike))
    print("%20s = %4.4f"%("r", r))
    
    # Black-Sholes theorical price
    T = np.arange(0, times+1, dtype=np.double)*dt
    true_put = eu_opt.eu_put(s0, strike, sigma, T[1:], r)
    true_put = np.insert(true_put, 0, max(.0, strike-s0))
    true_call = eu_opt.eu_call(s0, strike, sigma, T[1:], r)
    true_call = np.insert(true_call, 0, max(.0, s0-strike))
##    true_put = eu_opt.eu_put(s0, strike, sigma, T, r)
##    true_call = eu_opt.eu_call(s0, strike, sigma, T, r)
    
    # MC simulation
    mc_put_ev, mc_put_err, mc_call_ev, mc_call_err = gbm_MC.MC(rand, reps, times, mu, sigma, dt, s0, strike, T, r, p_reps)
    # results
    print("\nEU PUT")
    plt.printer(T, true_put, mc_put_ev, mc_put_err)
    print("\nEU CALL")
    plt.printer(T, true_call, mc_call_ev, mc_call_err)
    # plot
    if s0==strike and r==0:
        plt.plot1(T, true_put, mc_put_ev, mc_put_err, true_call, mc_call_ev, mc_call_err)
    else:
        plt.plot2(T, true_put, mc_put_ev, mc_put_err, true_call, mc_call_ev, mc_call_err)
    return 0

if __name__=="__main__":
    main()
