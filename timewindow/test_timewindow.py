from datetime import date
import os
import unittest

import timewindow as tw


REFERENCE_DAY = date(2023, 11, 14) # tuesday


class TestWeekSpan(unittest.TestCase):

    def setUp(self) -> None:
        self.reference_day = tw.WeekSpan(REFERENCE_DAY)

    def test_goto_end(self):
        res = self.reference_day.goto_end()
        exp = date(2023, 11, 19)
        self.assertEqual(res, exp)

    def test_goto_beginning(self):
        res = self.reference_day.goto_beginning()
        exp = date(2023, 11, 13)
        self.assertEqual(res, exp)

    def test_advance_one(self):
        res = self.reference_day.advance_one()
        exp = date(2023, 11, 21)
        self.assertEqual(res, exp)

    def test_advance_n(self):
        res = self.reference_day.advance_n(3)
        exp = date(2023, 12, 5)
        self.assertEqual(res, exp)

    def test_go_back_one(self):
        res = self.reference_day.go_back_one()
        exp = date(2023, 11, 7)
        self.assertEqual(res, exp)

    def test_go_back_n(self):
        res = self.reference_day.go_back_n(4)
        exp = date(2023, 10, 17)
        self.assertEqual(res, exp)


class TestMonthSpan(unittest.TestCase):

    def setUp(self) -> None:
        self.reference_day = tw.MonthSpan(REFERENCE_DAY)

    def test_goto_end(self):
        res = self.reference_day.goto_end()
        exp = date(2023, 11, 30)
        self.assertEqual(res, exp)

    def test_goto_beginning(self):
        res = self.reference_day.goto_beginning()
        exp = date(2023, 11, 1)
        self.assertEqual(res, exp)

    def test_advance_one(self):
        res = self.reference_day.advance_one()
        exp = date(2023, 12, 14)
        self.assertEqual(res, exp)

    def test_advance_n(self):
        res = self.reference_day.advance_n(3)
        exp = date(2024, 2, 14)
        self.assertEqual(res, exp)

    def test_go_back_one(self):
        res = self.reference_day.go_back_one()
        exp = date(2023, 10, 14)
        self.assertEqual(res, exp)

    def test_go_back_n(self):
        res = self.reference_day.go_back_n(4)
        exp = date(2023, 7, 14)
        self.assertEqual(res, exp)


class TestYearSpan(unittest.TestCase):

    def setUp(self) -> None:
        self.reference_day = tw.YearSpan(REFERENCE_DAY)

    def test_goto_end(self):
        res = self.reference_day.goto_end()
        exp = date(2023, 12, 31)
        self.assertEqual(res, exp)

    def test_goto_beginning(self):
        res = self.reference_day.goto_beginning()
        exp = date(2023, 1, 1)
        self.assertEqual(res, exp)

    def test_advance_one(self):
        res = self.reference_day.advance_one()
        exp = date(2024, 11, 14)
        self.assertEqual(res, exp)

    def test_advance_n(self):
        res = self.reference_day.advance_n(3)
        exp = date(2026, 11, 14)
        self.assertEqual(res, exp)

    def test_go_back_one(self):
        res = self.reference_day.go_back_one()
        exp = date(2022, 11, 14)
        self.assertEqual(res, exp)

    def test_go_back_n(self):
        res = self.reference_day.go_back_n(4)
        exp = date(2019, 11, 14)
        self.assertEqual(res, exp)


if __name__ == "__main__":
    _ = os.system("cls")
    unittest.main()