from __future__ import annotations
import abc
from datetime import date, timedelta


ONE_DAY = timedelta(days=1)
ONE_WEEK = ONE_DAY * 7


def quarter(d:date) -> int:
    return 1 + int(d.month // 3.1)


def semester(d:date) -> int:
    return 1 if d.month < 7 else 2


def belong_to_same_month(d1:date, d2:date) -> bool:
    return d1.year == d2.year and d1.month == d2.month


def belong_to_same_day(d1:date, d2:date) -> bool:
    return d1.month == d2.month and d1.day == d2.day


class TimeSpan(abc.ABC):

    def __init__(self, d:date) -> None:
        self.d = d

    @abc.abstractmethod
    def goto_end(self) -> date:
        pass

    @abc.abstractmethod
    def goto_beginning(self) -> date:
        pass

    def goto_beginning_next(self) -> date:
        return self.goto_end() + ONE_DAY

    @abc.abstractmethod
    def advance_one(self) -> date:
        pass

    def advance_n(self, n:int) -> date:
        new = self._from_self(self.d)
        for _ in range(n):
            new = self._from_self(new.advance_one())
        return self._try_assign_end_month(new.d, self.d.day)

    @abc.abstractmethod
    def go_back_one(self) -> date:
        pass

    def go_back_n(self, n:int) -> date:
        new = self._from_self(self.d)
        for _ in range(n):
            new = self._from_self(new.go_back_one())
        return self._try_assign_end_month(new.d, self.d.day)

    @classmethod
    def _from_self(cls, d:date) -> TimeSpan:
        return cls(d)

    @staticmethod
    def _try_assign_end_month(d:date, end:int) -> date:
        _end = end
        while 1:
            try:
                return d.replace(day=_end)
            except ValueError:
                _end -= 1


class WeekSpan(TimeSpan):

    def goto_end(self) -> date:
        return (6 - self.d.weekday())*ONE_DAY + self.d

    def goto_beginning(self) -> date:
        return self.d - self.d.weekday()*ONE_DAY

    def advance_one(self) -> date:
        return self.d + ONE_WEEK

    def advance_n(self, n:int) -> date:
        return self.d + ONE_WEEK * n

    def go_back_one(self) -> date:
        return self.d - ONE_WEEK

    def go_back_n(self, n:int) -> date:
        return self.d - ONE_WEEK * n


class MonthSpan(TimeSpan):

    def goto_end(self) -> date:
        for i in (31, 30, 29, 28):
            try:
                return self.d.replace(day=i)
            except ValueError:
                continue

    def goto_beginning(self) -> date:
        return self.d.replace(day=1)

    def advance_one(self) -> date:
        if self.d.month == 12:
            return self.d.replace(year=self.d.year+1, month=1)
        return self._try_assign_end_month(
            self.d.replace(month=self.d.month+1, day=1), self.d.day
        )

    def go_back_one(self) -> date:
        if self.d.month == 1:
            return self.d.replace(year=self.d.year-1, month=12)
        return self._try_assign_end_month(
            self.d.replace(month=self.d.month-1, day=1), self.d.day
        )


class YearSpan(TimeSpan):

    def goto_end(self) -> date:
        return self.d.replace(month=12, day=31)

    def goto_beginning(self) -> date:
        return self.d.replace(month=1, day=1)

    def advance_one(self):
        return self._try_assign_end_month(
            self.d.replace(year=self.d.year+1, day=1), self.d.day
        )

    def go_back_one(self) -> date:
        return self._try_assign_end_month(
            self.d.replace(year=self.d.year-1, day=1), self.d.day
        )