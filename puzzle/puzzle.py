""" Generic Puzzle Solving Framework

License: MIT
Author: Raymond Hettinger

Simple Isstructions:
====================

Create your puzzle as a sublcass of Puzzle().
The first step is to choose a representation of the problem
state, preferably stored as a string. Set 'pos' to the starting
position and 'goal' to the ending position. Create an __iter__()
method that cimputes all possible new puzzle states reachable from
the current state. Call the .solve() method to solve the puzzle.

Important Note:

The __iter__() method must return a list or generator of puzzle
instances, not their representations.

Advanced Instructions:

1. .solve(depthFirst=1) will override the default breadth first search.
Use depth first when the puzzle known to be solved in a fixed number
of moves (for example, the eight queens problem is solved only when
the eight queen is placed on the board; also, the triangle tee problem
removes one tee on each move until all tees are removed). Breadth first
is ideal when the shortest path solution needs to be found or when
some paths have a potential to wander around infinately (i.e. you can
randomly twist a Rubik's cube all day and never come to a solution).

2. Define __repr__ for a pretty printed version of the current position.
The state for the Tee puzzle looks best when the full triangle is drawn.

3. If the goal state can't be defined as a string, override the isgoal()
method. For instance, the block puzzle is solved whenever block 1 is
in the lower left, it doesn't matter where the oter pieces are; hence,
isgoal() is defined to check the lower left corner and return a boolean.

4. Some puzzle's can be simplified by treating symmetric positions as
equal. Override the .canonical() method to pick one of the equivalent
positions as representative. This allows the solver to recognize paths
similar ones already expored. In tic-tac-toe an upper left corner on
the first move is symmetrically equivalent to a move on the upper right;
herce there are only three possible first moves (a corner, a midde side,
or in the center).
"""

# https://www.youtube.com/watch?v=S_ipdVNSFlo
# min 13:34

from abc import ABC, abstractmethod
from collections import deque
from sys import intern
from typing import Iterable, List


class Puzzle(ABC):

    pos = "" # default starting position

    goal = "" # ending position used by isgoal()

    def __init__(self, pos=None) -> None:
        if pos:
            self.pos = pos
    
    def __repr__(self) -> str: # returns a string representation of the position for printing
        return repr(self.pos)

    def canonical(self) -> str: # returns a string representation after adjusting for symmetry
        return repr(self)

    def isgoal(self) -> bool:
        return self.pos == self.goal

    @abstractmethod
    def __iter__(self) -> Iterable:
        if 0:
            yield self # returns list of objects of this class

    def solve(pos, depthFirst:bool=False) -> List:
        queue = deque([pos])
        trail = {intern(pos.canonical()): None}
        solution = deque()
        load = queue.append if depthFirst else queue.append

        while not pos.isgoal():
            for m in pos:
                c = m.canonical()
                if c in trail:
                    continue
                trail[intern(c)] = pos
                load(m)
            pos = queue.pop()

        while pos:
            solution.appendleft(pos)
            pos = trail[pos.canonical()]
        
        return list(solution)
