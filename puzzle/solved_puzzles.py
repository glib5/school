
# https://www.youtube.com/watch?v=S_ipdVNSFlo
# min 13:34


import os
from pprint import pprint
import re
from typing import Iterable

from puzzle import Puzzle


class JugFill(Puzzle):
    pos = (0, 0, 8)
    capacity = (3, 5, 8)
    goal = (0, 4, 4)
    def __iter__(self) -> Iterable:
        for i in range(len(self.pos)):
            for j in range(len(self.pos)):
                if i==j:
                    continue
                qty = min(self.pos[i], self.capacity[j]-self.pos[j])
                if not qty:
                    continue
                dup = list(self.pos)
                dup[i] -= qty
                dup[j] += qty
                yield JugFill(tuple(dup))


class EightQueens(Puzzle):
    """place 8 queens on the chess board such that no one can attack the others"""
    pos = ()
    
    def isgoal(self):
        return len(self.pos) == 8

    def __iter__(self) -> Iterable:
        x = len(self.pos)
        for y in range(8):
            if y in self.pos:
                continue
            for xp in range(x):
                yp = self.pos[xp]
                if abs(x-xp) == abs(y-yp):
                    break
            else:
                yield EightQueens(self.pos + (y,))

    def __repr__(self) -> str:
        # n = len(self.pos)
        rows = ["\n"]
        for i, y in enumerate(self.pos):
            row = (["_", "."] if i&1 else [".", "_"])*4 + ["\n"]
            row[y] = "Q"
            rows.append("".join(row))
        return "".join(rows)


class TriPuzzle(Puzzle):
    """triangle tee puzzle
    Tees are arranged in holes on a 5x5 equilater triangle except for the
    top center which left open. A move consists of a checker style jump of
    one tee over the newt into an open hole and removed the jump tee. Find
    a sequence of jumps leaving the last tee in the top center position
    """

    pos = "011111111111111"
    goal = "100000000000000"

    triples = [(0, 1, 3),
               (1, 3, 6),
               (3, 6, 10),
               (2, 4, 7),
               (4, 7, 11),
               (5, 8, 12),
               (10, 11, 12),
               (11, 12, 13),
               (12, 13, 14),
               (6, 7, 8),
               (7, 8, 9),
               (3, 4, 5),
               (0, 2, 5),
               (2, 5, 9),
               (5, 9, 14),
               (1, 4, 8),
               (4, 8, 13),
               (3, 7, 12)]

    def __iter__(self) -> Iterable:
        for t in self.triples:
            if self.pos[t[0]]=="1" and self.pos[t[1]]=="1" and self.pos[t[2]]=="0":
                yield TriPuzzle(self.produce(t, "001"))
            if self.pos[t[0]]=="0" and self.pos[t[1]]=="1" and self.pos[t[2]]=="1":
                yield TriPuzzle(self.produce(t, "100"))
    
    def produce(self):
        return (self.pos[:t[0]] + re.sub(0) + 
                self.pos[t[0]+1:t[1]] + re.sub(1) +
                self.pos[t[1]:t[2]] + re.sub(2) + 
                self.pos[t[2]+1:]
                )
    
    def canonical(self) -> str:
        p = self.pos
        q = "".join(map(p.__getitem__, (0, 2, 1, 5, 4, 3, 9, 8, 7, 6, 14, 13, 12, 11, 10)))
        return max(p, q)

    def __repr__(self) -> str:
        return ""


class PaPuzzle(Puzzle):
    pos = "11221133450067886799"
    goal = re.compile(r"................1...")
    xlat = str.maketrans("38975", "22264")
    block = {(0, -4), (1, -4), (2, -4), (3, -4),
             (16, 4), (17, 4), (18, 4), (19, 4),
             (0, -1), (4, -1), (8, -1), (12, -1), (16, -1),
             (3, 1), (7, 1), (11, 1), (15, 1), (19, 1)}

    def isgoal(self):
        return self.goal.search(self.pos) != None

    def __repr__(self) -> str:
        ans = "\n"
        pos = self.pos.replace("0", ".")
        for i in [0, 4, 8, 12, 16]:
            ans = ans + pos[i:i+4] + "\n"
        return ans
    
    def canonical(self) -> str:
        return self.pos.translate(self.xlat)

    def __iter__(self) -> Iterable:
        dsone = self.pos.find("0")
        dstwo = self.pos.find("0", dsone+1)
        for dest in [dsone, dstwo]:
            for adj in [-4, -1, 1, 4]:
                if (dest, adj) in self.block:
                    continue
                piece = self.pos[dest+adj]
                if piece=="0":
                    continue
                newmove = self.pos.replace(piece, "0")
                for i in range(20):
                    if 0 <= i+adj < 20 and self.pos[i+adj]==piece:
                        newmove = newmove[:i] + piece + newmove[i+1:]
                if newmove.count("0") != 2:
                    continue
                yield PaPuzzle(newmove)


    
# ============================================================================


def main():
    _ = os.system("cls")

    print("JugFill")
    pprint(JugFill().solve())

    print("8 queens")
    pprint(EightQueens().solve())

    print("Papuzzle")
    pprint(PaPuzzle().solve())


    return 0

if __name__=="__main__":
    main()
    input()
