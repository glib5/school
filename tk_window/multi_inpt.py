import tkinter as tk


class Inputs(tk.Tk):
    """class for getting inputs from a nice window UI"""
    
    def __init__(self):
        super().__init__()
        self.initialize()
        
    def initialize(self):
        r = 0
        self.grid()
        stepOne = tk.LabelFrame(self, text=" 1. Enter Values ")
        stepOne.grid(row=r, columnspan=7)
        # generic structure of 1 input (str)
        self.L1 = tk.Label(stepOne,text="Value 1 (int)")
        self.L1.grid(row=r, column=0)
        self.i1 = tk.Entry(stepOne, width=30) # control size here
        self.i1.grid(row=r, column=1, columnspan=3)
        r+=1
        # second (row++, else equal) here is a dropdown menu
        self.L2 = tk.Label(stepOne,text="Value 2 (str)")
        self.L2.grid(row=r, column=0, sticky='E')
        options = ["A", "B", "C"]
        self.i2 = tk.StringVar()
        self.dropdown = tk.OptionMenu(stepOne, self.i2, *options)
        self.dropdown.grid(row=r, column=1, columnspan=3, pady=2, sticky='WE')
        r+=1
        # subtim button, do NOT edit!
        SubmitBtn = tk.Button(stepOne, text="Submit",command=self.submit)
        SubmitBtn.grid(row=r, column=3, sticky='W', padx=5, pady=2)

    def submit(self):
        # set class attribute to corresponding input
        self.num = self.i1.get()
        self.letter = self.i2.get()

        self.quit()


def get_inputs():
    """handles the multiple input widget"""
    UI = Inputs()
    UI.mainloop()
    try:       
        # evaluation to correct dtype
        num = int(UI.num) if UI.num else 'default val1'
        letter = str(UI.letter) if UI.letter else 'default val2'
        # remove tk window
        UI.destroy() 
        del UI
        # reorganize input data structure here!
        return (num, letter)
    except AttributeError:
        quit()

# =============================================================================

def main():
    a, b = get_inputs()
    print(a, type(a))
    print(b, type(b))
    return 0

if __name__=="__main__":
    main()
    input()

    

    




















    
    

