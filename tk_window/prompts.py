"""
simple module for tkinter-based UI
- get calendar date
- retrieve from dropdown menu
- retrieve multiple SCALAR sinputs
"""

from datetime import datetime
from keyword import iskeyword
import tkinter as tk
from typing import Iterable, Any


def __red_cross():
    """what to do when the red cross is clicked"""
    print("Red Cross clicked in a prompt -- quitting program")
    quit()
    # raising exception won't close the prompt, unfortunately
    # can still be done here

# =============================================================================

class _multi_menu(tk.Tk):
    '''class for getting input from a dropdown menu'''
    
    def __init__(self, options, caption): # do NOT modify this function
        super().__init__()
        self.options = options
        self.caption = caption
        self.grid()
        self.root = tk.LabelFrame(self, text='')
        self.root.grid(row=0, columnspan=7, sticky='W')
        self.lab = tk.Label(self.root, text=self.caption)
        self.lab.grid(row=0, column=0, sticky='E', padx=5, pady=2)
        self.choice = tk.StringVar(self)
        self.menu = tk.OptionMenu(self.root, self.choice, *self.options)
        self.menu.grid(row=0, column=1, columnspan=3, pady=2, sticky='WE')
        self.fin = None
        SubmitBtn = tk.Button(self.root, text='Submit', command=self.submit)
        SubmitBtn.grid(row=1, column=3, sticky='W', pady=2)

    def submit(self):
        self.fin = self.choice.get()
        self.root.destroy()
        self.quit()


def get_multi_menu(options, caption:str=None)->str:
    """handles the dropdown menu UI.
    returns empty str if no option is selected"""
    assert isinstance(options, Iterable)
    if caption is None:
        caption = " Select one option "
    assert isinstance(caption, str)
    ui = _multi_menu(options, caption)
    ui.title('')
    ui.protocol("WM_DELETE_WINDOW", __red_cross)
    ui.mainloop()
    fin = ui.fin if ui.fin else ''
    ui.destroy()
    del ui
    return fin 

# =============================================================================

class _multi_inpt(tk.Tk):
    """class for getting multiple inputs from a nice window UI"""

    def __init__(self, names):
        super().__init__()
        self.names = names
        self.inames = ["_%s"%str(i) for i in range(len(self.names))]
        self.out = None
        self.title(' ')
        self.minsize(200, 0)
        self.grid()
        self.root = tk.LabelFrame(self, text="Insert values")
        self.root.grid(row=0, columnspan=2)
        for r, (iname, name) in enumerate(zip(self.inames, self.names)):
            # set the label
            setattr(self, name, tk.Label(self.root, text=name))
            getattr(self, name).grid(row=r, column=0)
            # set the input entry line
            setattr(self, iname, tk.Entry(self.root, width=20))
            getattr(self, iname).grid(row=r, column=1)
        SubmitBtn = tk.Button(self.root, text='Submit', command=self.submit)
        SubmitBtn.grid(row=len(names)+1, column=1)

    def submit(self):
        self.out = {n:getattr(self, i).get() for n,i in zip(self.names, self.inames)}
        self.quit()
        

def get_inputs(input_dict:dict[str:Any]) -> dict[str:Any]:
    '''
    retreive multiple SCALAR values from basic UI
    @param input: dict of the form
        {"param_name": param_default_value}
        i.e.: {"name": "default_name"}
    returns a dictionary like {"param_name":param_value}
    '''
    
    assert isinstance(input_dict, dict)
    for k in input_dict.keys():
        assert isinstance(k, str)
        assert not iskeyword(k)

    # retrieve inputs from GUI
    UI = _multi_inpt(input_dict.keys())
    UI.protocol("WM_DELETE_WINDOW", __red_cross)
    UI.mainloop()
    fin = UI.out
    UI.destroy()
    del UI
    # default value handle + input evaluation to correct dtype
    for k, v in input_dict.items():
        if not fin[k]: # if not choosen
            fin[k] = v # -> replace with default
        try: # cast to correct dtype
            t = type(v)
            fin[k] = t(fin[k])
        except ValueError: # provided a str for a numerical input
            fin[k] = v # -> replace with default
    return fin


def get_date() -> datetime:
    labels = ["year", "month (1-12)", "day (1-31)"]
    cal = _multi_inpt(labels)
    cal.title("Create a date")
    cal.minsize(250, 0)
    cal.protocol("WM_DELETE_WINDOW", __red_cross)
    cal.mainloop()
    fin = cal.out
    cal.destroy()
    del cal
    _year = int(fin[labels[0]])
    _month = int(fin[labels[1]])
    _day = int(fin[labels[2]])
    return datetime(year=_year, month=_month, day=_day)
