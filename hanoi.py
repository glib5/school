def hanoi(n, start, end):
    '''towers of hanoi recursive solution'''
    if n==1:
        print("%d -> %d"%(start, end))
    else:
        other = 6 - start - end # 6=1+2+3, valid only for 3 towers
        hanoi(n-1, start, other)
        print("%d -> %d"%(start, end))
        hanoi(n-1, other, end)

def main():
    # start and end rod (doesn't really matter. rods=1,2,3)
    s = 1
    e = 3
    # number of disks
    d = 3#int(input("How many disks? "))#3
    # solve problem
    hanoi(d, s, e)
    input("\n\nComplete! ")


if __name__=="__main__":
    main()
