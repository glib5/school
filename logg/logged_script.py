"""
# https://docs.python.org/3/library/logging.html#logging.basicConfig

This function should be called from the main thread:

>>> from any_logged_script import main
>>> main()

"""



def set_logger(name: str = __name__):
    """ sets a basic logger """
    # lazy import
    import logging
    # params
    lvl = logging.INFO
    fmt = "%d/%m/%Y %H:%M:%S"
    # stream handler
    sh = logging.StreamHandler()
    sh.setLevel(lvl)
    sh.setFormatter(logging.Formatter('%(asctime)s - %(message)s', datefmt=fmt))
    # create file handler 
    fh = logging.FileHandler(filename=f"{name}.log", mode="w")
    fh.setLevel(lvl)
    fh.setFormatter(logging.Formatter('%(asctime)s [%(name)s][%(levelname)s] %(message)s', datefmt=fmt))
    logging.basicConfig(level=lvl, handlers=(sh, fh), datefmt=fmt)
    logger = logging.getLogger(name)
    global print
    print = logger.info
    return






def fun():
    print("inside the fun")


def main():

    set_logger("logged_script") 

    line = "MAX!"
    print(line) 
    fun()
    
    return 0


if __name__ == "__main__":
    main()
