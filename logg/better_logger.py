import logging


def set_logger(name:str=__name__):
    """
    put this function in every file you wish to be logged
    (other scripts will not be affected)
    - set new logger for stream and file
    - overrides print function
    """
    logger = logging.getLogger(name)
    logger.setLevel(logging.INFO)
    # always create console handler
    sh = logging.StreamHandler()
    sh.setLevel(logging.INFO)
    sfmt = logging.Formatter('%(asctime)s - %(message)s')
    sh.setFormatter(sfmt)
    logger.addHandler(sh)
    # create file handler 
    fh = logging.FileHandler(filename=f"{name}.log", mode="w")
    fh.setLevel(logging.INFO)
    ffmt = logging.Formatter('%(asctime)s [%(name)s][%(levelname)s] %(message)s')
    sh.setFormatter(ffmt)
    logger.addHandler(fh)
    # override 'print' behaviour
    global print
    print = logger.info
    return
set_logger()




def main():    
    print('print message again')
    return

if __name__=="__main__":
    main()
    # input()