import numpy as np
import matplotlib.pyplot as plt
from time import perf_counter


def mc_cir(rand, reps, periods, dt, k, th, eta, rho, v0, pos=None):
    ''' returns N cir trajectories and the relative integral'''
    ### create vol and int with small dt
    Z = np.empty(shape=(periods+1, reps))
    Z[0] = v0
    Int = np.empty(shape=(periods+1, reps))
    Int[0] = .0
    xi = rand.normal(size=(periods, reps))
    for n in range(periods):
        Ro = Z[n]
        Rn = Ro + k*(th-Ro)*dt + eta*np.sqrt(Ro*dt)*xi[n]
        np.maximum(Rn, 0, out=Rn)
        Z[n+1] = Rn
        Int[n+1] = Int[n] + .5*(Rn+Ro)*dt
    # rescaling factor to accomodate the underliyng process
    if pos is not None:
        Z = Z[pos]
        Int = Int[pos]
    return Z, Int


def mc_heston(rand, s_reps, s0, vol, int_vol, th, k, eta, rho, s_periods, s_dt, mu):
    ''' generates N heston trajectories, starting from a
    volatility and its integral of the same length'''
    S  = np.empty(shape=(s_periods+1, s_reps))
    xi = rand.normal(size=(s_periods+1, s_reps))
    S[0] = s0
    for n in range(1,s_periods+1):
        DI = int_vol[n] - int_vol[n-1]
        X = mu*s_dt - .5*DI + (rho/eta)*(vol[n] - vol[n-1] - k*(th*s_dt - DI)) + np.sqrt((1 - rho*rho)*DI)*xi[n-1]
        S[n] = S[n-1]*np.exp(X)
    return S


def MC(rand, v_reps, pv_reps, v_steps, v_dt, k, theta, eta, rho, v0, s_reps, s0, s_periods, s_dt, mu):
    fm = np.zeros(shape=s_periods+1, dtype=np.double)
    sm = np.zeros(shape=s_periods+1, dtype=np.double)
    pos = np.linspace(start=0, stop=v_steps-1, num=s_periods+1, dtype=np.int32)
    print("%30d"%(v_reps*s_reps))
    tim = perf_counter()
    for a in range(v_reps//pv_reps):
        V, I = mc_cir(rand, pv_reps, v_steps, v_dt, k, theta, eta, rho, v0, pos)
        for r in range(pv_reps):
            v = V[:, r]
            i = I[:, r]
            s = mc_heston(rand, s_reps, s0, v, i, theta, k, eta, rho, s_periods, s_dt, mu)
            fm += np.mean(s, axis=1)/v_reps
            sm += np.mean(s*s, axis=1)/v_reps
        print("%30d -- %2.2f\r"%((1+a)*pv_reps*s_reps, perf_counter()-tim), end='', flush=True)
    sm = 3*np.sqrt((sm-fm*fm)/v_reps)
    return fm, sm



def mc_plot(dt, perf_counters, ES, ERR, s0):
    '''simple MC plotting routine'''
    ax = [dt*i for i in range(perf_counters+1)]
    plt.errorbar(x=ax, y=ES, yerr=ERR)
    plt.ylim(s0*.9, s0*1.1)
    plt.plot([0, ax[-1]], [s0, s0], "r")
    plt.title("heston -- martingale")
    plt.show()



def main():

    rand = np.random.RandomState()
    rand.seed(45523)

    print("USING THE INTEGRAL OF THE VOLATILITY")
    
    v_reps = 13#int(input("log_2 of vol reps? "))
    if v_reps<10:
        v_reps=10
    v_reps = 1<<v_reps
    pv_reps = 1<<10

    v_steps = 360
    v_dt = 1/360
    k = .1
    theta = .24
    v0 = .24
    eta = .4
    rho = -.5

    s_reps = 10#int(input("log_2 of price reps per vol? "))
    if s_reps<10:
        s_reps=10
    s_reps = 1<<s_reps
    s_periods = 12
    s_dt = 1/12
    mu = .0
    s0 = 4.

    ES, ERR = MC(rand, v_reps, pv_reps, v_steps, v_dt, k, theta, eta, rho, v0, s_reps, s0, s_periods, s_dt, mu)

    mc_plot(s_dt, s_periods, ES, ERR, s0)

if __name__=="__main__":
    main()
